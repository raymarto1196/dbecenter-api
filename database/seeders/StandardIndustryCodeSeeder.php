<?php

namespace Database\Seeders;

use App\Models\Sic;
use App\Models\StandardIndustryCode;
use Illuminate\Database\Seeder;

class StandardIndustryCodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
           $naic_codes = [
                [
                    'code' => '238130',
                    'description' => 'Carpentry, framing',
                ],                
                [
                    'code' => '238150',
                    'description' => 'Curtain wall, glass, installation ',
                ],
                [
                    'code' => '541512',
                    'description' => 'Computer Systems Design Services',
                ],
                [
                    'code' => '311212',
                    'description' => 'Rice Milling',
                ],
                [
                    'code' => '424490',
                    'description' => 'Baby foods, canned, merchant wholesalers',
                ],
                [
                    'code' => '212321',
                    'description' => 'Construction sand or gravel dredging',
                ],
                [
                    'code' => '238120',
                    'description' => 'Stairway, precast concrete, installation',
                ]
            ];

            foreach ($naic_codes as $key => $naic_code) {
                StandardIndustryCode::create($naic_code);
            }
    }
}
