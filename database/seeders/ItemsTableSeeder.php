<?php

namespace Database\Seeders;

use App\Imports\ItemsImport;
use App\Models\InventoryBinMstr;
use App\Models\InventorySiteMstr;
use App\Models\Item;
use App\Models\ItemQuantity;
use App\Models\Vendor;
use App\Models\VendorCreditTerms;
use App\Models\Warehouse;
use App\Models\WarehousePallet;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;
use Faker\Factory as Faker;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $item_name = ['CARDIPRES 25 MG', 'PROVASC 5MG', 'TENORVAS 50MG', 'ITORVAZ 40MG', 'ITORVAZ 80MG', 'VIATREX 1G', 'ICOX 200 MG 50s', 'CLOVIX 75 MG', 'VIAMOX 1G', 'ZINOF 200MG 20s'];
        $item_description = ['CARVEDILOL 25 MG', 'AMLODIPINE BESILATE 5MG', 'ATENOLOL 50MG', 'ATORVASTATIN 40MG', 'ATORVASTATIN 80MG', 'CEFTRIAXONE 1G', 'CELECOXIB 200 MG', 'CLOPIDOGREL 75 MG','CO-AMOXICLAV 1G','FENOFIBRATE 200MG 20s'];

        foreach (range(1, 10) as $index => $number) {
            $warehouseCenterFloor = WarehousePallet::inRandomOrder()->first();// to check...
			$warehouseCenter = Warehouse::inRandomOrder()->first();
            $vendor = Vendor::inRandomOrder()->first();

            $item_code = sprintf(config('app.code_format'), $number);
            $unit_cost = $faker->randomNumber(2, true);
            $selling_price = $unit_cost + ($unit_cost * .10); //For example: an addional of n% to the unit cost
            $item = [
                "vendor_id" => $vendor->id,
                "item_code" => "BK-{$item_code}",
                "item_name" => $item_name[$index],
                "item_description" => $item_description[$index],
                "unit_cost" => $unit_cost,
                "selling_price" => $selling_price,
                "extended_pricing" => 0,
                "purchased_price" => $unit_cost,
                "list_price" => $selling_price,
                "uom" =>  $faker->randomElement(['tablet','capsule','sachet','can','soft gel','piece/s','box']),
                "item_type" => "drug",
                "free_goods" => rand(5, 10),
                "vat_exemption" => $faker->randomElement([1,0]),
                "purchasing_uom" => $faker->randomNumber(2, true),
                "selling_uom" => $faker->randomNumber(2, true),
                "currency" => $faker->randomNumber(2, true),
                "item_tax_schedule_id" => $faker->randomNumber(1, true),
                "last_generated_lot_number" => $faker->randomNumber(1, true),
                "qty_per_pack" => $faker->randomNumber(2, true),
                "vendor_lead_time" => $faker->randomElement(['30','60','90','120']),
                "lot_expiry_warning" => $faker->randomNumber(2, true),
                "lot_expiry_warning_days" => $faker->randomNumber(2, true),
                "manufacturer" => $faker->company,
                "pharmacological_category" => $faker->word,
                "price_method" => $faker->word,
                "item_group" => $faker->randomElement(['CARDIO','HOSPITAL','DIAB','NUTRA']),
                "item_segment" => $faker->randomElement(['ORAL','NUTRA','IV']),
                "packaging" =>  $faker->randomElement(['BOX','CAN']),
                'status' => $faker->randomElement(['pending','approved','drafts']),
                "stock_level" =>  0,
                "cpr_no" =>  $faker->randomNumber(6, true),
                "cpr_expiry" =>  $faker->dateTimeBetween('-1 week', '+1 week'),
                "date_drafted" =>  $faker->dateTimeBetween('-1 week', '+1 week'),
                "purchased_tax_option" => $faker->randomNumber(3, true),
                "safety_stock_level" => $faker->randomElement(['5000','10000','20000']),
                "total_quantity" => $faker->randomNumber(6, false),
                "shelf_life" => $faker->randomElement(['9','12','24']),
                "storage_requirements" => $faker->randomNumber(2, true),
                "item_class" => $faker->word,
                "distributor" => $faker->company,
                "cpr_validity_from" => $faker->randomNumber(2, true),
                "cpr_validity_to" => $faker->randomNumber(2, true),
                "cpr_details" => $faker->word,
                "gpm" => $faker->randomNumber(2, true),
                "sell_pack_box" => $faker->randomNumber(2, true),
                "sell_pack_cans" => $faker->randomNumber(2, true),
                "sell_pack_vials" => $faker->randomNumber(2, true),
                "sell_pack_batch_size" => $faker->randomNumber(2, true),
                "sell_pack_moq" => $faker->randomNumber(2, true),
                "sell_pack_temp_humid" => $faker->randomNumber(2, true),
                "sell_pack_gross_weight" => $faker->randomNumber(2, true),
                "sell_pack_net_weight" => $faker->randomNumber(2, true),
                "sell_pack_box_length" => $faker->randomNumber(2, true),
                "sell_pack_box_width" => $faker->randomNumber(2, true),
                "sell_pack_box_height" => $faker->randomNumber(2, true),
                "sell_pack_box_prospect" => $faker->randomNumber(2, true),
                "sell_unit_can" => $faker->randomNumber(2, true),
                "sell_unit_pouch" => $faker->randomNumber(2, true),
                "sell_unit_box" =>$faker->randomNumber(2, true),
                "sell_unit_qty_pack" => $faker->randomNumber(2, true),
                "sell_unit_shelf_life" => $faker->randomNumber(2, true),
                "sell_unit_barcode" => $faker->bothify('?###??##'),
                "sell_unit_length" =>  $faker->randomNumber(2, true),
                "sell_unit_width" =>  $faker->randomNumber(2, true),
                "sell_unit_height" =>  $faker->randomNumber(2, true),
                "temperature" =>  $faker->randomElement(['25 degree','40 degree','50 degree']),
                "humidity" =>  $faker->randomElement(['50%','30%','10%']),
                "net_weight" =>  $faker->randomElement(['10','15','30']),
                "gross_weight" =>  $faker->randomElement(['10','15','30']),
                "big_box" =>  $faker->randomElement(['10x10x10','5x5x5','30x30x30']),
                "small_box" =>  $faker->randomElement(['3x3x3','1x1x1','10x10x10']),
                "minimum_order_quantity" => $faker->randomElement(['5000','10000','20000']),
            ];

            $item = Item::create($item);

            //first create a vendor then create a product and add an item in the credit terms in the vendor
            $data = [
                'vendor_id' => $item->vendor_id,
                'item_id' =>  $item->id,
                'down_payment' => 50,
                'upon_delivery' =>  25,
                'days_upon_delivery' => rand(30,90),
                'days_percentage' => 25,
             ];

             VendorCreditTerms::create($data);

        }
    }
}

// database\seeders\ItemsTableSeeder.php
