<?php

namespace Database\Seeders;

use App\Models\InventoryRevaluation;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            // BankSeeder::class,
            // DepartmentsTableSeeder::class,
            UserRoleSeeder::class,
            UsersTableSeeder::class,
            CategoriesTableSeeder::class,
            StandardIndustryCodeSeeder::class,
            AssesstmentQuestionnaireSeeder::class,
            // ItemTypeSeeder::class,
            // ItemUOMSeeder::class,
            // ItemTaxOptionSeeder::class,
            // ItemTrackingOptionSeeder::class,
            // ItemLocationTrackingOptionSeeder::class,
            // FieldConfigurationsSeeder::class,
            // WarehouseSeeder::class,
            // VendorSeeder::class,
            // ItemsTableSeeder::class,
            // ItemBatchSeeder::class,
            // ItemQuantitySeeder::class,
            // InventoryRevaluationSeeder::class,
            // CourierSeeder::class,
            // CustomerSeeder::class,
            // PurchaseRequestSeeder::class,
            // PurchaseOrderSeeder::class,
            // InventoryReceiptSeeder::class,
            // InventoryAdjustmentSeeder::class,
            // SalesOrderSeeder::class,
            // InventoryTransferSeeder::class,
            // MaintenanceFieldCustomizationSeeder::class,
            // MaintenanceCodeSetupSeeder::class,
            // DocumentationSeeder::class,
            // CustomerCourierRequirementSeeder::class,
            // PaymentSeeder::class,
            // StaticModuleRoutesSeeder::class,
            // \AddressSeeder::class,
        ]);
    }
}
