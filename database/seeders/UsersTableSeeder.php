<?php

namespace Database\Seeders;

use App\Models\Department;
use App\Models\User;
use App\Models\UserProfile;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create admin user
        $Users = [
            [
                // 'name' => 'SYSADMIN',                                            : COMMENTED BY RAYMART 04/03/2022
                'email' => 'sysadmin@dbecenter.com',
                'username' => 'sysadmin',
                'email_verified_at' => now(),
                'password' => bcrypt('@secret!'),
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'email' => 'trial1@dbecenter.com',
                'username' => 'vendor',
                'type' => 'Small Business',
                'email_verified_at' => now(),
                'password' => bcrypt('@Test1234'),
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'email' => 'trial2@dbecenter.com',
                'type' => 'Government Agencies',
                'username' => 'contractor',
                'email_verified_at' => now(),
                'password' => bcrypt('@Test1234'),
                'created_at' => now(),
                'updated_at' => now()
            ]
        ];

        foreach ($Users as $key => $user) {
            $userData = User::create($user);
            
            $user_profile = [
                'user_id' => $userData->id,
                'lastname' => 'Admin',
                'firstname' => $userData->username,
                'telephone' => '0909123123',
                'address1' => 'Test',
                'address2' => ' I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system',
                'city' => 'Bago',
                'region' => 'South',
                'zipcode' => '5001',
                'country' => 'Philippines',
            ];

            $user = UserProfile::create($user_profile);
            
            if($userData->username == 'sysadmin'){
                $userData->assignRole('SYSADMIN');
            }elseif($userData->username == 'vendor'){
                $userData->assignRole('VENDOR');
            }elseif($userData->username == 'contractor'){
                $userData->assignRole('CONTRACTOR');
            }
              
        }


        // $items = [
        //     // PROJECTS
        //     [
        //         'item_image' => 'https://images.unsplash.com/photo-1492551557933-34265f7af79e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTR8fHByb2plY3RzfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop',
        //         'item_name' => 'Encoder',
        //         'identifier' => '1ss3x1',
        //         'category_id' => '3',
        //         'item_type' => 'Project',
        //         'item_description' => 'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system',
        //         // 'project_from' =>  '2022-11-11',
        //         // 'project_to' => '2023-05-05'
        //     ],[
        //         'item_image' => 'https://images.unsplash.com/photo-1531403009284-440f080d1e12?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop',
        //         'item_name' => 'Office Staff',
        //         'identifier' => 'va34c1',
        //         'category_id' => '3',
        //         'item_type' => 'Project',
        //         'item_description' => 'Some quick example text to build on the card item_name and make up the bulk of the cards content. Some quickSome quickSome quick',
        //         // 'project_from' =>  '2022-11-11',
        //         // 'project_to' => '2023-05-05'
        //     ],[
        //         'item_image' => 'https://images.unsplash.com/photo-1509463536615-1ca163bcfb3f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop',
        //         'item_name' => 'Construction',
        //         'identifier' => 'nvac123',
        //         'category_id' => '3',
        //         'item_type' => 'Project',
        //         'item_description' => 'which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure',
        //         // 'project_from' =>  '2022-11-13',
        //         // 'project_to' => '2023-08-05'
        //     ]

        //     //PRODUCTS
        //     ,[
        //         'item_image' => 'https://images.unsplash.com/photo-1523275335684-37898b6baf30?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8cHJvZHVjdHxlbnwwfHwwfHw%3D&auto=format&fit=crop',
        //         'item_name' => 'Watch',
        //         'identifier' => 'x1x12591',
        //         'category_id' => '2',
        //         'item_type' => 'Product',
        //         'item_description' => 'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system',
        //         //'item_star_rating' => '4',
        //         'quantity' => '51',
        //         'unit_cost' => '100.00'
        //     ],[
        //         'item_image' => 'https://images.unsplash.com/photo-1625772452859-1c03d5bf1137?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fHByb2R1Y3RzfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop',
        //         'item_name' => 'Dove',
        //         'identifier' => 'x13259x',
        //         'category_id' => '2',
        //         'item_type' => 'Product',
        //         'item_description' => 'Some quick example text to build on the card item_name and make up the bulk of the cards content. Some quickSome quickSome quick',
        //         //'item_star_rating' => '5',
        //         'quantity' => '52',
        //         'unit_cost' => '311.00'
        //     ],[
        //         'item_image' => 'https://images.unsplash.com/photo-1526947425960-945c6e72858f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTh8fHByb2R1Y3RzfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop',
        //         'item_name' => 'Lotion',
        //         'identifier' => 'x15asda',
        //         'category_id' => '2',
        //         'item_type' => 'Product',
        //         'item_description' => 'which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure',
        //         //'item_star_rating' => '2',
        //         'quantity' => '67',
        //         'unit_cost' => '512.00'
        //     ]

        //     //SERVICES
        //     ,[
        //         'item_image' => 'https://media.istockphoto.com/photos/young-friendly-operator-woman-agent-with-headsets-beautiful-business-picture-id1333748315?b=1&k=20&m=1333748315&s=170667a&w=0&h=4UiqyJtx2T4FX62yhmbgvINeE_xwad1VBdUp2HOyo-8=',
        //         'item_name' => 'Call Center Agents',
        //         'identifier' => 'mcas132',
        //         'category_id' => '1',
        //         'item_type' => 'Service',
        //         'item_description' => 'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system',
        //         //'item_star_rating' => '4',
        //         'quantity' => '51',
        //         'unit_cost' => '100.00'
        //     ],[
        //         'item_image' => 'https://media.istockphoto.com/photos/home-repairman-working-on-a-furnace-picture-id1291079851?b=1&k=20&m=1291079851&s=170667a&w=0&h=37wkK1Xxwy7NUoD6TRb6OhHQgOx-OETSg3G98_7vXts=',
        //         'item_name' => 'Electrical Technician',
        //         'identifier' => 'x13259',
        //         'category_id' => '1',
        //         'item_type' => 'Service',
        //         'item_description' => 'Some quick example text to build on the card item_name and make up the bulk of the cards content. Some quickSome quickSome quick',
        //         //'item_star_rating' => '5',
        //         'quantity' => '61',
        //         'unit_cost' => '311.00'
        //     ],[
        //         'item_image' => 'https://media.istockphoto.com/photos/latin-descent-blue-collar-air-conditioner-repairman-at-work-picture-id1316667059?b=1&k=20&m=1316667059&s=170667a&w=0&h=fwU6lb2YEWRbrSCEORIJReXdI9r5DLsEkn4-H7fE7Zk=',
        //         'item_name' => 'Delivery Service',
        //         'identifier' => 'oasj412',
        //         'category_id' => '1',
        //         'item_type' => 'Service',
        //         'item_description' => 'which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure',
        //         //'item_star_rating' => '1',
        //         'quantity' => '41',
        //         'unit_cost' => '512.00'
        //     ]
        // ];

        // foreach ($items as $key => $item) {
        //     $admin->item()->create($item);
        // }

        // Assign roles to users
    }
}
