<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $default_roles = [
            'SYSADMIN',
            'ADMINISTRATOR',
            'VENDOR',
            'CONTRACTOR'
        ];

        foreach ($default_roles as $key => $value) {
            Role::create(['name' => $value]);
        }
    }
}
