<?php

namespace Database\Seeders;

use App\Models\AssessmentQuestionnaire;
use Illuminate\Database\Seeder;

class AssesstmentQuestionnaireSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $questionnaires = [
            [
                'assessment_questionnaire_list' => [
                    'general_information' => [
                        'Company Name',
                        'Company Type',
                        'Year Established',
                        'State Established',
                        'Company Owner',
                        'Mobile Phone',
                        'Email',
                        'Position',
                        'Do you Own or Rent any other facilities i.e. a storage unit, a space at a yard, etc?',
                        'Do you have any business licenses?',
                        'Are you a Union Member/Signor?',   
                        'Have you been bonded for a project?',
                        'What is your highest bonding capacity?',
                        'Do you have any issues with obtaining insurance or bonding?',
                        'Commercial Automobile Insurance Coverage',
                        'General Liability Insurance Coverage',
                        'Workmans Compensation Insurance Coverage',
                        'Does your company have any other partners or investors?',
                        'Do your company own any intellectual Property?',
                        'What do you do?',
                        'Does your company have and Government designations?',
                        'What Government Designations does your Company have?',
                        'Do you work with any of the following?'
                    ],
                    'strategy' => [
                        'What are your goals for the next 3 years?',
                        'List 3 things you would like to improve in your business.',
                        'Provide 3-year vision for your business and yourself (Revenue Amount, #of employees, etc)',
                        'Provide 7-year vision for your business and yourself (Revenue Amount, #of employees, etc)',
                        'Explain your exit strategy: Include who is your successor & why? How is your successor being groomed or prepared ? Are your interested in expanding/franchising? Will you sell your company?',
                        'What are your objectives for the next 3 years?',
                        'what is slowing you down?',
                        'Provide 5-year vision for your business and yourself (Revenue Amount #)',
                        'Provide 10-year vision for your business and yourself (Revenue Amount, #of employees, etc)',

                        'Have you attended any business workshops/training in the last 12 months?',
                        'what are you doing to improve YOURSELF as the leader of your company?',
                        '# of education/training hours attended'
                    ],
                    'finance_and_accounting' => [
                        'Which of these financial documents do you have?',
                        'How often do you review your Financial Statements?',
                        'What is your year to date revenue?',
                        'What is your 3 year average revenue?',
                        'What is your 3 year average MET profit or NET income?',
                        'Are partner(s)/invenstor(s) receiving dividens?',
                        'Do you have any capital reserves to fund a projet start for 90 days?',
                        'Source of your capital reserves?',
                        'Amount of ($) Capital Reserves?',
                        'Are your interested in additional funding?',
                        'What was your credit score the last time your checked it?',
                        'Other than CAPITAL are there other financing challenges that you face?'
                    ],
                    'human_resources' => [
                        'Who handles your accounts receivable/payables?',
                        'Who handles your payroll?',
                        'Do you use an accounting software?',
                        'Which accounting software do you use?',
                        'Do you have an inventory system in place?',
                        'What makes up your inventory?',
                        'Estimated value of your inventory?',
                        'Are you collecting a salary?',
                        'What is your 3 year average annual salary?',
                        'What is your year to date annual salary?',
                        'Do you have any HR manuals?',
                        'I have the following HR manuals',
                        'Number of full time employess do you have (Including yourself and your partners)',
                        'Number of Part-time employees',
                        'Seasonal employees do you have?',
                        'What benefits do you provide to your employees?',
                        'Please provide any additional comments or explanations pertaining to your companys Accounting & Human Resource practices'
                    ],
                    'marketing' => [
                        'Does your Company have a website?',
                        'Website',
                        'What industry do you serve?',
                        'Who handles marketing?',
                        'Do you have any of the following?',
                        'Explain your Marketing Strategy, What do you do? if you have someone else doing it, what do they do?',
                        'How are you building your network?',
                        'Do you use any social media platforms?',
                        'Which social media platforms do you use?',
                        'List your company advantages(strengths) over the competition.',
                        'List your company disadvantages(weaknesses) over the competition.',
                        'List your competitors strengths(threats)',
                        'List your competitors weaknesses(oppurtunities)',
                        'What makes you diffrent from your competition?',
                        'If you have a tag line, add it here.',
                        'What awards has your company received?',
                        'Award Name',
                        'Recieved From'
                    ],
                    'project_and_biddings' => [
                        'Do you have any of the following?',
                        'Have you been awarded Contracts?',
                        'How many Bids/proposal do you submit per month?',
                        'How many of these submitted bids do your get awarded successfully',
                        'Why do you feel you were awarded the contract?',
                        'If you were not awarded the contract/what do you think/feel are the reason why?',
                        'How do you see yourself overcoming any of the mentioned changes?',
                        'Do you have experience in or be interested in exploring maintenance contract opportunities in the following areas...if you have other experiences, please add them.',
                        'Select MN Areas/Disctrict Preference',
                        'What takeoff/Estimating software do you use?'

                    ],        
                ],
            ],                
        ];

        foreach ($questionnaires as $key => $questionnaire) {
            AssessmentQuestionnaire::create($questionnaire);
        }
    }
}
