<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained();
            $table->string('profile_picture')->nullable();
            $table->string('lastname');
            $table->string('firstname');
            $table->string('middlename')->nullable();
            $table->string('suffixname')->nullable();
            $table->string('telephone')->nullable()->comment();
            $table->string('address1')->nullable()->comment();
            $table->string('address2')->nullable()->comment();
            $table->string('city')->nullable()->comment();
            $table->string('region')->nullable()->comment();
            $table->string('zipcode')->nullable()->comment();
            $table->string('country')->nullable()->comment();
            $table->timestamps();
            $table->softDeletes();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profiles');
    }
}
