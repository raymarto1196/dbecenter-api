<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceAvailmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_availments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('item_id');
            $table->foreignId('user_id');
            $table->foreignId('vendor_id');
            $table->string('reference_number')->unique()->nullable();  
            $table->string('name')->nullable();  
            $table->string('telephone')->nullable();  
            $table->string('address')->nullable();  
            $table->string('description')->nullable()->comment('delivery instructions');
            $table->string('status')->default('pending');
            $table->string('item_name');
            $table->string('identifier')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_availments');
    }
}
