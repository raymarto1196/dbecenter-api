<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('vendor_id');
            $table->string('reference_number')->unique()->nullable();  
            $table->string('name')->nullable();  
            $table->string('telephone')->nullable();  
            $table->string('shipping_address')->nullable();  
            $table->string('description')->nullable()->comment('delivery instructions');
            $table->string('status')->default('pending');
            $table->integer('total_price')->default(0);
            $table->date('estimated_delivery_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
