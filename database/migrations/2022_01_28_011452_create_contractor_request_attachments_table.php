<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * CreateContractorRequestAttachmentsTable
 *
 * Attachments on contractor requests
 */
class CreateContractorRequestAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contractor_request_attachments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('request_id')->constrained('contractor_requests');
            $table->string('name');
            $table->string('attachment')->comment('Absolute file path');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contractor_request_attachments');
    }
}
