<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained()->comment('Company Owner');
            $table->string('name')->unique();
            $table->string('logo')->nullable();
            $table->text('description')->nullable();
            $table->string('telephone')->nullable()->comment();
            $table->string('address1')->nullable()->comment();
            $table->string('address2')->nullable()->comment();
            $table->string('city')->nullable()->comment();
            $table->string('region')->nullable()->comment();
            $table->string('zipcode')->nullable()->comment();
            $table->string('country')->nullable()->comment();
            $table->json('capabilities')->nullable()->comment();
            $table->json('differentiators')->nullable()->comment();
            $table->json('certifications')->nullable()->comment();
            $table->json('contract_vehicles')->nullable()->comment();
            $table->json('government_designation')->nullable()->comment();
            $table->json('naics_codes')->nullable()->comment();
            $table->json('references')->nullable()->comment();
            $table->text('duns_number')->nullable()->comment('Data Universal Numbering System');
            $table->text('cage_code')->nullable()->comment('Commercial and Government Entity');
            $table->json('contact_information')->nullable()->comment();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
