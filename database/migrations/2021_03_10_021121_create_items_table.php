<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable()->constrained('users')->comment('Item Owner');
            $table->text('item_image')->nullable();
            $table->string('identifier')->nullable()->comment('SKU/NAICS Code/Model Number');
            $table->string('item_type')->nullable()->comment('products|services|contracts|event|resource');
            $table->integer('category_id')->nullable()->comment('1 - Services/ 2 - Products/ 3 - Projects');
            $table->string('item_name')->nullable()->comment('Item Name');
            $table->text('item_description')->nullable()->comment('Item Generic Name');
            $table->integer('quantity')->default(0);
            $table->string('status')->nullable()->comment('pending|posted|declined|deleted');
            $table->integer('total_views_count')->default(0);
            $table->text('manufacturer')->nullable()->comment('Manufacturer');
            $table->decimal('unit_cost', 10, 2)->nullable()->comment('Purchased Price');
            $table->decimal('vat_unit_cost', 10, 2)->nullable()->comment('Unit Cost with/without Vat exempt');
            $table->decimal('selling_price', 10, 2)->nullable()->comment('Selling Price');
            $table->decimal('vat_selling_price', 10, 2)->nullable()->comment('Selling Price with/without Vat exempt');
            $table->decimal('extended_pricing', 10, 2)->nullable();
            $table->date('from')->nullable()->comment('project from date');
            $table->date('to')->nullable()->comment('project to date');;
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
