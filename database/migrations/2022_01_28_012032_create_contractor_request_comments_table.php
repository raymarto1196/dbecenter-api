<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractorRequestCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contractor_request_comments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('request_id')->constrained('contractor_requests');
            $table->foreignId('user_id')->constrained();
            $table->text('comment');
            $table->integer('reply_id')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contractor_request_comments');
    }
}
