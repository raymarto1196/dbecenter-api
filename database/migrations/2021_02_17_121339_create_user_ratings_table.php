<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_ratings', function (Blueprint $table) {
            $table->unsignedBigInteger('userid_rater');
            $table->unsignedBigInteger('userid_ratee');
            $table->integer('rate');
            $table->text('comment')->nullable();
            $table->timestamps();

            // Add indexes composite keys
            $table->primary(['userid_rater', 'userid_ratee']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_ratings');
    }
}
