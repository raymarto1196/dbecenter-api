<?php

namespace App\Exceptions;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Validation\ValidationException;
use Spatie\Permission\Exceptions\RoleDoesNotExist;
use Spatie\Permission\Exceptions\UnauthorizedException as ExceptionsUnauthorizedException;
use Throwable;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });

        $this->renderable(function (RoleDoesNotExist $exception, $request) {
            return response()->json(['error' => true, 'message' => $exception->getMessage()], Response::HTTP_UNPROCESSABLE_ENTITY);
        });

        $this->renderable(function (NotFoundHttpException $exception, $request) {
            return response()->json([
                'error' => true,
                'message' => 'Requested resource not found.',
                'results_count' => 0,
                'results' => [],
            ], Response::HTTP_NOT_FOUND);
        });
    }

    /**
     * Response for Not Authenticated Users
     * This method is called for all unauthorized/unauthenticated api calls
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        return response()->json([
                'error' => true,
                'message' => $exception->getMessage()
            ], Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Convert a validation exception into a JSON response.
     * This method is called when validation fails from a call $request->validate()
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Validation\ValidationException  $exception
     * @return \Illuminate\Http\JsonResponse
     */
    protected function invalidJson($request, ValidationException $exception)
    {
        return response()->json([
            'error' => true,
            'message' => $exception->getMessage(),
            'errors' => $exception->errors()
        ], $exception->status);
    }
}
