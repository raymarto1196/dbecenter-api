<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;

class CreateUserRequest extends FormRequest
{



    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'employee_number' => '',
            'firstname' => 'bail|required',
            'middlename' => '',
            'lastname' => 'required',
            'email' => 'required|email|unique:users,email',
            'username' => 'unique:users,username',
            'password' => [
                'required',
                'confirmed',
                'min:6',
                'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$/'
            ],
            'department' => '',
            'position' => '',
            'signature_settings' => '',
            'signature_file' => '',
            'signature_nitials' => '',
            'role' => '',
        ];

        $roles = Role::all()->pluck('id')->toArray();

        if (count($this->post('roles')) != count(array_intersect($roles, $this->post('roles')))) {
            $rules['roles'] = [
                'required',
                Rule::in($this->roles),
            ];
        }

        if ($this->post('signature_settings') == 'e_signature') {
            // user must have uploaded file, check file extension, filesize
            $rules['signature_file'] = 'required|mimes:jpeg,jpg,png|max:2048';
        }

        // Updating...
        if ($this->getMethod() == 'PUT') {
            // Ignore if its the user current email
            $rules['email'] = 'required|email|unique:users,email,'.$this->route('user').'id';

            // Leave password field empty if you dont want to update it
            if (empty($this->post('password'))) {
                $rules['password'] = '';
            }
        }

        return $rules;

    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'password.min:6' => 'The :attribute must be at least 6 characters in length',
            'password.regex' => 'The :attribute must contain at least 1 digit, 1 lowercase and uppercase letter and a special character'
        ];
    }
}
