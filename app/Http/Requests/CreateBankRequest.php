<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateBankRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'name' => ['required'],
            'address1' => '',
            'address2' => '',
            'address3' => '',
            'city' => '',
            'state' => '',
            'zipcode' => '',
            'country' => '',
            'phone1' => '',
            'phone2' => '',
            'phone3' => '',
            'fax' => '',
            'transit_number' => '',
            'branch' => '',
        ];

        return $rules;
    }
}
