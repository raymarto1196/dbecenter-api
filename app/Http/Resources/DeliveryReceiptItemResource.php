<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DeliveryReceiptItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'item_id'           => $this->item_id,
            'item_description'  => $this->item_detail->item_description,
            'uom'               => $this->uom,
            'lot_number'        => $this->lot_number,
            'expiry_date'       => $this->expiry_date,
            'total'             => $this->allocated_quantity,                             
            'unit_cost'         => $this->unit_cost,
            'amount'            => $this->amount,
        ];
    }
}
