<?php

namespace App\Http\Resources;
use App\Models\InventoryReceiptItem;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\IventoryReceiptResourceItem;
class InventoryReceiptResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    { 
        return [
            'id' =>  isset($this->id) ? $this->id : '',
            'inventory_receipt_code' => isset($this->inventory_receipt_code) ? $this->inventory_receipt_code : '',
            'vendor_id' => isset($this->vendor_id) ? $this->vendor_id : '',
            'company' =>  isset($this->vendor->company) ? $this->vendor->company : '',
            'purchase_order_id' =>  isset($this->purchase_order_id) ? $this->purchase_order_id : '',
            'purchase_order_code' =>  isset($this->purchase_order->reference_number) ? $this->purchase_order->reference_number : '',
            'pr_date' =>  isset($this->purchase_order->pr_date) ? $this->purchase_order->pr_date->format('m-d-Y') : '',
            'dr_number' =>  isset($this->dr_number) ? $this->dr_number : '',
            'dr_date' =>  isset($this->dr_date) ? $this->dr_date->format('m-d-Y') : '',
            'si_number' =>  isset($this->si_number) ? $this->si_number : '',
            'si_date' =>  isset($this->si_date) ? $this->si_date->format('m-d-Y') : '',
            'irr_remarks' =>  isset($this->irr_remarks) ? $this->irr_remarks : '',
            'journal_remarks' =>  isset($this->journal_remarks) ? $this->journal_remarks : '',
            'file_data' =>  isset($this->file_data) ? $this->file_data : '',
            'received_by' =>  isset($this->receiver->name) ? $this->receiver->name : '',
            'posted_by' =>  isset($this->poster->name) ? $this->poster->name : '', 
            'quality_control_status' => isset($this->quality_control_status) ? $this->quality_control_status : '',
            'status' => isset($this->status) ? $this->status : '',
            'color_assignment' => isset($this->color_assignment) ? $this->color_assignment : '',
            'color_assignment_remarks' => isset($this->color_assignment_remarks) ? $this->color_assignment_remarks : '',
            'label_information' => isset($this->label_information) ? $this->label_information : '',
            'label_information_remarks' => isset($this->label_information_remarks) ? $this->label_information_remarks : '',
            'packing_quality' => isset($this->packing_quality) ? $this->packing_quality : '',
            'packing_quality_remarks' => isset($this->packing_quality_remarks) ? $this->packing_quality_remarks : '',
            'smell' => isset($this->smell) ? $this->smell : '',
            'smell_remarks' => isset($this->smell_remarks) ? $this->smell_remarks : '',
            'texture' => isset($this->texture) ? $this->texture : '',
            'texture_remarks' => isset($this->texture_remarks) ? $this->texture_remarks : '',
            'recommendation' => isset($this->recommendation) ? $this->recommendation : '',
            'inspection_remarks' => isset($this->inspection_remarks) ? $this->inspection_remarks : '',
            'checked_by' => isset($this->checker->name) ? $this->checker->name : '',
            'approved_date' => isset($this->approved_date) ? $this->approved_date->format('m-d-Y') : '',
            'approved_by' => isset($this->approver->name) ? $this->approver->name : '',
            'created_at' =>  isset($this->created_at) ? $this->created_at->format('m-d-Y') : '',
            'updated_at' => $this->updated_at, 
            'deleted_at' => $this->deleted_at, 

            ///call the method items() in InventoryReceipt.php
            'items' => InventoryReceiptResourceItem::collection($this->items),
        ];

    }
}
