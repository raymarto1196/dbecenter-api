<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InventoryTransferItemsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'inventory_transfer_id' => $this->inventory_transfer_id,
            'warehouse_id_origin' => $this->warehouse_id_origin,
            'warehouse_name_origin' => $this->warehouse2->warehouse_name,
            'warehouse_pallet_id_origin' => $this->warehouse_pallet_id_origin,
            'bin_name_origin' => $this->warehouse_pallet2->bin_name,
            'item_id' => $this->item_id,
            'item_name' => $this->item->item_name,
            'item_code' => $this->item->item_code,
            'warehouse_id' => $this->warehouse_id,
            'warehouse_name' => $this->warehouse->warehouse_name,
            'warehouse_pallet_id' => $this->warehouse_pallet_id,
            'bin_name' => $this->warehouse_pallet->bin_name,
            'quantity' => $this->quantity,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
