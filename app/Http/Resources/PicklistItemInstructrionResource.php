<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PicklistItemInstructrionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'sales_order_id' => $this->sales_order_id,
            'item_id' => $this->item_id,
            'bin_location' => $this->bin_location,
            'item_name' => $this->item_name,
            'uom' => $this->uom,
            'lot_number' => $this->lot_number,
            'expiry_date' => $this->expiry_date,
            'quantity' => $this->quantity,
            'free_goods' => $this->free_goods,
            'total' => $this->quantity + $this->free_goods,
            'available_quantity' => $this->available_quantity,
            'allocated_quantity' => $this->allocated_quantity,   
            'pick_priority' => $this->pick_priority,
        ];
    }
}
