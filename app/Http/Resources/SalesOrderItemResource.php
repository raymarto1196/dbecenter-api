<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SalesOrderItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'             => $this->id,
            'sales_order_id' => $this->sales_order_id,
            'item_id'        => $this->item_id,
            'item_name'      => $this->item->item_name,
            'quantity'       => $this->quantity,
            'free_goods'     => $this->free_goods,
            'unit_cost'      => $this->unit_cost,
            'selling_price'  => $this->selling_price,
            'gross_amount'   => $this->gross_amount,
            'discount'       => $this->discount,
            'vat'            => $this->vat,
            'net_amount'     => $this->net_amount,
        ];
    }
}
