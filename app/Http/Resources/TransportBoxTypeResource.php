<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;


class TransportBoxTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // $so = DB::table('sales_orders')->where('id', $this->sales_order_id)->first();

        return [
            'id'                         => $this->id,
            'transport_shipment_id'      => $this->transport_shipment_id,
            'courier_id'                 => $this->courier_id,
            'courier'                    => $this->courier->company_name,
            'quantity'                   => $this->quantity,
            'name'                       => $this->name,
            'weight'                     => $this->weight,
            'waybill_reference_number'   => $this->waybill_reference_number,
        ];
    }
}
