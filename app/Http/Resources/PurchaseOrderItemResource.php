<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PurchaseOrderItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'purchase_order_id' => $this->purchase_order_id,
            'item_id' => $this->item_id,
            'item_name' => $this->item->item_name,
            'item_code' => $this->item->item_code,
            'item_description' => $this->item->item_description,
            'stock_level' => $this->item->stock_level,
            'unit_cost' => $this->item->unit_cost,
            'vendor_id' => $this->vendor_id,
            'item_descriptions' => $this->item_description,
            'uom' => $this->uom,
            'quantity' => $this->quantity_ordered,
            'free_goods' => $this->free_goods,
            'total_quantity' => $this->total_quantity,
            'unit_cost' => $this->unit_cost,
            'total' => $this->total,
            'in_stock' => $this->item->stock_level + $this->total_quantity,
            'vendor_lead_time' => floor($this->item->vendor_lead_time/ 30) .' months',
            'extended_price' => $this->extended_price,
        ];
    }
}
