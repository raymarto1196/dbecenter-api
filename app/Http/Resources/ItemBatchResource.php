<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ItemBatchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id, 
            'item_id' =>  isset($this->item_id) ? $this->item_id : NULL,  
            'item_name' => isset($this->item->item_name) ? $this->item->item_name : NULL,  
            'item_code' => isset($this->item->item_code) ? $this->item->item_code : NULL,  
            'lot_number' => isset($this->lot_number) ? $this->lot_number : NULL,    
            'manufacture_date' => isset($this->manufacture_date) ? $this->manufacture_date : NULL,    
            'expiry_date' => isset($this->expiry_date) ? $this->expiry_date : NULL,    
            'unit_cost' => isset($this->unit_cost) ? $this->unit_cost : NULL,    
            'uom' => isset($this->item->uom) ? $this->item->uom : NULL,   
            'quantity_received' => isset($this->quantity_received) ? $this->quantity_received : NULL,    
            'quantity_sold' => isset($this->quantity_sold) ? $this->quantity_sold : NULL,    
            'in_stock' => isset($this->item->quantity_on_hand) ? $this->item->quantity_on_hand : NULL,  
            'system_count' => $this->quantity_received - $this->quantity_sold,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ];
    }
}
