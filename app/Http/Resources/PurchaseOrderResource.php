<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PurchaseOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'purchase_order_code' => $this->reference_number,
            'description' => $this->description,
            'status' => $this->status,
            'shipping_address' => $this->shipping_address,
            'date_needed' =>  isset($this->date_needed) ? $this->date_needed->format('m-d-Y') : '',
            'particulars' =>  $this->particulars,
            'description'=>  $this->description,
            'payment_terms' =>  $this->payment_terms,
            'estimated_delivery_date' => isset($this->estimated_delivery_date) ? $this->estimated_delivery_date->format('m-d-Y') : '',
            'requested_by' => isset($this->requested_by) ? $this->requested_by : '',
            'requestor' => isset($this->user->name) ? $this->user->name : '',    
            'contact_number' => $this->contact_number,
            'vendor_id' => $this->vendor_id,
            'company' => $this->vendor->company,
            'created_at' => $this->created_at->format('m-d-Y'),
            'updated_at' => $this->updated_at,
            'items' => PurchaseOrderItemResource::collection($this->items),
            'approval' => new PurchaseOrderApprovalResource($this->approval),
            'downpayment' => $this->downpayment,
            'payment_upon_delivery' => $this->payment_upon_delivery,
            'pdc' => $this->pdc,
            'grand_total' => $this->grand_total,
        ];
    }
}
