<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerCourierRequirmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => isset($this->id) ? $this->id : '',
            'customer_id' => isset($this->customer_id) ? $this->customer_id : '',
            'registered_name' => isset($this->customer->registered_name) ? $this->customer->registered_name : '',
            'customer_channel' => isset($this->customer->customer_channel) ? $this->customer->customer_channel : '',
            'shipping_address_id' => isset($this->shipping_address_id) ? $this->shipping_address_id : '',
            'lba_name' => isset($this->shipping_address->lba_name) ? $this->shipping_address->lba_name : '',
            'lba_code' => isset($this->shipping_address->lba_code) ? $this->shipping_address->lba_code : '',
            'street' => isset($this->shipping_address->street) ? $this->shipping_address->street : '',
            'barangay' => isset($this->shipping_address->barangay) ? $this->shipping_address->barangay : '',
            'city' => isset($this->shipping_address->city) ? $this->shipping_address->city : '',
            'province' => isset($this->shipping_address->province) ? $this->shipping_address->province : '',
            'zip' => isset($this->shipping_address->zip) ? $this->shipping_address->zip : '',
            'country' => isset($this->shipping_address->country) ? $this->shipping_address->country : '',
            'contact_number' => isset($this->shipping_address->contact_number) ? $this->shipping_address->contact_number : '',
            'courier_id' => isset($this->courier_id) ? $this->courier_id : '',
            'company_name' => isset($this->courier->company_name) ? $this->courier->company_name : '',
            'booker_id' => isset($this->booker_id) ? $this->booker_id : '', 
            'name' => isset($this->user->name) ? $this->user->name : '',
            'area' => isset($this->area) ? $this->area : '',
            'has_special_rqmts' => isset($this->has_special_rqmts) ? $this->has_special_rqmts : '',
        ];
    }
}
