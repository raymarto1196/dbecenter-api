<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InventoryAdjustmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'iar_code' =>  $this->iar_code,
            'item_id' => isset($this->item_id) ? $this->item_id : '',
            'item_name' => isset($this->items->item_name) ? $this->items->item_name : '',
            'lot_number' => isset($this->lot_number) ? $this->lot_number : '',
            'expiry_date' => isset($this->expiry_date) ? $this->expiry_date : '',
            // 'system_count' => ,
            'audit_actual_count' => isset($this->audit_actual_count) ? $this->audit_actual_count : '',
            'adjusted_quantity' => isset($this->adjusted_quantity) ? $this->adjusted_quantity : '',
            'journal_entry' => isset($this->journal_entry) ? $this->journal_entry : '',
            'reason' => isset($this->reason) ? $this->reason : '',
            'prepared_by' => isset($this->prepared_by) ? $this->prepared_by : '',
            'requestor' =>  isset($this->requestor->name) ? $this->requestor->name : '',
            'adjusted_by' => isset($this->adjusted_by) ? $this->adjusted_by : '',
            'updater' => isset($this->updater->name) ? $this->updater->name : '',
            'approved_by' => isset($this->approved_by) ? $this->approved_by : '',
            'approver' => isset($this->approver->name) ? $this->approver->name : '',
            'status' => isset($this->status) ? $this->status : '',
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,

        ];
    }
}
