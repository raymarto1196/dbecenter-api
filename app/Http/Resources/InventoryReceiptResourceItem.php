<?php

namespace App\Http\Resources;

use App\Models\InventoryReceipt;
use Illuminate\Http\Resources\Json\JsonResource;

class InventoryReceiptResourceItem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => isset($this->id) ? $this->id : '',
            'inventory_receipt_id' => isset($this->inventory_receipt_id) ? $this->inventory_receipt_id : '',
            'item_id' => isset($this->item_id) ? $this->item_id : '',
            'item_name' => isset($this->item_details->item_name) ? $this->item_details->item_name : '',
            'item_code' => isset($this->item_details->item_code) ? $this->item_details->item_code : '',
            'uom' => isset($this->item_details->uom) ? $this->item_details->uom : '',
            'unit_cost' => isset($this->item_details->unit_cost) ? $this->item_details->unit_cost : '',
            'total_inventory' => isset($this->item_details->total_quantity) ? $this->item_details->total_quantity : '',
            'inventory_received' => isset($this->inventory_received) ? $this->inventory_received : '',
            'inventory_retention' => isset($this->inventory_retention) ? $this->inventory_retention : '',
            'lot_number' => isset($this->lot_number) ? $this->lot_number : '',
            'manufacture_date' => isset($this->manufacture_date) ? $this->manufacture_date->format('m-d-Y') : '',
            'expiry_date' => isset($this->expiry_date) ? $this->expiry_date->format('m-d-Y') : '',
            'remarks' => isset($this->remarks) ? $this->remarks : '',
            'warehouse_id' => isset($this->warehouse_id) ? $this->warehouse_id : '',
            'warehouse_name' => isset($this->warehouse->warehouse_name) ? $this->warehouse->warehouse_name : '',
            'warehouse_pallet_id' => isset($this->warehouse_pallet_id) ? $this->warehouse_pallet_id : '',
            'bin_name' => isset($this->warehouse_pallet->bin_name) ? $this->warehouse_pallet->bin_name : '',


        ];
    }
}
