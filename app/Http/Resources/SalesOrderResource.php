<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SalesOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'customer_id'               => $this->customer_id,
            'customer_name'             => $this->customer->registered_name,
            'customer_po_number'        => $this->customer_po_number,
            'reference_number'          => $this->reference_number,
            'estimated_delivery_date'   => $this->estimated_delivery_date,
            'net_amount'                => $this->net_amount,
            'gross_amount'              => $this->gross_amount,
            'received_by'               => $this->received_by,
            'billing_address'           => $this->billing_address,
            'shipping_address'          => $this->shipping_address,
            'payment_terms'             => $this->payment_terms,
            'status'                    => $this->status,
            'special_instruction'       => $this->special_instruction,
            'booker_id'                 => $this->booker_id,
            'booker_name'               => $this->booker->name,
            'courier_id'                => $this->customer->courier_id,
            'courier'                   => $this->customer->courier->company_name,
            'items'                     => SalesOrderItemResource::collection($this->items),
            'statuses'                  => SalesOrderStatusesResource::collection($this->statuses),
        ];
    }
}
