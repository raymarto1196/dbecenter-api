<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PurchaseRequestItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'purchase_request_id' => $this->purchase_order_id,
            'item_id' => $this->item_id,
            'item_name' => $this->item->item_name,
            'item_code' => $this->item->item_code,
            'item_description' => $this->item->item_description,
            'item_segment' => $this->item->item_segment,
            'current_level' => $this->item->total_quantity,
            'vendor_id' => $this->vendor_id,
            'vendor' => $this->vendor->displayname,
            'uom' => $this->uom,
            'quantity' => $this->quantity_ordered,
            'unit_cost' => $this->unit_cost,
            'vendor_lead_time' => floor($this->item->vendor_lead_time/ 30) .' months',
        ];
    }
}
