<?php

namespace App\Http\Resources;

use App\Models\DeliveryReceipt;
use App\Models\Picklist;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class TransportShipmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // $so = DB::table('sales_orders')->where('id', $this->sales_order_id)->first();

        return [
            'id'                         => $this->id,
            'customer_id'                => $this->customer_id,
            'sales_order_id'             => $this->sales_order_id,
            'courier_id'                 => $this->courier_id,
            'delivery_status'            => $this->delivery_status,
            'sales_order_data'           => [
                                                'date_ordered'              => $this->sales_order->created_at,
                                                'customer_id'               => $this->customer_id,
                                                'customer_name'             => $this->customer->registered_name,
                                                'customer_po_number'        => $this->sales_order->customer_po_number,
                                                'reference_number'          => $this->sales_order->reference_number,
                                                'estimated_delivery_date'   => $this->sales_order->estimated_delivery_date,
                                                'net_amount'                => $this->sales_order->net_amount,
                                                'gross_amount'              => $this->sales_order->gross_amount,
                                                'received_by'               => $this->sales_order->received_by,
                                                'billing_address'           => $this->sales_order->billing_address,
                                                'shipping_address'          => $this->sales_order->shipping_address,
                                                'payment_terms'             => $this->sales_order->payment_terms,
                                                'status'                    => $this->sales_order->status,
                                                'booker_id'                 => $this->sales_order->booker_id,
                                                'booker_name'               => $this->sales_order->booker->name,
                                                'courier_id'                => $this->customer->courier_id,
                                                'courier'                   => $this->customer->courier->company_name,
                                            ],
            'picklist_id'                => $this->picklist_id,
            'picklist_data'              => [
                                               'picklist'                   => PicklistResource::collection($this->picklist),
                                            ],
            'days_delay'                 => 0,    
            'parcels'                    => TransportBoxTypeResource::collection($this->box_type),  
        ];
    }
}
