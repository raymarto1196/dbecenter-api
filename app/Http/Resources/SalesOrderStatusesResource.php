<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SalesOrderStatusesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'             => $this->id,
            'sales_order_id' => $this->sales_order_id,
            'status'         => $this->status,
            'description'    => $this->description,
            'remarks'        => $this->remarks,
            'user_id'        => $this->user_id,
        ];
    }
}
