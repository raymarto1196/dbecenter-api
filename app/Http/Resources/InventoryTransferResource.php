<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InventoryTransferResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'itr_code' => $this->itr_code,
            'status' => isset($this->status) ? $this->status : '',
            'reason' => isset($this->reason) ? $this->reason : '',
            'approved_date' => isset($this->approved_date) ? $this->approved_date->format('m-d-Y') : '',
            'approved_by' => isset($this->approved_by) ? $this->approved_by : '',
            'approver' => isset($this->user_approved->name) ? $this->user_approved->name : '',
            'requested_by' => isset($this->requested_by) ? $this->requested_by : '',
            'requestor' => isset($this->user_requested->name) ? $this->user_requested->name : '',
            'created_at' => isset($this->created_at) ? $this->created_at->format('m-d-Y') : '',
            'updated_at' => isset($this->updated_at) ? $this->updated_at : '',
            ///call the method items() in InventoryTransfer.php
            'items' => InventoryTransferItemsResource::collection($this->items),
        ];
    }
}
