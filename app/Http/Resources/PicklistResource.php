<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class PicklistResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $total_quantity = DB::table('sales_order_items')->where('sales_order_id', $this->sales_order_id)->select(DB::raw('SUM(`quantity`) + SUM(`free_goods`)'))->value('total_quantity'); // Updated: By Raymart added where sales_order_id = (sales_order_id))pick_list
        return [
            'id'                            => $this->id,
            'date_ordered'                  => $this->sales_order->created_at,
            'sales_order_id'                => $this->sales_order_id,
            'picklist_reference_number'     => $this->reference_number,
            'sales_order_reference_number'  => $this->sales_order->reference_number,
            'customer_id'                   => $this->customer_id,
            'customer_code'                 => $this->sales_order->customer->customer_code,
            'customer_name'                 => $this->sales_order->customer->registered_name,
            'delivery_receipt_number'       => $this->delivery_receipt_number ?? null,         
            'delivery_receipt_date'         => $this->delivery_receipt_date ?? null,                                   
            'sales_invoice_number'          => $this->sales_invoice_number ?? null,
            'sales_invoice_date'            => $this->sales_invoice_date ?? null,   
            'booker_id'                     => $this->sales_order->booker_id,                                               
            'booker'                        => $this->sales_order->booker->name,
            'days_delay'                    => $this->sales_order->created_at->diffInDays(now()),                                                    
            'status'                        => $this->status,
            'picked_by'                     => $this->picked_by,      
            'picker'                        => $this->picker->name ?? null,
            'booked_by'                     => $this->booked_by,      
            'marker'                        => $this->marker->name ?? null,
            'packed_by'                     => $this->packed_by,      
            'packer'                        => $this->packer->name ?? null,
            'pick_start_date'               => $this->pick_start_date,
            'total_quantity'                => $total_quantity,
            'items'                         => PicklistItemInstructrionResource::collection($this->item_instructions),
            'delivery_receipt'              => DeliveryReceiptResource::collection($this->delivery_receipt),
            'sales_invoice'                 => InvoiceResource::collection($this->invoices),
        ];
    }
}
