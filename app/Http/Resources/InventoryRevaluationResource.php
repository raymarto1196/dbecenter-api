<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InventoryRevaluationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'item_id' => $this->item_id,
            'item_code' => $this->item->item_code,
            'item_name' => $this->item->item_name,
            'lot_number' => isset($this->lot_number) ? $this->lot_number : NULL,
            'revaluation_type' => $this->revaluation_type,
            'irr_code' => $this->irr_code,
            'new_cost' => $this->new_cost,
            'remarks' => $this->remarks,
            'journal_remarks' => $this->journal_remarks,
            'requested_by' =>  $this->requested_by,
            'requestor' =>  isset($this->requestor->name) ? $this->requestor->name : NULL,
            'status' => $this->status,
            'approved_by' => $this->approved_by,
            'approver' =>  isset($this->approver->name) ? $this->approver->name : NULL,
            'uom' => $this->item->uom,
            'valuation_method' => $this->valuation_method,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,

        ];
    }
}
