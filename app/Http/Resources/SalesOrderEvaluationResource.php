<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class SalesOrderEvaluationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'sales_order_id'            => $this->sales_order_id,
            'cc_status'                 => $this->status,
            'approved_reason'           => $this->approved_reason,
            'remark'                    => $this->remark,
            'customer_po_number'        => $this->sales_order->customer_po_number,
            'reference_number'          => $this->sales_order->reference_number,
            'estimated_delivery_date'   => $this->sales_order->estimated_delivery_date,
            'net_amount'                => $this->sales_order->net_amount,
            'gross_amount'              => $this->sales_order->gross_amount,
            'booker_id'                 => $this->sales_order->booker_id,
            'past_due_rate'             => null,
            'dso'                       => null,
            'payment_to_sales_ratio'    => null,
            'system_assestment'         => null,
            'days_delay'                => null,
            'billing_address'           => $this->sales_order->billing_address,
            'shipping_address'          => $this->sales_order->shipping_address,
            'payment_terms'             => $this->sales_order->payment_terms,
            'is_rush'                   => $this->sales_order->is_rush,
            'status'                    => $this->sales_order->status,
            'so_statuses'               => SalesOrderStatusesResource::collection($this->statuses),
        ];
    }
}
