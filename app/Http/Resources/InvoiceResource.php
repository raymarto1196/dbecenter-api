<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class InvoiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $total_quantity = DB::table('delivery_receipt_items')->where('sales_order_id', $this->sales_order_id)->where('delivery_receipt_id', $this->delivery_receipt_id)->select(DB::raw('SUM(`amount`)'))->value('total_quantity'); // Updated: By Raymart added where sales_order_id = (sales_order_id))pick_list
        return [
            'id'                             => $this->id,
            'date_ordered'                   => $this->sales_order->created_at,
            'sales_order_id'                 => $this->sales_order_id,
            'picklist_reference_number'      => $this->reference_number,
            'sales_order_reference_number'   => $this->sales_order->reference_number,
            'customer_code'                  => $this->sales_order->customer->customer_code,
            'customer_name'                  => $this->sales_order->customer->registered_name,
            'customer_address'               => $this->customer_address,
            'terms'                          => $this->terms,          
            'delivery_receipt_number'        => $this->delivery_receipt_number,      
            'sales_invoice_number'           => $this->sales_invoice_number,     
            'sales_invoice_date'             => $this->sales_invoice_date,                                
            'picker'                         => $this->picker->name ?? null,
            'marker'                         => $this->marker->name ?? null,
            'packer'                         => $this->packer->name ?? null,
            'total_quantity'                 => $total_quantity,
            'items'                          => DeliveryReceiptItemResource::collection($this->delivery_receipt_item),
            'less_vat'                       => null,
            'amount_net_of_vat'              => null,
            'pwd_discount'                   => null,
            'amount_due'                     => null,
            'add_vat'                        => null,
            'total_amount_view'              => null,
            'vatable_sales'                  => null,
            'vat_exempted_sales'             => null,
            'zero_rated_sales'               => null,
            'vat_amount'                     => null,
            
        ];
    }
}
