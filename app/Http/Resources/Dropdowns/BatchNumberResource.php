<?php

namespace App\Http\Resources\Dropdowns;

use Illuminate\Http\Resources\Json\JsonResource;

class BatchNumberResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'value' => $this->lot_number,
            'label' => $this->lot_number,
            'code' => $this->lot_number
        ];
    }
}
