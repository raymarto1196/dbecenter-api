<?php

namespace App\Http\Resources\Dropdowns;

use Illuminate\Http\Resources\Json\JsonResource;


class VendorResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'value' => $this->id,
            'label' => $this->displayname,
            'code' => $this->vendorcode
        ];
    }
}
