<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PurchaseRequestApprovalResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'purchase_request_id' => $this->purchase_request_id,
            'approver_1' => $this->approver_1,
            'disapproval_reason1' => $this->disapproval_reason1,
            'approver_2' => $this->approver_2,
            'disapproval_reason2' => $this->disapproval_reason2,
        ];
    }
}
