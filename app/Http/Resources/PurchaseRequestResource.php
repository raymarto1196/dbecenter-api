<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PurchaseRequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'reference_number' => $this->reference_number,
            'department' => $this->department_name->name,
            'date_needed' => isset($this->date_needed) ? $this->date_needed->format('m-d-Y') : '',
            'requested_by' => isset($this->requested_by) ? $this->requested_by : '',
            'requestor' => isset($this->requestor->name) ? $this->requestor->name : '',
            'particulars' =>  $this->particulars,
            'description'=>  $this->description,
            'status' =>  $this->status,
            'vendor' =>  $this->item->vendor->displayname,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'item' => new PurchaseRequestItemResource($this->item),
            'approval' => new PurchaseRequestApprovalResource($this->approval),
        ];
    }
}
