<?php

namespace App\Http\Controllers;

use Closure;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $auth_user;

    protected $code_format;

    public function __construct()
    {
        $this->code_format = config('app.code_format');

        $this->default_response = [
            'error' => false,
            'message' => 'default message',
            'results_count' => 0,
            'results' => []
        ];

        /**
         * CommentAuthor: KenS
         * CommentDate: 09-08-2021.
         * Accessing the session of Authenticated User on the Controller constructor
         * reference: https://laravel.com/docs/5.3/upgrade#5.3-session-in-constructors
         */
        $this->middleware(function (Request $request, Closure $next) {
            $this->auth_user = auth()->user();
            return $next($request);
        });
    }

    /**
     * Return success response
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse(array $response, $code = Response::HTTP_OK): JsonResponse
    {
        $response = array_merge($this->default_response, $response);

        return response()->json($response, $code);
    }

    /**
     * Return generic error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($message, $errors = [], $code = 404): JsonResponse
    {
        $response = [
            'error' => true,
            'message' => $message,
        ];


        if (!empty($errors)) {
            $response['data'] = $errors;
        }


        return response()->json($response, $code);
    }
}
