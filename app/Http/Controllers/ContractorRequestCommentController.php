<?php

namespace App\Http\Controllers;

use App\Models\ContractorRequestComment;
use App\Http\Requests\StoreContractorRequestCommentRequest;
use App\Http\Requests\UpdateContractorRequestCommentRequest;

class ContractorRequestCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreContractorRequestCommentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreContractorRequestCommentRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ContractorRequestComment  $contractorRequestComment
     * @return \Illuminate\Http\Response
     */
    public function show(ContractorRequestComment $contractorRequestComment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ContractorRequestComment  $contractorRequestComment
     * @return \Illuminate\Http\Response
     */
    public function edit(ContractorRequestComment $contractorRequestComment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateContractorRequestCommentRequest  $request
     * @param  \App\Models\ContractorRequestComment  $contractorRequestComment
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateContractorRequestCommentRequest $request, ContractorRequestComment $contractorRequestComment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ContractorRequestComment  $contractorRequestComment
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContractorRequestComment $contractorRequestComment)
    {
        //
    }
}
