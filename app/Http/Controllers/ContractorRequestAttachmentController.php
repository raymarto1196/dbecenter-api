<?php

namespace App\Http\Controllers;

use App\Models\ContractorRequestAttachment;
use App\Http\Requests\StoreContractorRequestAttachmentRequest;
use App\Http\Requests\UpdateContractorRequestAttachmentRequest;

class ContractorRequestAttachmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreContractorRequestAttachmentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreContractorRequestAttachmentRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ContractorRequestAttachment  $contractorRequestAttachment
     * @return \Illuminate\Http\Response
     */
    public function show(ContractorRequestAttachment $contractorRequestAttachment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ContractorRequestAttachment  $contractorRequestAttachment
     * @return \Illuminate\Http\Response
     */
    public function edit(ContractorRequestAttachment $contractorRequestAttachment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateContractorRequestAttachmentRequest  $request
     * @param  \App\Models\ContractorRequestAttachment  $contractorRequestAttachment
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateContractorRequestAttachmentRequest $request, ContractorRequestAttachment $contractorRequestAttachment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ContractorRequestAttachment  $contractorRequestAttachment
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContractorRequestAttachment $contractorRequestAttachment)
    {
        //
    }
}
