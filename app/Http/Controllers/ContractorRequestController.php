<?php

namespace App\Http\Controllers;

use App\Models\ContractorRequest;
use App\Http\Requests\StoreContractorRequestRequest;
use App\Http\Requests\UpdateContractorRequestRequest;

class ContractorRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreContractorRequestRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreContractorRequestRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ContractorRequest  $contractorRequest
     * @return \Illuminate\Http\Response
     */
    public function show(ContractorRequest $contractorRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ContractorRequest  $contractorRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(ContractorRequest $contractorRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateContractorRequestRequest  $request
     * @param  \App\Models\ContractorRequest  $contractorRequest
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateContractorRequestRequest $request, ContractorRequest $contractorRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ContractorRequest  $contractorRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContractorRequest $contractorRequest)
    {
        //
    }
}
