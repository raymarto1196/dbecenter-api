<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Documentation;
use App\Models\DocumentFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class DocumentationController extends Controller
{
    protected $documents = ['file'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $results = Documentation::with('children.document_file')->where(['parent_id' => 0])->get(); 

       $this->params['results_count'] = $results->count();
       $this->params['results'] = $results;

       return $this->sendResponse($this->params, 'Resource successfully retrieved.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([

        ]);

        $data = [
            'parent_id' => $request->input('parent_id', 0),
            'name' => $request->input('name')
    	];

        $documentData = Documentation::create($data);

            if( $documentData->parent_id == 0 ){
                $documentData->mime_type = 'root_folder';
            }
            else{
                $documentData->mime_type = 'sub_folder';
            }

        $documentData->path = 'folder-'.$documentData->id;
        $path = $documentData->path;
  
        Storage::makeDirectory($path);

        $documentData->save();

        $this->params['results'] = $documentData;
        return $this->sendResponse($this->params, 'resource created successfully', false, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $results = Documentation::find($id);

        $results->load('document_file');
        $results->load('children.document_file');

        $this->params['results'] = $results;
        return $this->sendResponse($this->params, 'resource retrieved successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $results = Documentation::find($id);
        if (!$results) return response()->json(['error'=>true, 'message'=>'Requested resource does not exist'], Response::HTTP_NOT_FOUND);
        
        $foldername = $results->path;
        Storage::deleteDirectory($foldername);
        
        $results->delete();
      
        return $this->sendResponse([], 'resource deleted successfully');
    }

    public function add_folder(Request $request,Documentation $document)
    {
        $data = [
            'parent_id' => $document->id,
            'name' => $request->input('name')
    	];

        $documentData = Documentation::create($data);

        if( $documentData->parent_id == 0 ){
            $documentData->mime_type = 'root_folder';
        }
        else{
            $documentData->mime_type = 'sub_folder';
        }

        $find = Documentation::find($document->id);

        $documentData->path = 'folder-'.$documentData->id;
        $path = $documentData->path;

        // $documentData->path = $documentData->name;
        // $path = $document->name.'/'.$documentData->path;

        Storage::makeDirectory($path);

        $documentData->save();

        $this->params['results'] = $documentData;
        return $this->sendResponse($this->params, 'resource created successfully', false, Response::HTTP_CREATED);
    }

    public function upload_files(Request $request,Documentation $document)
    {
		    	$data = $request->validate([
		    		'document_id' => $document,
                    'file_name' => '',
                    'physical_filing' => '',
		    		'source' => '',
                    'received_physicaly' => '',
		    		'last_released'  => '',
                    
		    	]);
                
                if($document->id == 1)
                {
                    $this->params['results'] = $document->id;
                    return $this->sendResponse($this->params, 'resource cannot be under the root folder', true);
                }
             
		    	$results = $document->document_file()->create($data);

                if($request->has('files') && $request->input('files')){
                    $base64_image = $request->input('files');
    
                    // Currently accepts image only
                    if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                        $data = substr($base64_image, strpos($base64_image, ',') + 1);
                        $extension = getFileExtension($base64_image);
                        $file = base64_decode($data);
                        $filename =  'file-id_'.$results->id.'.'.$extension;
                        $results->file = $filename;
    
                        if(Storage::put('folder-'.$document->id.'/'. $filename, $file)){
                            $image_path = 'folder-'.$document->id.'/'.$filename;
                            $results->file_path = $image_path;
                        }
                    }
                }

                $results->save();

                $this->params['results'] = $results;
                return $this->sendResponse($this->params, 'resource created successfully', false, Response::HTTP_CREATED);

    }

    public function move_files(Request $request)
    {
        if ($request->has('document_files') && ($request->input('document_files') != null) && is_array($request->input('document_files'))) {
    		$document_files = $request->input('document_files');

            foreach ($document_files as $key => $data) {
                $data = [
                $obj = DocumentFile::find($data['file_id']),
                $current_path = $obj->file_path,
                $obj->document_id = $data['document_id'],
                // $obj->physical_filing = $data['physical_filing'],
                   		// 'source' => $data['source'],
                    // 'received_physicaly' => Auth::user()->name,
		    		// 'last_released'  => Auth::user()->name,
                
                ];

                $new_path =  'folder-'.$obj->document_id.'/'.$obj->file;
                $obj->file_path = $new_path;
                Storage::move( $current_path,  $new_path);
                $obj->save();
            
	    	}
        }
        $this->params['results'] = $obj;
        return $this->sendResponse($this->params, 'resource created successfully', false, Response::HTTP_CREATED);
    }

    public function update_files(Request $request, DocumentFile $document)
    {
        $data = $request->validate([
            'document_id' => '',
            'file_name' => '',
            'physical_filing' => '',
            'source' => '',
            'received_physicaly' => '',
            'last_released'  => '',
            
        ]);

        $results = $document->update($data);  

        if($request->has('files') && $request->input('files')){
            $base64_image = $request->input('files');

            
            Storage::delete($document->file_path);
            // Currently accepts image only
            if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                $data = substr($base64_image, strpos($base64_image, ',') + 1);
                $extension = getFileExtension($base64_image);
                $file = base64_decode($data);
                $filename =  'file-id_'.$document->id.'.'.$extension;
                $document->file = $filename;

                if(Storage::put('folder-'.$document->document_id.'/'. $filename, $file)){
                    $image_path = 'folder-'.$document->document_id.'/'.$filename;
                    $document->file_path = $image_path;
                }
            }
        }

        $document->save();
        
        $this->params['result'] = $results;
        return $this->sendResponse($this->params, 'resource update successfully.', false, Response::HTTP_CREATED);
    }


    public function file_previews(Request $request,$id)
    {
        $results = DocumentFile::find($id);

        $this->params['results'] = $results;
        return $this->sendResponse($this->params, 'resource retrieved successfully');
    }

    public function destroy_files(Request $request,$id)
    {
        $results = DocumentFile::find($id);
        if (!$results) return response()->json(['error'=>true, 'message'=>'Requested resource does not exist'], Response::HTTP_NOT_FOUND);
                
        $results->delete();

        $file_path = $results->file_path;
        Storage::delete($file_path);

        return $this->sendResponse([], 'resource deleted successfully');
    }
}