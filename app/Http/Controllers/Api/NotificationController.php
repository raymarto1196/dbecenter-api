<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class NotificationController extends Controller
{

	public function __construct()
    {

    }

    public function index(Request $request)
    {
    	$notifications = auth()->user()->notifications;
    	$this->params['notifications'] = $notifications;
        return response()->json($this->params, Response::HTTP_OK);
    }

    /*
    * This is a sample Notification create and broadcast with pusher
    */
    public function store(Request $request)
    {
    	$user = auth()->user();
        $details = [
            'greeting' => 'Hi User',
            'body' => 'Order has been received.',
            'thanks' => 'Thank you for visiting belkenz!',
        ];

        // Use the method to notify user ex: \App\Notifications\OrderReceived
        $notification = $user->notify(new \App\Notifications\OrderReceived($details));
        return response()->json($notification);
    }

    public function update(Request $request, $notification_id)
    {
    	if ($notification_id) {
    		$notif_id = $notification_id;
    		$notification = auth()->user()->unreadNotifications()->where('id', $notif_id)->first();
    		if ($notification) {
    			$notification->markAsRead();
		        return response()->json(['success' => true, 'msg' => 'Notification has been marked read.'], Response::HTTP_OK);
    		}
    	}

        return response()->json(['success' => false, 'msg' => 'Invalid request.'], Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}