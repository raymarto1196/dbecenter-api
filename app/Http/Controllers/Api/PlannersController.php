<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Planner;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PlannersController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $validated = $request->validate([
            "name" => ['required'],
            "note_index" => '',
            "picker" => '',
            "marker" => '',
            "packer" => '',
            "user_group" => ''
        ]);

        $results = Planner::create($validated);

        $this->params['results_count'] = 1;
        $this->params['results'] = $results;

        return $this->sendResponse($this->params, 'Resource created successfully', false, Response::HTTP_CREATED);
    }
}
