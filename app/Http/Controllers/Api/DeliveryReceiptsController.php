<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\DeliveryReceiptResource;
use App\Models\DeliveryReceipt;
use App\Models\DeliveryReceiptItem;
use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Picklist;
use App\Models\SalesOrder;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


class DeliveryReceiptsController extends Controller
{
    private $reference_number = null;

    public function __construct()
    {
        parent::__construct();

        // $this->status = [("0 - Picking, 1 - Picked, 2 - Marking, 3 - Marked, 4 - Packing, 5 - Packed, 6 - On-Hold")];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query  = DeliveryReceipt::query();

        $results = DeliveryReceiptResource::collection($query->get());

        $this->params['results_count'] = $results->count();
        $this->params['results'] = $results;

        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }


    public function store(Request $request, $picklist)
    {      
        $picklist_data = Picklist::findOrFail($picklist);
        $dr_number = sprintf($this->code_format,  (DeliveryReceipt::max('id')+1));
        $sales_order = SalesOrder::findOrFail($picklist_data->sales_order_id);

        $delivery_receipt = [
            'reference_number'         => $picklist_data->reference_number,
            'sales_order_id'           => $picklist_data->sales_order_id,
            'picklist_id'              => $picklist_data->id,
            'customer_id'              => $picklist_data->customer_id,
            'customer_address'         => $sales_order->shipping_address,
            'terms'                    => $sales_order->customer->payment_mode,
            'delivery_receipt_number'  => $request->input('delivery_receipt_number', $dr_number),
            'delivery_receipt_date'    => $request->input('delivery_receipt_date', now()), 
            'picked_by'                => $picklist_data->picked_by,
            'marked_by'                => $picklist_data->marked_by,
            'packed_by'                => $picklist_data->packed_by,
        ];

        $data = DeliveryReceipt::create($delivery_receipt);

        $item_instructions = $sales_order->picklist->item_instructions;

              //check if delivery receipt item has free goods
        //duplicate
        
        for($i = 0; $i < $item_instructions->count(); $i++)
        {
            $item = Item::where('id',$sales_order->picklist->item_instructions[$i]->item_id)->first();

           $delivery_receipt_item = [
               'delivery_receipt_id'    => $data->id,
               'sales_order_id'         => $sales_order->picklist->item_instructions[$i]->sales_order_id,
               'item_id'                => $sales_order->picklist->item_instructions[$i]->item_id,
               'uom'                    => $sales_order->picklist->item_instructions[$i]->uom,
               'lot_number'             => $sales_order->picklist->item_instructions[$i]->lot_number,
               'expiry_date'            => $sales_order->picklist->item_instructions[$i]->expiry_date,
               'quantity'               => $sales_order->picklist->item_instructions[$i]->quantity,
               'free_goods'             => $sales_order->picklist->item_instructions[$i]->free_goods,
               'allocated_quantity'     => $sales_order->picklist->item_instructions[$i]->allocated_quantity,
               'unit_cost'              => $item->selling_price,
               'amount'                 => $sales_order->picklist->item_instructions[$i]->allocated_quantity * $item->selling_price,
           ];

           $result = DeliveryReceiptItem::create($delivery_receipt_item);

           $check_data = DeliveryReceiptItem::where('item_id',$result->item_id)->where('quantity',$result->free_goods)->where('free_goods',0)->first();

               // check if delivery receipt item has free goods
               if($sales_order->picklist->item_instructions[$i]->free_goods != 0 && $check_data === null){
                   
                $delivery_receipt_item = [
                    'delivery_receipt_id'    => $data->id,
                    'sales_order_id'         => $result->sales_order_id,
                    'item_id'                => $result->item_id,
                    'bin_location'           => $result->bin_location,
                    'item_name'              => $result->item_name,
                    'uom'                    => $result->uom,
                    'lot_number'             => $result->lot_number,
                    'expiry_date'            => $result->expiry_date, 
                    'quantity'               => $result->free_goods,
                    'unit_cost'              => 0,
                    'free_goods'             => 0,
                    'allocated_quantity'     => $result->free_goods,
                    'amount'                 => 0
                ];
     
               $free_good =  DeliveryReceiptItem::create($delivery_receipt_item);

               // Minus the free goods to the first array before the generation of the free goods row
               $result->allocated_quantity -= $free_good->quantity;
               $result->amount = $result->allocated_quantity * $item->selling_price;
               $result->save();
           }
        }

        $this->params['results_count'] = 1;   
        $this->params['results'] = $data->load('delivery_receipt_items');
        return $this->sendResponse($this->params, 'Resource successfully created.');
    }

    public function show($picklist, $delivery_receipt)
    {
        $data =  DeliveryReceiptResource::collection(DeliveryReceipt::all())
        ->where('picklist_id',$picklist)
        ->where('id',$delivery_receipt);
        $result = $data->flatten();

        $this->params['results'] = $result;
        return $this->sendResponse($this->params, 'Resource succesresfully retrieved.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$picklist, $delivery_receipt)
    {
        $result = DeliveryReceipt::where('id',$delivery_receipt)->where('picklist_id', $picklist)->first();

        $result->delivery_receipt_number = $request->input('delivery_receipt_number');
        $result->delivery_receipt_date = $request->input('delivery_receipt_date');

        $result->save();
        $this->params['results'] = $result->load('delivery_receipt_items');
        return $this->sendResponse($this->params, 'Resource successfully updated.');
    }

}
