<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\RolePermissions;
use App\Models\StaticModuleRoutes;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class RolePermissionsController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RolePermissions  $rolePermissions
     * @return \Illuminate\Http\Response
     */
    public function show($role_id)
    {
        $this->params['results_count'] = RolePermissions::where('role_id', $role_id)->count();
        $results = RolePermissions::where('role_id', $role_id)->get();
        $this->params['results'] = $results->load('accessRoute');
        return $this->sendResponse($this->params, 'Resource successfully retrieved.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RolePermissions  $rolePermissions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $role_id)
    {
        
        $rolePermissions = StaticModuleRoutes::all()->pluck('id')->toArray();
        $postRolePermissions = array_column($request->input('roles_permissions'), 'static_module_route_id');
        
        // $rolePermissions = $request->input('roles_permissions');
        if (count($postRolePermissions) != count(array_intersect($rolePermissions, $postRolePermissions))) {
            $rule['static_module_route_id'] = ['required', Rule::in($rolePermissions)]; 
        }

        DB::table('role_permissions')->where('role_id', $role_id)->delete();
        $data = [];
        $now = Carbon::now()->toDateTimeString();

        foreach ($request->input('roles_permissions') as $value) {
            array_push($data, [
                'role_id' => $role_id,
                'static_module_route_id' => $value['static_module_route_id'],
                'can_add' => $value['can_add'],
                'can_edit' => $value['can_edit'],
                'can_delete' => $value['can_delete'],
                'can_approve' => $value['can_approve'],
                'created_at' => $now,
                'updated_at' => $now
            ]);
        }

        RolePermissions::insert($data);
       
        return $this->sendResponse($data, 'Resource successfully updated.');
    }
}
