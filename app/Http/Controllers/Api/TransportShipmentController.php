<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\TransportShipmentResource;
use App\Models\DispatchingParcel;
use App\Models\Picklist;
use App\Models\TransportBoxType;
use App\Models\TransportShipment;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TransportShipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = TransportShipmentResource::collection(TransportShipment::all());

        $this->params['total_request'] = $data->count();
        $this->params['results'] = $data;
        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,  $picklist)
    {
        $picklist_data = Picklist::findOrFail($picklist);
        if($request->input('courier_id') != null){
            $validated = $request->validate([
                'courier_update_reason' => 'required',
            ]);
        }
        
        $parcel_header = [
            "picklist_id"           => $picklist_data->id,
            "sales_order_id"        => $picklist_data->sales_order_id,
            "courier_id"            => $request->input('courier_id', $picklist_data->id),
            "courier_update_reason" => $request->input('courier_update_reason'),
            "customer_id"           => $picklist_data->customer_id,
            "delivery_status"       => 'ready_for_dispatching',
        ];

        $data = TransportShipment::create($parcel_header);

        if ($request->has('parcels') && ($request->input('parcels') != null) && is_array($request->input('parcels'))) {
    		$parcels = $request->input('parcels');
            foreach ($parcels as $key => $parcel) {
                
            $parcel_tagging = [
                'courier_id'   => $data->courier_id,
                'quantity'     => $parcel['quantity'],
                'name'         => $parcel['name'],
                'weight'       => $parcel['weight'],
            ];
            
            $data->box_type()->create($parcel_tagging);
            }
        };

        // you are on the status because theres an action status = already pack , for waybill printing , for dispatching, update delivery status ,
        // ready for dispatching, in transit, delivered
        $this->params['results_count'] = 1;
        $this->params['results'] = $data->load('box_type');
        return $this->sendResponse( $this->params, 'Resource updated successfully');

        // return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($picklist, $dispatching_parcel)
    {
        $data =  TransportShipmentResource::collection(TransportShipment::all())
        ->where('picklist_id',$picklist)
        ->where('id',$dispatching_parcel);
        $result = $data->flatten();

        $this->params['results'] = $result;
        return $this->sendResponse($this->params, 'Resource succesresfully retrieved.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function change_status(Request $request,$picklist, $dispatching_parcel)
    {
        $result = TransportShipment::where('id',$dispatching_parcel)->where('picklist_id',$picklist)->first();

        $status = $request->input('status');
        
        if($status == 'print_waybill' &&  $result->delivery_status == 'ready_for_dispatching'){
            $result->delivery_status = $status;
            $result->is_printed = 1;
        }elseif($status == 'dispatched' &&  $result->delivery_status == 'print_waybill'){        
            $validated = $request->validate([
                'waybill_reference_number' => 'array',
            ]);

            $result->waybill_reference_number = $request->input('waybill_reference_number');
            $result->delivery_status = $status;
             
            if ($request->has('parcels') && ($request->input('parcels') != null) && is_array($request->input('parcels'))) {
                $parcels = $request->input('parcels');
                foreach ($parcels as $key => $parcel) {

                $obj = TransportBoxType::find($parcel['id']);
                $get_array[] = $parcel['id'];
                
                if($obj == null){
                    $parcel_tagging = [
                        'transport_shipment_id'    => $result->id,
                        'courier_id'               => $parcel['courier_id'] ,
                        'quantity'                 => $parcel['quantity'],
                        'name'                     => $parcel['name'],
                        'weight'                   => $parcel['weight'],
                    ];
                    $get_id = $result->box_type()->create($parcel_tagging);
                    array_push($get_array,$get_id->id);
                }
                else{
                        $obj->transport_shipment_id     = $result->id;
                        $obj->courier_id                = $parcel['courier_id'];
                        $obj->quantity                  = $parcel['quantity'];
                        $obj->name                      = $parcel['name'];
                        $obj->weight                    = $parcel['weight'];
                        $obj->save();
                   }
                }
                $remove_null = array_filter($get_array);
                TransportBoxType::where('transport_shipment_id', $result->id)->whereNotIn('id',$remove_null)->delete();
                if (empty($request->parcels))
              {
                TransportBoxType::where('transport_shipment_id', $result->id)->delete();
              }
            };

        }elseif($status == 'delivered'){
            $result->delivery_status = $status;
            $result->recieved_by = Auth::user()->id;
            $result->date_delivered = Carbon::now();
        }elseif($status == 'cancelled'){
            $result->delivery_status = $status;
            $result->cancelled_reason = $request->input('cancelled_reason');
        }elseif($status == 'return_to_seller'){
            $result->delivery_status = $status;
        }elseif($status == 'return_document'){
            $result->delivery_status = $status;
        }elseif($status == 'affidavit_of_loss'){
            $result->delivery_status = $status;
        }else{
            $result->delivery_status = $status;
        }

        $result->save();

        $this->params['results_count'] = 1;
        $this->params['results'] = $result;
        return $this->sendResponse( $this->params, 'Resource updated successfully');
    }
}
