<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\UserProfile;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Company;
use Auth;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Exceptions\RoleDoesNotExist;
use Spatie\Permission\Models\Permission;
use Carbon\Carbon;

class UsersController extends Controller
{

    protected $rules = [];
    protected $roles = [];
    protected $password_rules = [
        'required',
        'confirmed',
        'min:6',             // must be at least 6 characters in length
        'regex:/[a-z]/',      // must contain at least one lowercase letter
        'regex:/[A-Z]/',      // must contain at least one uppercase letter
        'regex:/[0-9]/',      // must contain at least one digit
        'regex:/[@$!%*#?&]/', // must contain a special character
    ];

    public function __construct()
    {
        parent::__construct();

        $this->params = [
            'error'                 => false,
            'msg'                   => '',
            'total_active_accounts'        => 0,
            'total_locked_accounts'        => 0,
            'total_inactive_accounts'        => 0,
            'results'               => []
        ];

        $this->user = Auth::user();
        $this->roles = Role::all()->pluck('name')->toArray();
    }


    private function _get_initials($str) {
        $ret = '';
        $words = explode(' ', preg_replace('/\s+/', ' ', $str));
        foreach ($words as $word)
        $ret .= strtoupper($word[0]);
        return $ret;
    }

    

/**
     * @OA\Get(
     * path="/api/v1/users",
     * tags={"Users"},
     * operationId="usersIndex",
     * description="Get List of users",
     * security={
     *       {"api_key": {}}
     *     },
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="List of users successfully retrieved"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/User"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     *
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Display a listing of the Vendor.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {           
        $total_active_accounts = UserProfile::with('user')->active()->get();
        $total_inactive_accounts = UserProfile::with('user')->inactive()->get();
        $this->params['total_active_accounts'] = $total_active_accounts->count();
        $this->params['total_inactive_accounts'] = $total_inactive_accounts->count();
        $this->params['results_count'] = UserProfile::with('user.roles')->count();
        $this->params['results'] = UserProfile::with('user.roles')->get();
        
        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {
        // Validation
        $request->validated();

        $signature_settings = $request->input('signature_settings', 'initials'); // initials OR e_signature
        $department = $request->input('department_id');
        $position = $request->input('position', 'Purchasing Staff');
        $lastname = $request->input('lastname');
        $firstname = $request->input('firstname');
        $middlename = $request->input('middlename');
        $full_name = preg_replace('/\s+/', ' ', "{$firstname} {$middlename} {$lastname}");
        $assigned_roles = $request->input('roles');
        $signature_initials = $this->_get_initials($full_name);

        // Build User Properties
        $user_props = [
            'email' =>  $request->input('email'),
            'password' =>  bcrypt($request->input('password')),
            'name' => $full_name
        ];

        // Create the user
        $user = User::create($user_props);

        // Assign role/s to newly created user
        $user->syncRoles($assigned_roles);

        // Build employee number from user id
        $employee_number = 'BK-'.sprintf('%05d', $user->id); //BK-00001

        // Build User Profile Properties
        $user_profile_props = [
            'employee_number' => $employee_number,
            'lastname' => $lastname,
            'firstname' => $firstname,
            'middlename' => $middlename,
            'position' => $position,
            'department_id' => $department,
            'signature_settings' => $signature_settings,
            'signature_initials' => $signature_initials
        ];

        $user_profile = $user->profile()->create($user_profile_props);


        $results = $user->profile->load('department', 'user', 'user.roles');

        $this->params['results_count'] = 1;
        $this->params['results'] = $results;

        return $this->sendResponse($this->params, 'Resource created successfully', false, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, User $user)
    {
        $results = UserProfile::with('user.roles', 'department')->where('user_id', $user->id)->first();

        $this->params['results_count'] = 1;
        $this->params['results'] = $results;

        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

    public function get_user(){
        $result = $this->user;
        $result->load('profile','company','roles');

        $this->params['results_count'] = 1;
        $this->params['message'] = 'Resource retrieved successfully';
        $this->params['results'] = $result;

        return $this->sendResponse($this->params);
    }

    public function user_profile(Request $request, UserProfile $user_profile){

        $password = $request->input('password');
        
        if(!empty($password)){
            $request->validate(
                [
                    'password' => [
                        'required',
                        'confirmed',
                        'min:6',             // must be at least 6 characters in length
                        'regex:/[a-z]/',      // must contain at least one lowercase letter
                        'regex:/[A-Z]/',      // must contain at least one uppercase letter
                        'regex:/[0-9]/',      // must contain at least one digit
                        'regex:/[@$!%*#?&]/', // must contain a special character
                    ]
                ]
            );
        }

        $user = ['user_id'=> $this->user->id];

        $profile_data = [
            'firstname'     => $request->input('firstname'),
            'middlename'    => $request->input('middlename'),
            'lastname'      => $request->input('lastname'),
            'telephone'     => $request->input('telephone'),
            'address1'      => $request->input('address1'),
            'address2'      => $request->input('address2'),
            'city'          => $request->input('city'),
            'region'        => $request->input('region'),
            'zipcode'       => $request->input('zipcode'),
            'country'       => $request->input('country'),
        ];

        $result = $user_profile->UpdateOrCreate($user,$profile_data);

        if($password){
            $password = bcrypt($password);
            $this->user->update(['password' => $password]);
        }

        if($request->has('profile_picture') && $request->input('profile_picture')){
            $base64_image = $request->input('profile_picture');

            // Currently accepts image only
            if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                $data = substr($base64_image, strpos($base64_image, ',') + 1);
                $extension = getFileExtension($base64_image);
                $file = base64_decode($data);
                $filename =  'userId-'.$result->user_id.'.'.$extension;
                $result->profile_picture = $filename;

                if(Storage::put('user/profile_picture/'. $filename, $file)){
                    $image_path = 'user/profile_picture/'.$filename;
                    $result->profile_picture = $image_path;
                }
            }
        }
        
        $result->save();

        $this->params['results_count'] = 1;
        $this->params['message'] = 'Resource updated successfully';
        $this->params['results'] = $result;

        return $this->sendResponse($this->params);
    }

    public function user_company(Request $request, Company $company){
        $request->validate([
            'name' => 'required',
            'naics_codes' => 'array'
            ]);

        $user = ['user_id'=> $this->user->id];
        $company_data = [
            'name'          => $request->input('name'),
            'description'   => $request->input('description'),
            'telephone'     => $request->input('telephone'),
            'address1'      => $request->input('address1'),
            'address2'      => $request->input('address2'),
            'city'          => $request->input('city'),
            'region'        => $request->input('region'),
            'zipcode'       => $request->input('zipcode'),
            'country'       => $request->input('country'),
            'naics_codes'   => $request->input('naics_codes', []),
            'duns_number'   => $request->input('duns_number'),
            'cage_code'     => $request->input('cage_code'),
        ];

       $result = $company->UpdateOrCreate($user,$company_data);

        if($request->has('logo') && $request->input('logo')){
            $base64_image = $request->input('logo');

            if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                $data = substr($base64_image, strpos($base64_image, ',') + 1);
                $extension = getFileExtension($base64_image);
                $file = base64_decode($data);
                $filename =  'userId-'.$result->user_id.'.'.$extension;
                $result->logo = $filename;

                if(Storage::put('company/logo/'. $filename, $file)){
                    $image_path = 'company/logo/'.$filename;
                    $result->logo = $image_path;
                }
            }
        }
        
        $result->save();

       $this->params['results_count'] = 1;
       $this->params['message'] = 'Resource Updated or Created successfully';
       $this->params['results'] = $result;

       return $this->sendResponse($this->params);
    }

    public function user_company_differentiators(Request $request){
       $request->validate([
        'differentiators' => 'array'
       ]);

       $result = $this->user->company()->update(['differentiators' => $request->input('differentiators', [])]);

       $this->params['results_count'] = 1;
       $this->params['message'] = 'Resource Updated successfully';
       $this->params['results'] = $result;

       return $this->sendResponse($this->params);
    }

    public function user_company_capabilities(Request $request){
        $request->validate([
         'capabilities' => 'array'
        ]);
 
        $result = $this->user->company()->update(['capabilities' => $request->input('capabilities', [])]);
 
        $this->params['results_count'] = 1;
        $this->params['message'] = 'Resource Updated successfully';
        $this->params['results'] = $result;
 
        return $this->sendResponse($this->params);
     }    
     
     public function user_company_references(Request $request){
        $request->validate([
            'references' => 'array'
        ]);
 
        $result = $this->user->company()->update(['references' => $request->input('references', [])]);
 
        $this->params['results_count'] = 1;
        $this->params['message'] = 'Resource Updated successfully';
        $this->params['results'] = $result;
 
        return $this->sendResponse($this->params);
     }    
     
     public function user_company_certifications(Request $request){
        $request->validate([
            'certifications' => 'array'
        ]);
 
        $result = $this->user->company()->update(['certifications' => $request->input('certifications', [])]);
 
        $this->params['results_count'] = 1;
        $this->params['message'] = 'Resource Updated successfully';
        $this->params['results'] = $result;
 
        return $this->sendResponse($this->params);
     }

     public function government_designation(Request $request){
        $request->validate([
            'government_designation' => 'array'
        ]);
 
        $result = $this->user->company()->update(['government_designation' => $request->input('government_designation', [])]);
 
        $this->params['results_count'] = 1;
        $this->params['message'] = 'Resource Updated successfully'; 
        $this->params['results'] = $result;
 
        return $this->sendResponse($this->params);
     }     

    public function update(Request $request, User $user)
    {
        $this->rules['email'] = 'required|email|unique:users,email,'.$user->id.'id';

        if ($user->profile->signature_settings != $request->input('signature_settings') && 'e_signature' == $request->input('signature_settings')) {
            $this->rules['signature_file'] = 'required|mimes:jpeg,jpg,png|max:2048';
        }

        $request->validate($this->rules);

        $signature_settings = $request->input('signature_settings', $user->signature_settings); // initials OR e_signature
        $department = $request->input('department', $user->profile->department);
        $position = $request->input('position',  $user->profile->position);
        $lastname = $request->input('lastname', $user->profile->lastname);
        $firstname = $request->input('firstname', $user->profile->firstname);
        $middlename = $request->input('middlename', $user->middlename);
        $full_name = preg_replace('/\s+/', ' ', "{$firstname} {$middlename} {$lastname}");
        $assigned_roless = $request->input('roles', $user->getRoleNames());

        $signature_initials = $this->_get_initials($full_name);

        // Build User Properties
        $user_props = [
            'email' =>  $request->input('email', $user->email),
            'password' =>  !empty($request->input('password')) ? bcrypt($request->input('password')) : $user->password,
            'name' => $full_name
        ];

        $user->update($user_props);

        // Assign role
        $user->syncRoles($assigned_roless);

        // Build employee number from user id
        $employee_number = $request->input('employee_number', $user->profile->employee_number);

        // Build User Profile Properties
        $user_profile_props = [
            'employee_number' => $employee_number,
            'lastname' => $lastname,
            'firstname' => $firstname,
            'middlename' => $middlename,
            'position' => $position,
            'department' => $department,
            'signature_settings' => $signature_settings,
            'signature_initials' => $signature_initials
        ];

        $user->profile->update($user_profile_props);

        // Upload eSignature file if present

        $results = $user->profile->load('department', 'user', 'user.roles');
        $this->params['results_count'] = 1;
        $this->params['results'] = $results;

        // Update user with selected role assigned
        return $this->sendResponse($this->params, 'Resource updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, User $user)
    {
        $deleted = false;

        DB::beginTransaction();
        try {
            // Change statuses to Inactive
            $user->profile()->update(['status' => 'Inactive']);
            $user->update(['active' => false]);

            // Delete user profile
            $user->profile()->delete();

            // Delete user
            $user->delete();

            // Commmit Transaction
            DB::commit();
            $deleted = true;
        } catch (\Exception $e) {
            DB::rollBack();
        }

        if (! $deleted) {
            return $this->sendResponse([], 'Encountered error while processing');
        }

        return $this->sendResponse([], 'Resource deleted successfully');
    }

    public function change_password(Request $request, User $user) {

        $validated = $request->validate([
            'password' => $this->password_rules
        ]);

        $user->update([
            'password' => bcrypt($validated['password'])
        ]);

        $results = $user->profile->load('department', 'user', 'user.roles');
        $this->params['results_count'] = 1;
        $this->params['results'] = $results;

        return $this->sendResponse($this->params, 'Resource updated successfully');
    }

    public function change_status(Request $request, User $user)
    {
        $user->active = !$user->active;

        $user->save();

        $user->profile()->update([
            'status' => $user->active ? 'Active' : 'Inactive'
        ]);

        $results = $user->profile->load('department', 'user', 'user.roles');
        $this->params['results_count'] = 1;
        $this->params['results'] = $results;

        return $this->sendResponse($this->params, 'Resource updated successfully');
    }

    public function companies(Request $request){
        $results = Company::where([
            [function ($query) use ($request) {
                if (($search_string = $request->search_string)) {
                    $search_string = '%' . $search_string . '%';
                    $query->orWhere('naics_codes', 'LIKE', [$search_string]);
                    // $query->orWhere('identifier', 'LIKE', $search_string);
                    // $query->orWhere('item_type', 'LIKE', $search_string);
                    // $query->orWhere('item_description', 'LIKE', $search_string);
                }
            }]
            ])->get();
    
    
            $this->params['results_count'] = $results->count();
            $this->params['results'] = $results;
            $this->params['message'] = 'Resource retrieved successfully';
    
            return $this->sendResponse($this->params);
    }

    public function user_full_details(Request $request, $user_id){
        $result = User::findOrFail($user_id);
        $result->load(['profile','company','item','roles:id,name']);

        $this->params['message'] = 'Resource retrieved successfully';
        $this->params['results_count'] = 1;
        $this->params['results'] = $result;

        return $this->sendResponse($this->params);
    }
}
