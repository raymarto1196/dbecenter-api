<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PurchaseOrderItemResource;
use App\Http\Resources\PurchaseOrderResource;
use App\Http\Resources\PurchaseRequestResource;
use App\Models\PurchaseOrder;
use App\Models\Department;
use App\Models\Item;
use App\Models\PurchaseOrderApproval;
use App\Models\PurchaseOrderItem;
use App\Models\PurchaseRequest;
use App\Models\PurchaseRequestItem;
use App\Models\Vendor;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class PurchaseOrdersController extends Controller
{

    // public function __construct()
    // {
    //     parent::__construct();
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $results =  PurchaseOrderResource::collection(PurchaseOrder::all());

        $this->params['results_count'] = $results->count();
        $this->params['results'] = $results;
        return $this->sendResponse($this->params, 'Resource retrieved successfully');

    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PurchaseOrder $purchase_order)
    {
        $data = new PurchaseOrderResource($purchase_order);
        $data->load('approval');

        $this->params['results_count'] = 1;
        $this->params['results'] = $data;

        return $this->sendResponse($this->params, 'Resource retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PurchaseOrder $purchase_order)
    {
        // Find the up-approved pr to be merge into the current purchase_order
        $data_to_merge = PurchaseRequest::where('id',$request->input('purchase_request_id'))
        ->with('item')
        ->first();

        if($data_to_merge == null){
            return $this->sendResponse([], 'Resource does not exist');
        }

        // TODO check if the PR_item to be combine to PO has the same vendor_id and lead_time
        $po_item_data = Item::findOrFail($purchase_order->items[0]->item_id)->first();
        $pr_item_data = Item::findOrFail($data_to_merge->item->item_id)->first();

        $po_item_months = floor($po_item_data->vendor_lead_time/ 30);
        $pr_item_months = floor($pr_item_data->vendor_lead_time/ 30);

        if($purchase_order->vendor_id != $data_to_merge->item->vendor_id || $po_item_months != $pr_item_months)
        {
            return $this->sendResponse([], 'Resource does not have the same supplier and lead_time');  
        }

        //if yes add the pr_item_line to po_item_line
        $pr_item_data = [
            'item_id' => $data_to_merge->item->item_id,
            'vendor_id' => $data_to_merge->item->vendor_id,
            'uom' => $data_to_merge->item->uom,
            'quantity_ordered' => $data_to_merge->item->quantity_ordered,
            'free_goods' => $pr_item_data->free_goods,
            'total_quantity' => $data_to_merge->item->quantity_ordered + $pr_item_data->free_goods,
            'unit_cost' => $data_to_merge->item->unit_cost,
            'total' => $data_to_merge->item->quantity_ordered * $data_to_merge->item->unit_cost,
            ];

        $po_data = $purchase_order->items()->create($pr_item_data);

        //sum the current purchase order grand_total and the combined un_approved pr_item_lines total
        //used the current payment scheme of the purchase order just to follow the wireframes - not final can be change,
        $purchase_order->grand_total += $po_data->total;
        $purchase_order->downpayment = $purchase_order->grand_total * $purchase_order->payment_scheme_dp;
        $purchase_order->payment_upon_delivery = $purchase_order->grand_total * $purchase_order->payment_scheme_pud;
        $purchase_order->pdc = $purchase_order->grand_total * $purchase_order->payment_scheme_pdc;
        $purchase_order->save();

        // TODO DELETE THE MERGE unapproved PR
        PurchaseRequest::find($data_to_merge->id)->delete();

        $this->params['results_count'] = 1;
        $this->params['data'] =  $purchase_order->load('items');
        return $this->sendResponse($this->params, 'Resource updated successfully');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function approve_po1(Request $request,PurchaseOrder $purchase_order)
    {
       $status = $request->input('status');

       if (true == $purchase_order->approval->approver_1) {
        return $this->sendResponse([], 'Resource status was already approved');
       }

       if($status == true){
            $purchase_order->approval->approver_1 = $status;
       }else{
            $validated = $request->validate([
                'disapproval_reason' => 'required',
            ]);
            $purchase_order->approval->disapproval_reason1 = $request->disapproval_reason;
            $purchase_order->approval->approver_1 = $status;
            $purchase_order->status = "cancelled";
            $purchase_order->save();
       }

       $purchase_order->approval->save();

       return $this->sendResponse([], 'Resource updated successfully');
    }

    public function approve_po2(Request $request, PurchaseOrder $purchase_order)
    {
        $status = $request->input('status');

        if (false == $purchase_order->approval->approver_1) {
            return $this->sendResponse([], 'Resource approver_1 status is not approved');
        }

        if (true == $purchase_order->approval->approver_2) {
         return $this->sendResponse([], 'Resource status was already approved');
        }
 
        if($status == true){
             $purchase_order->approval->approver_2 = $status;
        }else{
             $validated = $request->validate([
                 'disapproval_reason' => 'required',
             ]);
             $purchase_order->approval->disapproval_reason2 = $request->disapproval_reason;
             $purchase_order->approval->approver_2 = $status;
             $purchase_order->status = "cancelled";
             $purchase_order->save();
        }
 
        $purchase_order->approval->save();
 
        return $this->sendResponse([], 'Resource updated successfully');
    }

    public function approve_po3(Request $request,PurchaseOrder $purchase_order)
    {
        $status = $request->input('status');

        if (false == $purchase_order->approval->approver_1) {
            return $this->sendResponse([], 'Resource approver_1 status is not approved');
        }   

        if (false == $purchase_order->approval->approver_2) {
            return $this->sendResponse([], 'Resource approver_2 status is not approved');
        }

        if (true == $purchase_order->approval->approver_3) {
         return $this->sendResponse([], 'Resource status was already approved');
        }
 
        if($status == true){
             $purchase_order->approval->approver_3 = $status;
             $purchase_order->status = "approved";
        }else{
             $validated = $request->validate([
                 'disapproval_reason' => 'required',
             ]);
             $purchase_order->approval->disapproval_reason3 = $request->disapproval_reason;
             $purchase_order->approval->approver_3 = $status;
             $purchase_order->status = "cancelled";
        }
        
        $purchase_order->approval->save();
        $purchase_order->save();
 
        return $this->sendResponse([], 'Resource updated successfully');
    }

    public function pr_with_same_po_vendor(PurchaseOrder $purchase_order)
    {
        $vendor_id = $purchase_order->vendor_id;

        // $results = PurchaseRequest::where('status','=','pending')
        // ->whereHas('item', function($q) use ($vendor_id) {
        //     $q->where('vendor_id', '=',$vendor_id);
        // })->with('item')->get();

        $results = PurchaseRequestResource::collection(PurchaseRequest::where('status','=','pending')
        ->whereHas('item', function($q) use ($vendor_id) {
            $q->where('vendor_id', '=',$vendor_id);
        })->with('item')->get());

        $this->params['results_count'] = $results->count();
        $this->params['data'] =  $results;
        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

    public function approved_purchase_orders(){
        $results =  PurchaseOrderResource::collection(PurchaseOrder::all())->where('status','approved');
        $results = $results->flatten();
        $this->params['results_count'] = $results->count();
        $this->params['results'] = $results;
        return $this->sendResponse($this->params, 'Resource retrieved successfully');
        
    }

}
 