<?php

namespace App\Http\Controllers\Api;

use App\ActivityLog;
use App\Http\Controllers\Controller;
use App\Models\UserActivityLog;
use App\Models\MaintenanceCodeSetup;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Carbon\Carbon;

use App\Models\Vendor;
use App\Models\VendorCreditTerms;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Console\Input\Input;

class VendorsController extends Controller
{

    protected $documents = ['attach_document','companyprofile','latestaudited','iso','gdp','gmp','lto','proofoflease','listofproduct','listofclient','adr','listofsuppliers','birvat','secdti','mayorspermit'];
    protected $credit_files = ['trade','non_trade'];
    protected $allowedfileExtension = ['pdf','jpg','png','docx'];
    protected $credit_terms = ['trade','non_trade'];

    /**
     * @OA\Get(
     * path="/api/v1/vendors",
     * tags={"Vendor Management"},
     * operationId="VendorsIndex",
     * description="Get List of Vendor",
     * security={
     *       {"api_key": {}}
     *     },
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="List of Vendors successfully retrieved"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/Vendor"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     *
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Display a listing of the Vendor.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //do not delete
        // if (! $this->auth_user->can('list_vendor')) {
        //
        //     return response()->json([
        //         'responseMessage' => 'You do not have the required authorization.',
        //         'responseStatus'  => 403,
        //     ], Response::HTTP_NOT_FOUND
        //
        //   );
        // }
             
        $results = Vendor::where([
                [function ($query) use ($request){
            if (($search_string = $request->search_string)) {

                $search_string = '%' . $search_string . '%';
                $query->orWhere('vendorcode', 'LIKE', $search_string)->get();
                $query->orWhere('registeredbusinessname', 'LIKE', $search_string)->get();
                $query->orWhere('tin', 'LIKE', $search_string)->get();
                $query->orWhere('officeaddress', 'LIKE', $search_string)->get();
                $query->orWhere('email', 'LIKE', $search_string)->get();
            }

            if(($start_date = $request->start_date) && ($end_date = $request->end_date)){

                $start_date = Carbon::createFromFormat('m-d-Y H:i:s', $start_date)->format('Y-m-d H:i:s');
                $end_date = Carbon::createFromFormat('m-d-Y H:i:s', $end_date)->format('Y-m-d H:i:s');
                $query->whereBetween('created_at', [$start_date, $end_date])->get();
            }
        }]
        ])->get();
        // ])->paginate(
        //     $request->pageLimit
        // );

        $this->params['results_accredited'] = $results->where('status',  1)->count();
        $this->params['results_not_accredited'] = $results->where('status', 0)->count();
        $this->params['results_count'] = $results->count();
        $this->params['results'] = $results;
        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    

        // $results = Vendor::all();
        // $this->params['results_accredited'] = $results->where('accredited',  1)->count();
        // $this->params['results_not_accredited'] = $results->where('accredited', 0)->count();
        // $this->params['results_count'] = $results->count();
        // $this->params['results'] = $results;
        // return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }


    /**
     * @OA\Post(
     * path="/api/v1/vendors",
     * tags={"Vendor Management"},
     * operationId="VendorStore",
     * description="Create Vendor",
     * security={
     *       {"api_key": {}}
     *     },
     *  @OA\RequestBody(
     *    required=true,
     *    description="Create Vendor",
     *    @OA\JsonContent(
     *       required={"vendorcode","tin","yearinbusiness","contactnumber","formofbusiness","officeaddress","taxability","country","faxnumber","email","natureofbusiness","companyprofile"},
     *        @OA\Property(property="vendorcode", type="string",  example="001"),
     *        @OA\Property(property="tin", type="string",  example="5001"),
     *        @OA\Property(property="yearinbusiness", type="string",  example="13"),
     *        @OA\Property(property="contactnumber", type="string",  example="093000000"),
     *        @OA\Property(property="formofbusiness", type="string",  example="proprietorship"),
     *        @OA\Property(property="officeaddress", type="string",  example="Manila"),
     *        @OA\Property(property="taxability", type="string",  example="sadas"),
     *        @OA\Property(property="country", type="string",  example="Philippines"),
     *        @OA\Property(property="faxnumber", type="string",  example="0123"),
     *        @OA\Property(property="email", type="email",  example="Jose@gmail.com"),
     *        @OA\Property(property="natureofbusiness", type="string",  example="Jose Dela Cruz"),
     *        @OA\Property(property="registeredbusinessname", type="string",  example="Jose Dela Cruz"),
     *        @OA\Property(property="companyprofile", type="string",  example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
     * @OA\Property(property="latestaudited", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
     * @OA\Property(property="iso", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
     * @OA\Property(property="gdp", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
     * @OA\Property(property="gmp", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
     * @OA\Property(property="lto", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
     * @OA\Property(property="proofoflease", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
     * @OA\Property(property="listofproduct", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
     * @OA\Property(property="listofclient", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
     * @OA\Property(property="adr", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
     * @OA\Property(property="listofsuppliers", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
     * @OA\Property(property="birvat", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
     * @OA\Property(property="secdti", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
     * @OA\Property(property="mayorspermit", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
     *
     *    ),
     * ),
     * @OA\Response(
     *    response=400,
     *    description="Bad Request",
     *    @OA\JsonContent(
     *            @OA\Property(property="success", type="boolean", example=false),
     *       @OA\Property(property="message", type="string", example="Validation Error"),
     *    )
     *  ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     *
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Create New Vendor.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Vendor $model)
    {
        //do not delete
        // if (! $this->auth_user->can('create_vendor')) {
        //
        //     return response()->json([
        //         'responseMessage' => 'You do not have the required authorization.',
        //         'responseStatus'  => 403,
        //     ], Response::HTTP_NOT_FOUND
        //
        //   );
        // }
        if(($birvat_date_issued = $request->birvat_date_issued) && ($birvat_valid_until = $request->birvat_valid_until)  && ($secdti_date_issued = $request->secdti_date_issued) && ($secdti_valid_until = $request->secdti_valid_until) && ($mayorspermit_date_issued = $request->mayorspermit_date_issued) && ($mayorspermit_valid_until = $request->mayorspermit_valid_until) && ($secdti_valid_until = $request->secdti_valid_until) && ($date_established = $request->date_established)){
            $birvat_date_issued = Carbon::createFromFormat('m-d-Y', $birvat_date_issued)->format('Y-m-d');
            $birvat_valid_until = Carbon::createFromFormat('m-d-Y', $birvat_valid_until)->format('Y-m-d');
            $secdti_date_issued = Carbon::createFromFormat('m-d-Y', $secdti_date_issued)->format('Y-m-d');
            $secdti_valid_until = Carbon::createFromFormat('m-d-Y', $secdti_valid_until)->format('Y-m-d');
            $mayorspermit_date_issued = Carbon::createFromFormat('m-d-Y', $mayorspermit_date_issued)->format('Y-m-d');
            $mayorspermit_valid_until = Carbon::createFromFormat('m-d-Y', $mayorspermit_valid_until)->format('Y-m-d');
            $date_established = Carbon::createFromFormat('m-d-Y', $date_established)->format('Y-m-d');
        }

        $validated = $request->validate([
            'vendorcode' => 'required|unique:vendors,vendorcode',
            'registeredbusinessname' => 'required',
            'tin' => 'required',
            'yearinbusiness' => 'required',
            'contactnumber' => 'required',
            'owner' => 'required',
            'officeaddress' => 'required',
            'country' => 'required',
            'faxnumber' => 'required',
            'email' => 'required|email',
            'formofbusiness' => 'required',
            'taxability' => 'required',
            'natureofbusiness' => 'required'
        ]);

        $optional_fields = [
            'firstname' => $request->post('firstname'),
            'birvat_permit_number' => $request->post('birvat_permit_number'),
            'birvat_date_issued' => $birvat_date_issued, 
            'birvat_valid_until' => $birvat_valid_until,
            'secdti_permit_number' => $request->post('secdti_permit_number'),
            'secdti_date_issued' => $secdti_date_issued,
            'secdti_valid_until' => $secdti_valid_until,
            'mayorspermit_permit_number' => $request->post('mayorspermit_permit_number'),
            'mayorspermit_date_issued' => $mayorspermit_date_issued,
            'mayorspermit_valid_until' => $mayorspermit_valid_until,
            'official_representatives' => $request->post('official_representatives'),
            'company_representatives' => $request->post('company_representatives'),
            'satellife_office' => $request->post('satellife_office'),
            'warehouse_address' => $request->post('warehouse_address'),
            'website_address' => $request->post('website_address'),
            'bank_details' => $request->post('bank_details'),
            'iso_certified' => $request->post('iso_certified'),
            'iso_certified_number' => $request->post('iso_certified_number'),
            'date_established' => $date_established,
            'industry_served' => $request->post('industry_served'),
            'organization_status' => $request->post('organization_status'),
            'employees_number' => $request->post('employees_number'),
            'vendor_compliance_rating' => $request->post('vendor_compliance_rating'),
        ];

        $vendor_props = array_merge($validated, $optional_fields);

        $vendor = $model->create($vendor_props);
        $codesetup = MaintenanceCodeSetup::find(2);  
        $vendor->vendorcode = $codesetup->code_prefix.$vendor->vendorcode;

        /**
         * File uploads
         * Upload documents to vendor folder
         * Todo: Check file filze size limit and allowable extension
         */
        $foldername = 'vendors/'.$vendor->vendorcode;

        foreach ($this->documents as $document) {
            if ($file = $request->file($document)) {
                $extension = $file->getClientOriginalExtension();
                $filename = $document.'.'.$extension;
                $vendor->$document = uploadFileToPath($foldername, $file, $filename);
            }
        }

        if ($request->has('credit_terms') && ($request->input('credit_terms') != null) && is_array($request->input('credit_terms'))) {
    		$credit_terms = $request->input('credit_terms');

    		// add credit_terms on vendor
    		foreach ($credit_terms as $key => $terms) {

		    	$credit_terms_data = [
		    		'vendor_id' =>$vendor->id,
                    'item_id' => $terms['item_id'],
                    'down_payment' => $terms['down_payment'],
                    'upon_delivery' => $terms['upon_delivery'],
                    'days_upon_delivery' => $terms['days_upon_delivery'],
                    'days_percentage' => $terms['days_percentage']
		    	];

		    	$results = $vendor->credit_terms()->create($credit_terms_data);

                $foldername = 'vendors/'.$vendor->vendorcode.'/'.$results->id;
            
            }

        }

        // Resave Item to attach the documents uploaded
        $vendor->save();

        $this->params['results_count'] = 1;
        $this->params['results'] = $vendor->load('credit_terms');

        return $this->sendResponse($this->params, 'Resource created successfully', false, Response::HTTP_CREATED);
    }

    /**
     * @OA\PUT(
     * path="/api/v1/vendors/{id}",
     * tags={"Vendor Management"},
     * operationId="VendorUpdate",
     * description="Update a Vendor by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="Vendor id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *  @OA\RequestBody(
     *    required=true,
     *    description="Update a Vendor by ID",
     *    @OA\JsonContent(
     *       required={"vendorcode","tin","yearinbusiness","contactnumber","formofbusiness","officeaddress","taxability","country""faxnumber","email","natureofbusiness","companyprofile",},
     *        @OA\Property(property="vendorcode", type="string",  example="001"),
     *        @OA\Property(property="tin", type="string",  example="5001"),
     *        @OA\Property(property="yearinbusiness", type="string",  example="13"),
     *        @OA\Property(property="contactnumber", type="string",  example="093000000"),
     *        @OA\Property(property="formofbusiness", type="string",  example="proprietorship"),
     *        @OA\Property(property="officeaddress", type="string",  example="Manila"),
     *        @OA\Property(property="taxability", type="string",  example="sadas"),
     *        @OA\Property(property="country", type="string",  example="Philippines"),
     *        @OA\Property(property="faxnumber", type="string",  example="0123"),
     *        @OA\Property(property="email", type="email",  example="Jose@gmail.com"),
     *        @OA\Property(property="natureofbusiness", type="string",  example="Jose Dela Cruz"),
     *        @OA\Property(property="registeredbusinessname", type="string",  example="Jose Dela Cruz"),
     *        @OA\Property(property="companyprofile", type="string",  example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
            * @OA\Property(property="latestaudited", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
            * @OA\Property(property="iso", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
            * @OA\Property(property="gdp", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
            * @OA\Property(property="gmp", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
            * @OA\Property(property="lto", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
            * @OA\Property(property="proofoflease", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
            * @OA\Property(property="listofproduct", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
            * @OA\Property(property="listofclient", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
            * @OA\Property(property="adr", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
            * @OA\Property(property="listofsuppliers", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
            * @OA\Property(property="birvat", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
            * @OA\Property(property="secdti", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
            * @OA\Property(property="mayorspermit", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="Vendor successfully updated"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/Vendor"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=400,
     *    description="Bad Request",
     *    @OA\JsonContent(
     *            @OA\Property(property="success", type="boolean", example=false),
     *       @OA\Property(property="message", type="string", example="Validation Error"),
     *    )
     *  ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description="Vendor Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example="Vendor Found"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Update a Vendor by ID
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vendor $vendor)
    {
        //do not delete
        // if (! $this->auth_user->can('update_vendor')) {
        //
        //     return response()->json([
        //         'responseMessage' => 'You do not have the required authorization.',
        //         'responseStatus'  => 403,
        //     ], Response::HTTP_NOT_FOUND
        //
        //   );
        // }
        if(($birvat_date_issued = $request->birvat_date_issued) && ($birvat_valid_until = $request->birvat_valid_until)  && ($secdti_date_issued = $request->secdti_date_issued) && ($secdti_valid_until = $request->secdti_valid_until) && ($mayorspermit_date_issued = $request->mayorspermit_date_issued) && ($mayorspermit_valid_until = $request->mayorspermit_valid_until) && ($secdti_valid_until = $request->secdti_valid_until) && ($date_established = $request->date_established)){
            $birvat_date_issued = Carbon::createFromFormat('m-d-Y', $birvat_date_issued)->format('Y-m-d');
            $birvat_valid_until = Carbon::createFromFormat('m-d-Y', $birvat_valid_until)->format('Y-m-d');
            $secdti_date_issued = Carbon::createFromFormat('m-d-Y', $secdti_date_issued)->format('Y-m-d');
            $secdti_valid_until = Carbon::createFromFormat('m-d-Y', $secdti_valid_until)->format('Y-m-d');
            $mayorspermit_date_issued = Carbon::createFromFormat('m-d-Y', $mayorspermit_date_issued)->format('Y-m-d');
            $mayorspermit_valid_until = Carbon::createFromFormat('m-d-Y', $mayorspermit_valid_until)->format('Y-m-d');
            $date_established = Carbon::createFromFormat('m-d-Y', $date_established)->format('Y-m-d');
        }

        $validated = $request->validate([
            'vendorcode' => 'required|unique:vendors,vendorcode,'.$vendor->id.'id',
            'registeredbusinessname' => 'required',
            'tin' => 'required',
            'yearinbusiness' => 'required',
            'contactnumber' => 'required',
            'formofbusiness' => 'required',
            'officeaddress' => 'required',
            'taxability' => 'required',
            'country' => 'required',
            'faxnumber' => 'required',
            'email' => 'required|email',
            'natureofbusiness' => 'required'
        ]);

        $optional_fields = [
            'firstname' => $request->post('firstname'),
            'birvat_permit_number' => $request->post('birvat_permit_number'),
            'birvat_date_issued' => $birvat_date_issued, 
            'birvat_valid_until' => $birvat_valid_until,
            'secdti_permit_number' => $request->post('secdti_permit_number'),
            'secdti_date_issued' => $secdti_date_issued,
            'secdti_valid_until' => $secdti_valid_until,
            'mayorspermit_permit_number' => $request->post('mayorspermit_permit_number'),
            'mayorspermit_date_issued' => $mayorspermit_date_issued,
            'mayorspermit_valid_until' => $mayorspermit_valid_until,
            'official_representatives' => $request->post('official_representatives'),
            'company_representatives' => $request->post('company_representatives'),
            'satellife_office' => $request->post('satellife_office'),
            'warehouse_address' => $request->post('warehouse_address'),
            'website_address' => $request->post('website_address'),
            'bank_details' => $request->post('bank_details'),
            'iso_certified' => $request->post('iso_certified'),
            'iso_certified_number' => $request->post('iso_certified_number'),
            'date_established' => $date_established,
            'industry_served' => $request->post('industry_served'),
            'organization_status' => $request->post('organization_status'),
            'employees_number' => $request->post('employees_number'),
            'vendor_compliance_rating' => $request->post('vendor_compliance_rating'),
        ];

        $vendor_props = array_merge($validated, $optional_fields);

        $vendor->update($vendor_props);

        /**
         * File uploads
         * Upload documents to vendor folder
         * Todo: Check file filze size limit and allowable extension
         */
        $foldername = 'vendors/'.$vendor->vendorcode;

        foreach ($this->documents as $document) {
            if ($file = $request->file($document)) {
                $extension = $file->getClientOriginalExtension();
                $filename = $document.'.'.$extension;
                $vendor->$document = uploadFileToPath($foldername, $file, $filename);
            }
        }

        if ($request->has('credit_terms') && ($request->input('credit_terms') != null) && is_array($request->input('credit_terms'))) {
    		$credit_terms = $request->input('credit_terms');

    		// add credit_terms on vendor
    		foreach ($credit_terms as $key => $terms) {

                $obj = VendorCreditTerms::find($terms['id']);
                $get_array[] = $terms['id'];
                if($obj == null){

                    $credit_terms_data = [
                        'vendor_id' =>$vendor->id,
                        'item_id' => $terms['item_id'],
                        'down_payment' => $terms['down_payment'],
                        'upon_delivery' => $terms['upon_delivery'],
                        'days_upon_delivery' => $terms['days_upon_delivery'],
                        'days_percentage' => $terms['days_percentage']
                    ];

                   $get_id = $vendor->credit_terms()->create($credit_terms_data);
                   array_push($get_array,$get_id->id);
                }

                else{
                    $obj->vendor_id = $vendor->id;
                    $obj->item_id = $terms['item_id'];
                    $obj->down_payment = $terms['down_payment'];
                    $obj->upon_delivery = $terms['upon_delivery'];
                    $obj->days_upon_delivery = $terms['days_upon_delivery'];
                    $obj->days_percentage = $terms['days_percentage'];
                    $obj->save();
            }   
        };
            $remove_null = array_filter($get_array);
            VendorCreditTerms::where('vendor_id', $vendor->id)->whereNotIn('id',$remove_null)->delete();
        }

        if (empty($request->credit_terms))
        {
            VendorCreditTerms::where('vendor_id', $vendor->id)->delete();
        }

        $vendor->save();

        $this->params['results_count'] = 1;
        $this->params['results'] = $vendor->load('credit_terms');

        return $this->sendResponse($this->params, 'Resource updated successfully');
    }

    /**
     * @OA\Get(
     * path="/api/v1/vendors/{id}",
     * tags={"Vendor Management"},
     * operationId="VendorShow",
     * description="Get a Vendor by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="Vendor id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="department successfully retrieved"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/Vendor"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description="Vendor Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example="Vendor Not Found"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Get Vendor By ID.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Vendor $vendor)
    {
        //do not delete
        // if (! $this->auth_user->can('read_vendor')) {
        //
        //     return response()->json([
        //         'responseMessage' => 'You do not have the required authorization.',
        //         'responseStatus'  => 403,
        //     ], Response::HTTP_NOT_FOUND
        //
        //   );
        // }

        $this->params['results_count'] = 1;
        $this->params['results'] = $vendor->load('credit_terms');
        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

    /**
     * @OA\Delete(
     * path="/api/v1/vendors/{id}",
     * tags={"Vendor Management"},
     * operationId="departmentDelete",
     * description="Delete a Vendor by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="Vendor",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      *  @OA\RequestBody(
     *    required=true,
     *    description="Update a Role by ID",
     *    @OA\JsonContent(
     *       required={"reason"},
     *       @OA\Property(property="reason", type="integer",  example="sample"),
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="Vendor successfully deleted"),
     *        )
     *     ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description="Vendor Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example="Vendor Not Found"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Delete a Vendor by ID
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //do not delete
        // if (! $this->auth_user->can('list_warehouse')) {
        //
        //     return response()->json([
        //         'responseMessage' => 'You do not have the required authorization.',
        //         'responseStatus'  => 403,
        //     ], Response::HTTP_NOT_FOUND
        //
        //   );
        // }

        $request->validate([
            'reason' => 'required'
        ]);

        $vendor = Vendor::find($id);
        if (!$vendor) return response()->json(['error'=>true, 'message'=>'Requested vendor does not exist'], Response::HTTP_NOT_FOUND);

        // Delete the selected vendor: Softdelete
        $vendor->delete();

        // Append record to user activity logs
        UserActivityLog::create([
            'user_id' => auth()->user()->id,
            'ip_address' => $request->ip(),
            'user_agent' => $request->userAgent(),
            'description' => 'Deleted vendor: '.$vendor->vendorcode,
            'loggable_id' => $vendor->id,
            'loggable_type' => Vendor::class,
            'reason' => $request->post('reason')
        ]);

        return $this->sendResponse([], ' Resource successfully deleted.');
    }
          /**
     * @OA\PUT(
     * path="/api/v1/vendors/{id}/change_to_credited",
     * tags={"Vendor Management"},
     * operationId="VendorChange_to_credited",
     * description="Change_to_credited a Vendor by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="Vendor id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *  @OA\RequestBody(
     *    required=true,
     *    description="Change_to_credited a Vendor by ID",
     *    @OA\JsonContent(
     *       required={"accredited"},
     *        @OA\Property(property="accredited", type="boolean",  example=1),
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="Vendor successfully Change_to_creditedd"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/Vendor"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=400,
     *    description="Bad Request",
     *    @OA\JsonContent(
     *            @OA\Property(property="success", type="boolean", example=false),
     *       @OA\Property(property="message", type="string", example="Validation Error"),
     *    )
     *  ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description="Vendor Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example="Vendor Found"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Change_to_credited a Vendor by ID
     *
     * @return \Illuminate\Http\Response
     */
    public function change_to_credited(Request $request, Vendor $vendor)
    {
        $vendor->status = 1;
        $vendor->save();
        return $this->sendResponse([], ' Resource successfully added to credited vendors.');
    }

    /**
     * @OA\PUT(
     * path="/api/v1/vendors/{id}/change_status",
     * tags={"Vendor Management"},
     * operationId="Vendorchange_status",
     * description="change_status a Vendor by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="Vendor id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *  @OA\RequestBody(
     *    required=true,
     *    description="change_status a Vendor by ID",
     *    @OA\JsonContent(
     *       required={"active"},
     *        @OA\Property(property="active", type="boolean",  example=1),
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="Vendor successfully change_status"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/Vendor"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=400,
     *    description="Bad Request",
     *    @OA\JsonContent(
     *            @OA\Property(property="success", type="boolean", example=false),
     *       @OA\Property(property="message", type="string", example="Validation Error"),
     *    )
     *  ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description="Vendor Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example="Vendor Found"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * change_status a Vendor by ID
     *
     * @return \Illuminate\Http\Response
     */
    public function change_status(Request $request, Vendor $vendor)
    {
        $vendor->active = !$vendor->active;
        $vendor->save();
        return $this->sendResponse($vendor, 'Resource successfully updated');
    }

    public function change_statuses(Request $request)
    {
        if  ($request->input('vendor_ids')) {
            $vendors = Vendor::whereIn('id', $request->input('vendor_ids'))->get();
            foreach ($vendors as $vendor) {
                $vendor->active = !$vendor->active;
                $vendor->save();
            }
        
        }
        return $this->sendResponse([], 'Resource successfully updated');

    }

}
