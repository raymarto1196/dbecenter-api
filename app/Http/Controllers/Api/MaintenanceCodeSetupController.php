<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\MaintenanceCodeSetup;

class MaintenanceCodeSetupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query  = MaintenanceCodeSetup::query();

        $results = $query->get();

        $this->params['results_count'] = $results->count();
        $this->params['results'] = $results;

        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'module' => 'required',
            'file_name' => 'required',
            'description' => 'required',
            'code_prefix' => ''
     
        ]);

        $fieldcustomization = MaintenanceCodeSetup::create($validated);

        $this->params['results_count'] = 1;
        $this->params['results'] = $fieldcustomization;

        return $this->sendResponse($this->params, 'Resource created successfully.', false, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $restults = MaintenanceCodeSetup::find($id);
        $this->params['results_count'] = 1;
        $this->params['results'] = $restults;

        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'description' => '',
            'code_prefix' => ''
     
        ]);

        $fieldcustomization = MaintenanceCodeSetup::find($id);
        $results = $fieldcustomization->update($validated);

        $this->params['results_count'] = 1;
        $this->params['results'] = $results;

        return $this->sendResponse($this->params, 'Resource updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = MaintenanceCodeSetup::find($id);
        $customer->delete();

        return $this->sendResponse([], 'Resource deleted successfully');
    }
}
