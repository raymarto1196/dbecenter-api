<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\ItemRating;
use Illuminate\Http\Request;

class ItemRatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            'user_id' => $this->auth_user->id,
            'item_id' => $request->input('item_id'),
            'rate'    => $request->input('rate'),
            'comment' => $request->input('comment'),
        ];

        $result = ItemRating::Create($data);

        $this->params['message'] = 'Resource created successfully';
        $this->params['results_count'] = 1;
        $this->params['results'] = $result;

        return $this->sendResponse($this->params);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = ItemRating::findOrFail($id);

        $this->params['message'] = 'Resource retrieved successfully';
        $this->params['results_count'] = 1;
        $this->params['results'] = $result;

        return $this->sendResponse($this->params);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,ItemRating $item_rating)
    {
        $data = [
            'rate'    => $request->input('rate'),
            'comment' => $request->input('comment'),
        ];

        $result = $item_rating->update($data);

        $this->params['message'] = 'Resource updated successfully';
        $this->params['results_count'] = 1;
        $this->params['results'] = $result;

        return $this->sendResponse($this->params);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = ItemRating::findOrFail($id);
        $result->delete();

        $this->params['message'] = 'Resource deleted successfully';
        $this->params['results_count'] = 1;
        $this->params['results'] = true;
        return $this->sendResponse($this->params);
    }

    public function get_item_ratings($item_id)
    {
        $result = Item::findOrFail($item_id);

        $this->params['message'] = 'Resource retrieved successfully';
        $this->params['results_count'] = 1;
        $this->params['results'] = $result->load('item_ratings.user.profile:user_id,firstname,lastname,profile_picture');

        return $this->sendResponse($this->params);
    }
}
