<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\MaintenanceCodeSetup;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Carbon\Carbon;
use App\Exceptions\Handler;
use App\Models\User;
use App\Models\Warehouse;
use App\Models\WarehousePallet;
use Illuminate\Support\Facades\Storage;

class UserItemsController extends Controller
{
    protected $documents = ['small_box_image','big_box_image','sell_unit_box_image','sell_pack_box_image','cpr_document','assay_test', 'fda_document', 'vendor_document', 'artwork', 'box_specification', 'item_image'];
    protected $allowedfileExtension = ['pdf','jpg','png','docx'];

    public function docs()
    {
        return [];
    }

    /**
    * Display a listing of the Items.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request, User $user)
    {
        //do not delete
        // if (! $this->auth_user->can('list_item')) {
        //
        //     return response()->json([
        //         'responseMessage' => 'You do not have the required authorization.',
        //         'responseStatus'  => 403,
        //     ], Response::HTTP_NOT_FOUND
        //
        //   );
        // }
        // Role::find(3)->syncPermissions([3,4, 'list_sales_order']);

        // return response()->json($this->auth_user->can('update_inventory_receipt'));
        // return response()->json($this->auth_user->getAllPermissions());
        // $query = Item::query();

        // if ($request->exclude_ids && is_array($request->exclude_ids)) {
        //     $query->whereNotIn('id', $request->exclude_ids);
        // }

        // return $this->sendResponse($this->params, 'Resource retrieved successfully');

        $results = $user->item;

        $this->params['results_count'] = $results->count();
        $this->params['message'] = 'Resource Retrieved successfully';
        $this->params['results'] = $results;
        return $this->sendResponse($this->params);
    }


    /**
     * Create New Items.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Item $model)
    {
        //IMPORTANT: DO NOT DELETE
        // if (! $this->auth_user->can('create_item')) {
        //
        //     return response()->json([
        //         'responseMessage' => 'You do not have the required authorization.',
        //         'responseStatus'  => 403,
        //     ], Response::HTTP_NOT_FOUND
        //
        //   );
        // }

        // $request->validate([]);

        $data = [
            'vendor_id' => $this->auth_user->id,
            'identifier' => $request->input('identifier', 'SAMPLEONLY'),
            'item_type' => strtoupper($request->input('item_type', 'SERVICES')),
            'category_id' => $request->input('category_id', 1),
            'item_name' => $request->input('item_name', 'SAMPLE' . date('YmdHqis')),
            'item_description' => $request->input('item_description', 'SAMPLEDESCRIPTION'),
            'status' =>'set by client'
        ];

        // Save item properties
        $item = $model->create($data);

        $this->params['results_count'] = 1;
        $this->params['results'] = $item;

        return $this->sendResponse($this->params, Response::HTTP_CREATED);
    }

    /**
     * Get Item By ID.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Item $item)
    {
        //do not delete
        // if (! $this->auth_user->can('read_item')) {
        //
        //     return response()->json([
        //         'responseMessage' => 'You do not have the required authorization.',
        //         'responseStatus'  => 403,
        //     ], Response::HTTP_NOT_FOUND
        //
        //   );
        // }

        $this->params['results_count'] = 1;
        $this->params['results'] = $item->load('vendor:id,name');
        $this->params['message'] = 'Resource retrieved successfully';

        return $this->sendResponse($this->params);
    }

    /**
     * Update Item by ID
     *
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Item $item)
    {
        //do not delete
        // if (! $this->auth_user->can('update_item')) {
        //
        //     return response()->json([
        //         'responseMessage' => 'You do not have the required authorization.',
        //         'responseStatus'  => 403,
        //     ], Response::HTTP_NOT_FOUND
        //
        //   );
        // }

        // if (($cpr_expiry = $request->cpr_expiry)) {
        //     $cpr_expiry = Carbon::createFromFormat('m-d-Y', $cpr_expiry)->format('Y-m-d');
        //     $item->cpr_expiry = $cpr_expiry;
        // }

        $data = [
            'vendor_id' => $this->auth_user->id,
            'identifier' => $request->input('identifier'),
            'item_type' => strtoupper($request->input('item_type')),
            'category_id' => $request->input('category_id'),
            'item_name' => $request->input('item_name'),
            'item_description' => $request->input('item_description')
        ];

        $item->update($data);

        /**
         * File uploads
         * Upload documents to items folder
         */

        // $foldername = 'items/item-'.$item->id.'/';
        // foreach ($this->documents as $document) {
        //     /**
        //      * Store file uploaded from Base64
        //      */
        //     if ($request->has($document) && $request->input($document)) {
        //         $base64_image = $request->input($document);

        //         // Currently accepts image only
        //         if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
        //             $data = substr($base64_image, strpos($base64_image, ',') + 1);
        //             $extension = getFileExtension($base64_image);
        //             $file = base64_decode($data);
        //             $filename = $document.'.'.$extension;

        //             if (Storage::put($foldername. $filename, $file)) {
        //                 $image_path = $foldername.$filename;
        //                 $item->$document = $image_path;
        //             }
        //         }
        //     }
        // }

        // Resave model to attach the documents uploaded
        $item->save();

        $this->params['message'] = 'Resource updated successfully';
        $this->params['results_count'] = 1;
        $this->params['results'] = $item;

        return $this->sendResponse($this->params, );
    }

    /**
    * Delete a Item by ID
    *
    * @return \Illuminate\Http\Response
    */
    public function destroy(Item $item)
    {
        //do not delete
        // if (! $this->auth_user->can('delete_item')) {
        //
        //     return response()->json([
        //         'responseMessage' => 'You do not have the required authorization.',
        //         'responseStatus'  => 403,
        //     ], Response::HTTP_NOT_FOUND
        //
        //   );
        // }

        // TODO: Must implement event ItemDeleted for detailed processing...
        $item->delete();
        $this->params['message'] = 'Resource deleted successfully';
        return $this->sendResponse($this->params);
    }
}
