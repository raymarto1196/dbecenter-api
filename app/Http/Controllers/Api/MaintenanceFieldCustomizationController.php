<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\MaintenanceFieldCustomization;

class MaintenanceFieldCustomizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query  = MaintenanceFieldCustomization::query();

        if ($request->input('module')) {
            $query->where('module', $request->input('module'));
        }

        if ($request->input('field_name')) {
            $query->where('field_name', $request->input('field_name'));
        }

        $results = $query->get()->groupBy('module');

        $this->params['results_count'] = $results->count();
        $this->params['results'] = $results;

        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'module' => 'required',
            'field_name' => 'required',
            'description' => 'required',
            'values' => 'array'
     
        ]);

        $fieldcustomization = MaintenanceFieldCustomization::create($validated);

        $this->params['results_count'] = 1;
        $this->params['results'] = $fieldcustomization;

        return $this->sendResponse($this->params, 'Resource created successfully.', false, Response::HTTP_CREATED);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, MaintenanceFieldCustomization $field_customization)
    {
        $this->params['results_count'] = 1;
        $this->params['results'] = $field_customization;

        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'description' => '',
            'values' => 'array'
     
        ]);

        $fieldcustomization = MaintenanceFieldCustomization::find($id);
        $results = $fieldcustomization->update($validated);

        $this->params['results_count'] = 1;
        $this->params['results'] = $results;

        return $this->sendResponse($this->params, 'Resource updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $customer = MaintenanceFieldCustomization::find($id);
        $customer->delete();

        return $this->sendResponse([], 'Resource deleted successfully');
    }
}
