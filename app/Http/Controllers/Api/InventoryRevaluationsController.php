<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\InventoryRevaluationResource;
use App\Http\Resources\ItemBatchResource;
use App\Models\InventoryRevaluation;
use App\Models\Item;
use App\Models\ItemBatch;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Carbon\Carbon;

class InventoryRevaluationsController extends Controller
{
    public function __construct() {}

    /**
     * @OA\Get(
     * path="/api/v1/inventory/revaluations",
     * tags={"Inventory Revaluations"},
     * operationId="inventory revaluationsIndex",
     * description="Get List of inventory revaluations",
     * security={
     *       {"api_key": {}}
     *     },
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="List of inventory revaluations successfully retrieved"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/InventoryRevaluation"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     *
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Display a listing of the inventory revaluations.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        // $filter_by_status = $request->has('status');

        // $query = InventoryRevaluation::with('item');

        // if ($filter_by_status) {
        //     $status = $request->input('status') == 1 ? 'approved' : 'pending';
        //     $query->whereStatus($status);
        // }
     
        $results =  InventoryRevaluationResource::collection(InventoryRevaluation::where([
            [function ($query) use ($request){
                if (($search_string = $request->search_string)) {
                    $search_string = '%' . $search_string . '%';
                    $query->orWhere('irr_code', 'LIKE', $search_string);
                }
                if(($start_date = $request->start_date) && ($end_date = $request->end_date)){
                    $start_date = Carbon::createFromFormat('m-d-Y H:i:s', $start_date)->format('Y-m-d H:i:s');
                    $end_date = Carbon::createFromFormat('m-d-Y H:i:s', $end_date)->format('Y-m-d H:i:s');
                    $query->whereBetween('created_at', [$start_date, $end_date]);
                }
                
                if ($request->has('qa_status') && $request->input('qa_status')) {
                    $query->where('qa_status', 'approved');
                }
        
         }]
         ])->get());

        $this->params['results_count'] = $results->count();
        $this->params['results'] = $results;
        return $this->sendResponse($this->params, 'Resource retrieved successfully');

    }

    /**
     * @OA\Post(
     * path="/api/v1/inventory/revaluations",
     * tags={"Inventory Revaluations"},
     * operationId="Inventory RevaluationsStore",
     * description="Create Inventory Revaluations",
     * security={
     *       {"api_key": {}}
     *     },
     *  @OA\RequestBody(
     *    required=true,
     *    description="Create Inventory Revaluations",
     *    @OA\JsonContent(
     *       required={"item_id","valuation_type","new_cost","adjusted_qty","valuation_method","status"},
		*    @OA\Property(property="item_id", type="string", example="1"),
		*  * @OA\Property(property="valuation_type", type="string", example="null"),
		*  * @OA\Property(property="new_cost", type="string", example="1333"),
		*  * @OA\Property(property="adjusted_qty", type="string", example="1"),
		*  * @OA\Property(property="valuation_method", type="string", example="null"),
		*  * @OA\Property(property="status", type="string", example="pending for approval"),
     *    ),
     * ),
     * @OA\Response(
     *    response=400,
     *    description="Bad Request",
     *    @OA\JsonContent(
     *            @OA\Property(property="success", type="boolean", example=false),
     *       @OA\Property(property="message", type="string", example="Validation Error"),
     *    )
     *  ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     *
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Create New Inventory Revaluations.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       $revaluation_type = $request->post('revaluation_type');
       
        if('Price Change' == $revaluation_type){

            $props = [
                'item_id' => $request->post('item_id'),
                'lot_number' => $request->post('lot_number', NUll),
                'revaluation_type' => $revaluation_type,
                'new_cost' => $request->post('new_cost', 0),
                'remarks' => $request->post('remarks', NUll),
                'journal_remarks' => $request->post('journal_remarks', NUll),
                'requested_by' =>  Auth::user()->id,
                'valuation_method' => 'Specific ID',
                'status' => 'pending',
            ];    
         }

         elseif('Other Revaluation' == $revaluation_type){

            $props = [
                'item_id' => $request->post('item_id'),
                'lot_number' => $request->post('lot_number', NUll),
                'revaluation_type' => $revaluation_type,
                'remarks' => $request->post('remarks', NUll),
                'new_cost' => null,
                'journal_remarks' => $request->post('journal_remarks', NUll),
                'requested_by' =>  Auth::user()->id,
                'valuation_method' => 'Specific ID',
                'status' => 'pending'
            ];   
         }

        $revaluation = InventoryRevaluation::create($props);
        $revaluation->irr_code = 'IRRev-'.sprintf('%04d', $revaluation->id);
        $revaluation->save();

        $this->params['results_count'] = 1;
        $this->params['results'] = $revaluation;

        return $this->sendResponse($this->params, 'Resource created successfully', false, Response::HTTP_CREATED);
    }

    public function show(Request $request, $id)
    {
        $data =  InventoryRevaluationResource::collection(InventoryRevaluation::all())
        ->where('id',$id);
        $results = $data->flatten();
        
        $this->params['results_count'] = 1;
        $this->params['result'] = $results;
        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

    public function update(Request $request, InventoryRevaluation $inventory_revaluation)
    {
        if ('approved' == $inventory_revaluation->status) {
            return $this->sendResponse([], 'Resource status was already approved Cannot Update');
        }

        $revaluation_type = $request->post('revaluation_type');
       
        if('Price Change' == $revaluation_type){
            $props = [
                'item_id' => $request->post('item_id'),
                'lot_number' => $request->post('lot_number', NUll),
                'revaluation_type' => $revaluation_type,
                'new_cost' => $request->post('new_cost', 0),
                'remarks' => $request->post('remarks', NUll),
                'journal_remarks' => $request->post('journal_remarks', NUll),
                'valuation_method' => 'Specific ID',
                'status' => 'pending',
            ];    
         }
         elseif('Other Revaluation' == $revaluation_type){
            $props = [
                'item_id' => $request->post('item_id'),
                'lot_number' => $request->post('lot_number', NUll),
                'revaluation_type' => $revaluation_type,
                'remarks' => $request->post('remarks', NUll),
                'new_cost' => null,
                'journal_remarks' => $request->post('journal_remarks', NUll),
                'valuation_method' => 'Specific ID',
                'status' => 'pending'
            ];   
         }

        $results = $inventory_revaluation->update($props);

        $this->params['results_count'] = 1;
        $this->params['result'] = $inventory_revaluation;

        return $this->sendResponse($this->params, 'Resource updated successfully');
    }

    public function destroy(Request $request, InventoryRevaluation $inventory_revaluation)
    {
        if ('approved' == $inventory_revaluation->status) {
            return $this->sendResponse([], 'Cannot delete approved request');
        }

        $inventory_revaluation->delete();
        return $this->sendResponse([], 'Resource deleted successfully');
    }


    public function change_status(Request $request, InventoryRevaluation $inventory_revaluation)
    {
        // $status = '$request->input('status')';
           $status = 'approved';

        // Update only when INVRevaluation is not yet `approved`, else return early
        if ('approved' == $inventory_revaluation->status) {
            return $this->sendResponse([], 'Resource status was already approved');
        }

        if($status == 'approved' && $inventory_revaluation->revaluation_type == 'Price Change'){
            
            $inventory_revaluation->status = 'approved';
            $inventory_revaluation->approved_by =  auth()->user()->id;

            //check if batch is null update all item_batches unit_cost else update a specific itembatch with new_cost
            if($inventory_revaluation->lot_number === NULL){
                $update_batch = ItemBatch::where('item_id',$inventory_revaluation->item_id)
                ->update(['unit_cost' => $inventory_revaluation->new_cost]);
            }
            else{
                $update_batch = ItemBatch::where('item_id',$inventory_revaluation->item_id)
                ->where('lot_number',$inventory_revaluation->lot_number)
                ->first();
    
                $update_batch->unit_cost = $inventory_revaluation->new_cost;
                $update_batch->save();
            }

        }


        elseif($status == 'approved' && $inventory_revaluation->revaluation_type == 'Other Revaluation'){
            $inventory_revaluation->status = 'approved';
            $inventory_revaluation->approved_by =  auth()->user()->id;
        }

        elseif($status == 'cancelled'){

            $inventory_revaluation->status = $status;
            $inventory_revaluation->approved_by = null;

        }

        $inventory_revaluation->save();

        // Todo: Do some additional task when INVRevaluation request is approved.

        $this->params['results_count'] = 1;
        $this->params['result'] = $inventory_revaluation;

        return $this->sendResponse($this->params, 'Resource updated successfully');
    }

    public function get_item_batches(Request $request){
        
        $results =  ItemBatchResource::collection(ItemBatch::where('item_id', 'LIKE', $request->input('item_id'))
        ->where('lot_number', 'LIKE',$request->input('lot_number'))
        ->get());


        if($results->count() == 0)
        {
            return $this->sendResponse([], 'Resource does not exists');
        }
 
        $this->params['results_count'] = $results->count();
        $this->params['results'] = $results;
        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }
}
