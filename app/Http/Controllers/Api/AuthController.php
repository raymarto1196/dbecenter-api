<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\ResetPasswordRequest;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Hash;
use App\Models\RolePermissions;
use App\Models\StaticModuleRoutes;
use App\Models\UserProfile;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Validation\ValidationException;
use ReflectionClass;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class AuthController extends Controller
{

    /**
    * Register New User
    *
    * @return \Illuminate\Http\Response
    */
    public function register(Request $request)
    {
        $input = $request->all();

        $request->validate(
            [
                'username' => 'bail|required|min:4|unique:users',
                'email' => 'required|email|unique:users',
                'firstname' => 'required|string',
                'lastname' => 'required|string',
                'password' => [
                    'required',
                    'confirmed',
                    'min:6',             // must be at least 6 characters in length
                    'regex:/[a-z]/',      // must contain at least one lowercase letter
                    'regex:/[A-Z]/',      // must contain at least one uppercase letter
                    'regex:/[0-9]/',      // must contain at least one digit
                    'regex:/[@$!%*#?&]/', // must contain a special character
                ]
            ]
        );

        $input['password'] = bcrypt($input['password']);

        $role  = strtoupper($input['role'] ?? 'contractor');

        $user = User::create($input)->assignRole($role);

        $user_profile = [
            'user_id' => $user->id,
            'firstname' => $input['firstname'],
            'lastname' => $input['lastname']
        ];

        $user->profile()->create($user_profile);

        $accessToken =  $user->createToken($input['email'])->accessToken;

        $response = [
            'message'   => 'Successfully registered.',
            'accessToken' => $accessToken,
            'user'      => $user,
        ];

        return response()->json($response, 201);
    }

    /**
    * Login User
    *
    * @return \Illuminate\Http\Response
    */
    public function login(Request $request)
    {
        $input = $request->only(['login', 'password', 'remember']);

        $request->validate([
            'login' => 'required',
            'password' => 'required',
        ]);

        $login = $input['login'];
        $field = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        $user = User::where($field, $login)->first();

        if (!$user) {
            return $this->sendResponse(['error' => true, 'message' => 'Unauthorized Credentials.'], Response::HTTP_UNAUTHORIZED);
        }

        if (!Hash::check($input['password'], $user->password)) {
            return $this->sendResponse(['error' => true, 'message' => 'Unauthorized Credentials.'], Response::HTTP_UNAUTHORIZED);
        }

        $user->load('profile', 'roles');
        $accessToken = $user->createToken($user->email)->accessToken;

        $response = [
            'message' => 'Successfully logged in',
            'accessToken' => $accessToken,
            'user' => $user,
        ];

        return $this->sendResponse($response);
    }

    /**
     * Logout User
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        /** @var \App\Models\User */
        $user = Auth::user();
        $user->token()->revoke();
        return $this->sendResponse([], 'Successfully logged out.');
    }


    /**
     * Request for reset password link
     */
    public function forgot()
    {
        $credentials = request()->validate(['email' => 'required|email']);

        Password::sendResetLink($credentials);
        return response()->json([
            'error' => false,
            'message' => 'Reset password link sent on your email id.',
        ]);
    }

    /**
     * Reset user password
     */
    public function reset(ResetPasswordRequest $request)
    {
        $reset_password_status = Password::reset($request->validated(), function ($user, $password) {
            $user->password = $password;
            $user->save();
        });

        if ($reset_password_status == Password::INVALID_TOKEN) {
            return response()->json(['INVALID_RESET_PASSWORD_TOKEN']);
        }

        return response()->json(["Password has been successfully changed"]);
    }
}
