<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\SalesOrder;
use Illuminate\Http\Request;

class PaymentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Payment::query();

        $results = $query->get();

        $this->params['results_count'] = $results->count();
        $this->params['results'] = $results;

        return $this->sendResponse($this->params, 'Resource updated successfully');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Invoice $invoice)
    {
        return response()->json($invoice);

        // Check DocType
        // 0 = Reserved for balance carried forward records
        // 1 = Sales / Invoice
        // 2 = Reserved for schedule payments
        // 3 = Debit Memos
        // 4 = Finance Charges
        // 5 = Service Repairs
        // 6 = Warranties
        // 7 = Credit Memos
        // 8 = Returns
        // 9 = Payments

        // Validate incoming request


        // Build props
        $props = [
            'invoice_number' => $request->input('invoice_number'),
            'customer_id' => $request->input('customer_id'),
            'vendor_id' => $request->input('vendor_id'),
            'ar_number' => $request->input('ar_number'),
            'ap_number' => $request->input('ap_number'),
            "amount" => $request->input('amount'),
            "bank_id" => $request->input('bank_id'),
            "bank_account_number" => $request->input('bank_account_number'),
            "check_number" => $request->input('check_number'),
            'transaction_type' => rand(0,4),
            'transaction_date' => now()
        ];

        $results = Payment::create($props);

        $this->params['results_count'] = 1;
        $this->params['results'] = $results;

        return $this->sendResponse($this->params, 'Resource created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
