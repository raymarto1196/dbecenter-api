<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateBankRequest;
use App\Models\Bank;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BanksController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Bank::query();

        $results = $query->get();

        // $this->params['results_accredited'] = $results->where('accredited',  1)->count();
        // $this->params['results_not_accredited'] = $results->where('accredited', 0)->count();
        $this->params['results_count'] = $results->count();
        $this->params['results'] = $results;
        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateBankRequest $request)
    {
        $validated = $request->validated();

        $model = Bank::create($validated);

        $this->params['results_count'] = 1;
        $this->params['results'] = $model;

        return $this->sendResponse($this->params, 'Resource created successfully', false, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Bank $bank)
    {
        $this->params['results_count'] = 1;
        $this->params['results'] = $bank;

        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateBankRequest $request, Bank $bank)
    {
        $validated = $request->validated();

        $bank->update($validated);

        $this->params['results_count'] = 1;
        $this->params['results'] = $bank;

        return $this->sendResponse($this->params, 'Resource updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bank $bank)
    {
        $bank->delete();
        return $this->sendResponse([], 'Resource deleted successfully');
    }
}
