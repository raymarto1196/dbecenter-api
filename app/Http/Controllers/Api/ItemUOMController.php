<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ItemTaxOption;
use App\Models\ItemUOM;
use Illuminate\Http\Request;

class ItemUOMController extends Controller
{
    public function index()
    {
        $this->params = ItemUOM::all();
        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }
}
