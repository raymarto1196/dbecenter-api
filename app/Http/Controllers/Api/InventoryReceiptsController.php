<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Dropdowns\PurchaseOrderResource;
use Illuminate\Http\Request;
use App\Models\InventoryReceipt;
use App\Models\Item;
use App\Models\WarehouseCenter;
use App\Models\MaintenanceCodeSetup;
use App\Http\Resources\InventoryReceiptResource;
use App\Http\Resources\IventoryReceiptResourceItem;
use App\Models\InventoryReceiptItem;
use App\Models\PurchaseOrder;
use App\Models\ItemBatch;
use App\Models\ItemQuantity;
use App\Models\Warehouse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use OpenApi\Annotations\Items;

class InventoryReceiptsController extends Controller
{
    protected $documents = ['file_data'];

    /**
     * @OA\Get(
     * path="/api/v1/inventory-receipts",
     * tags={"InventoryReceipt"},
     * operationId="inventoryReceiptIndex",
     * description="Get List of Inventory Receipts",
     * security={
     *       {"api_key": {}}
     *     },
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="List of Inventory Receipt successfully retrieved"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/InventoryReceipt"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=403,
     *    description="Unauthorized",
     *  ),
     *
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Display a listing of the InventoryReceipt.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $query = InventoryReceipt::query()->with('items');

        // if ($request->has('qa_status') && $request->input('qa_status')) {
        //     $query->where('qa_status', 'approved');
        // }

        // // return $query->get();

        // $data =  IventoryReceiptResource::collection(InventoryReceipt::all());

        // foreach ($data as $key => $IR) {
        // 	$IR->items;
        // }

        // $this->params['results_count'] = $data->count();
        // $this->params['results'] = $data;

        // return $this->sendResponse($this->params, 'Resource retrieved successfully');

        $results = InventoryReceiptResource::collection(InventoryReceipt::where([
            [function ($query) use ($request){
                if (($search_string = $request->search_string)) {
                    $search_string = '%' . $search_string . '%';
                    $query->orWhere('inventory_receipt_code', 'LIKE', $search_string);
                    $query->orWhere('purchase_order_code', 'LIKE', $search_string);
                }
                if(($start_date = $request->start_date) && ($end_date = $request->end_date)){
                    $start_date = Carbon::createFromFormat('m-d-Y H:i:s', $start_date)->format('Y-m-d H:i:s');
                    $end_date = Carbon::createFromFormat('m-d-Y H:i:s', $end_date)->format('Y-m-d H:i:s');
                    $query->whereBetween('created_at', [$start_date, $end_date])->get();
                }

                if ($request->has('qa_status') && $request->input('qa_status')) {
                    $query->where('qa_status', 'approved');
                }

         }]
            // ])->with('items')->paginate(
            //         $request->pageLimit
            // );
            ])->with('items')->get());
            $this->params['results_count'] = $results->count();
            $this->params['results'] = $results;
            return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }


    /**
     * @OA\Post(
     * path="/api/v1/inventory-receipts",
     * tags={"InventoryReceipt"},
     * operationId="inventory-receiptsStore",
     * description="Create inventory-receipts",
     * security={
     *       {"api_key": {}}
     *     },
     *  @OA\RequestBody(
     *    required=true,
     *    description="Create inventory-receipts",
     *    @OA\JsonContent(
     * required={"inrr_number","po_number","date_purchased","supplier","dr_number","dr_date","posting_date","posted_by","si_number","si_date","location","approval_date","approved_by","qa_status"},
	 * @OA\Property(property="inrr_number", type="string", example="INRRN-000"),
	 * @OA\Property(property="po_number", type="string", example="PO-0000"),
	 * @OA\Property(property="date_purchased", type="string", example="2021-04-08"),
	 * @OA\Property(property="supplier", type="string", example="Supplier0"),
	 * @OA\Property(property="dr_number", type="string", example="DR-0016178592"),
	 * @OA\Property(property="dr_date", type="string", example="2021-04-08"),
	 * @OA\Property(property="posting_date", type="string", example="2021-04-08"),
	 * @OA\Property(property="posted_by", type="string", example="Encoder 0"),
	 * @OA\Property(property="si_number", type="string", example="si-2130"),
	 * @OA\Property(property="si_date", type="string", example="2021-04-08"),
	 * @OA\Property(property="location", type="string", example="Supplier0"),
	 * @OA\Property(property="approved_date", type="string", example="2021-04-08"),
	 * @OA\Property(property="approved_by", type="string", example="null"),
	 *  @OA\Property(property="qa_status", type="string", example="null"),
     *  @OA\Property(property="items", type="object",
     * type="array",
     * @OA\Items(
     * @OA\Property(property="item_code", type="string", example="13"),
     * @OA\Property(property="item_name", type="string", example="Item Name 3"),
     * @OA\Property(property="batch_number", type="integer", example="ItB-00003"),
     * @OA\Property(property="uom", type="integer", example="box"),
     * @OA\Property(property="quantity_for_in", type="integer", example=123),
     * @OA\Property(property="quantity_for_return", type="string", example=8),
     * @OA\Property(property="quantity_ordered", type="string", example=8),
     * @OA\Property(property="unit_price", type="integer", example="27.00"),
     * @OA\Property(property="total_price", type="integer", example="249.00"),
     * @OA\Property(property="location", type="integer", example="RA-BKT3"),
     * @OA\Property(property="remarks", type="integer", example="Good"),
     * ) ),
     *    ),
     * ),
     * @OA\Response(
     *    response=400,
     *    description="Bad Request",
     *    @OA\JsonContent(
     *            @OA\Property(property="success", type="boolean", example=false),
     *       @OA\Property(property="message", type="string", example="Validation Error"),
     *    )
     *  ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     *
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Create New inventory-receipts.
     *
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        // Purchase Order Receiving
        $purchase_order_id = $request->input('purchase_order_id');
        $purchase_order = PurchaseOrder::findOrFail($purchase_order_id);

        $reference_number = date('Y').'-'.sprintf($this->code_format, (InventoryReceipt::max('id')+1));
        $InventoryReceiptData = [
            'vendor_id' => $purchase_order->vendor_id,
            'inventory_receipt_code' => "IRR-{$reference_number}",
            'purchase_order_id' => $request->input('purchase_order_id'),
            'dr_number' => $request->input('dr_number'),
            'dr_date' => $request->input('dr_date'),
    		'si_number' => $request->input('si_number'),
    		'si_date' => $request->input('si_date'),
            'irr_remarks' => $request->input('irr_remarks'),
            'journal_remarks' => 'Goods Receipt',
    		'received_by' => Auth::user()->id,
    		'quality_control_status' => 'pending',
    	];

    	$newIR = InventoryReceipt::create($InventoryReceiptData);

        $foldername = "inventory_receipt/inventory_receipt-{$newIR->id}/";

        foreach ($this->documents as $document) {
            /**
             * Store file uploaded from file object
             */
            if ($file = $request->file($document)) {
                $extension = $file->getClientOriginalExtension();
                $filename = $document.'.'.$extension;
                $newIR->file_data = uploadFileToPath($foldername, $file, $filename);
            }
        }

        $newIR->save();

    	if ($request->has('items') && ($request->input('items') != null) && is_array($request->input('items'))) {
    		$items = $request->input('items');

    		// Create Invetory Receipt Line Item
    		foreach ($items as $key => $item) {

		    	$IRItemData = [
		    		'inventory_receipt_id' => $newIR->id,
                    'warehouse_id' => $item['warehouse_id'],
                    'warehouse_pallet_id' => $item['warehouse_pallet_id'],
                    'item_id' => $item['item_id'],
                    'lot_number' => $item['lot_number'],
                    'expiry_date' => $item['expiry_date'],
                    'manufacture_date' => $item['manufacture_date'],
                    'inventory_received' => $item['inventory_received'],
		    		'inventory_retention' => $item['inventory_retention'],
		    		'remarks'  => $item['remarks'],
		    	];

		    	$results = $newIR->items()->create($IRItemData);
	    	}
    	}

        $this->params['results_count'] = 1;
        $this->params['results'] = $newIR->load('items');

        return $this->sendResponse($this->params, 'Resource created successfully.', false, Response::HTTP_CREATED);
    }

    /**
     * @OA\PUT(
     * path="/api/v1/inventory-receipts/{id}",
     * tags={"InventoryReceipt"},
     * operationId="inventory-receipts Update",
     * description="Update a inventory-receipts by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="inventory-receipts id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *  @OA\RequestBody(
     *    required=true,
     *    description="Update inventory-receipts",
     *    @OA\JsonContent(
     *       required={"inrr_number","po_number","date_purchased","supplier","dr_number","dr_date","posting_date","posted_by","si_number","si_date","location","approval_date","approved_by","qa_status"},
		*    @OA\Property(property="inrr_number", type="string", example="INRRN-000"),
		*  * @OA\Property(property="po_number", type="string", example="PO-0000"),
		*  * @OA\Property(property="date_purchased", type="string", example="2021-04-08"),
		*  * @OA\Property(property="supplier", type="string", example="Supplier0"),
		*  * @OA\Property(property="dr_number", type="string", example="DR-0016178592"),
		*  * @OA\Property(property="dr_date", type="string", example="2021-04-08"),
		*  * @OA\Property(property="posting_date", type="string", example="2021-04-08"),
		*  * @OA\Property(property="posted_by", type="string", example="Encoder 0"),
		*  * @OA\Property(property="si_number", type="string", example="si-2130"),
		*  * @OA\Property(property="si_date", type="string", example="2021-04-08"),
		*  * @OA\Property(property="location", type="string", example="Supplier0"),
		*  * @OA\Property(property="approved_date", type="string", example="2021-04-08"),
		*  * @OA\Property(property="approved_by", type="string", example="ADMIN"),
		*  * @OA\Property(property="qa_status", type="string", example="APPROVED"),
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="inventory-receipts successfully updated"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/InventoryReceipt"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=400,
     *    description="Bad Request",
     *    @OA\JsonContent(
     *            @OA\Property(property="success", type="boolean", example=false),
     *       @OA\Property(property="message", type="string", example="Validation Error"),
     *    )
     *  ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description="inventory-receipts Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example="inventory-receipts Not Found"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Update a inventory-receipts by ID
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InventoryReceipt $inventoryReceipt)
    {
        // if(($dr_date = $request->dr_date) && ($si_date = $request->si_date)){
        //     $dr_date = Carbon::createFromFormat('m-d-Y', $dr_date)->format('Y-m-d');
        //     $si_date = Carbon::createFromFormat('m-d-Y', $si_date)->format('Y-m-d');
        // }
    	$inventoryReceiptData = [
            'purchase_order_id' => $request->input('purchase_order_id'),
            'dr_number' => $request->input('dr_number'),
            'dr_date' => $request->input('dr_date'),
    		'si_number' => $request->input('si_number'),
    		'si_date' => $request->input('si_date'),
            'irr_remarks' => $request->input('remarks'),
            'journal_remarks' => $request->input('journal_remarks'),
    	];

    	 $inventoryReceipt->update($inventoryReceiptData);

         if ($request->has('items') && ($request->input('items') != null) && is_array($request->input('items'))) {
    		$items = $request->input('items');

    		foreach ($items as $key => $item) {

                $obj = InventoryReceiptItem::find($item['id']);
                $get_array[] = $item['id'];
                if ($obj == null) {

                    $ir_data = [
                        'inventory_receipt_id' => $inventoryReceipt->id,
                        'item_id' => $item['item_id'],
                        'lot_number' => $item['lot_number'],
                        'expiry_date' => $item['expiry_date'],
                        'manufacture_date' => $item['manufacture_date'],
                        'inventory_received' => $item['inventory_received'],
                        'inventory_retention' => $item['inventory_retention'],
                        'warehouse_id' => $item['warehouse_id'],
                        'warehouse_pallet_id' => $item['warehouse_pallet_id'],
                        'remarks'  => $item['remarks'],
                    ];

                    $get_id = $inventoryReceipt->items()->create($ir_data);
                    array_push($get_array, $get_id->id);

                } else{
                    $obj->inventory_receipt_id = $inventoryReceipt->id;
                    $obj->item_id = $item['item_id'];
                    $obj->lot_number = $item['lot_number'];
                    $obj->expiry_date = $item['expiry_date'];
                    $obj->manufacture_date = $item['manufacture_date'];
                    $obj->inventory_received = $item['inventory_received'];
                    $obj->inventory_retention = $item['inventory_retention'];
                    $obj->warehouse_id = $item['warehouse_id'];
                    $obj->warehouse_pallet_id = $item['warehouse_pallet_id'];
                    $obj->remarks = $item['remarks'];
                    $obj->save();
                }
	    	};

            $remove_null = array_filter($get_array);
            InventoryReceiptItem::where('inventory_receipt_id', $inventoryReceipt->id)->whereNotIn('id',$remove_null)->delete();
    	}

        if (empty($request->items))
        {
            InventoryReceiptItem::where('inventory_receipt_id', $inventoryReceipt->id)->delete();
        }

        $foldername = "inventory_receipt/inventory_receipt-{$inventoryReceipt->id}/";

        foreach ($this->documents as $document) {
            /**
             * Store file uploaded from file object
             */
            if ($file = $request->file($document)) {
                $extension = $file->getClientOriginalExtension();
                $filename = $document.'.'.$extension;
                $inventoryReceipt->file_data = uploadFileToPath($foldername, $file, $filename);
            }
        }
        $inventoryReceipt->save();

        $this->params['results_count'] = 1;
        $this->params['results'] = $inventoryReceipt;
        return $this->sendResponse( $this->params, 'Resource updated successfully');
    }

    public function show(InventoryReceipt $inventoryReceipt)
    {
        $results = new InventoryReceiptResource($inventoryReceipt);
        $results->load('items');

        $this->params['results_count'] = 1;
        $this->params['results'] = $results;
        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

    /**
     * @OA\Delete(
     * path="/api/v1/inventory-receipts/{id}",
     * tags={"InventoryReceipt"},
     * operationId="inventory-receipts Delete",
     * description="Delete a inventory-receipts by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="inventory-receipts",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="inventory-receipts successfully deleted"),
     *        )
     *     ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description="inventory-receipts Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example="inventory-receipts Not Found"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Delete a inventory-receipts by ID
     *
     * @return \Illuminate\Http\Response
     */

    public function destroy(InventoryReceipt $inventoryReceipt)
    {
        $inventoryReceipt->delete();
        return $this->sendResponse([], 'Inventory Receipt successfully deleted.');

        $deleted = false;

        DB::beginTransaction();
        try {
            /**
             * Todo: Delete Items: inventory_receipt_items
             */


            // Delete the actual IR
            // $inventoryReceipt->delete();

            // Commmit Transaction
            DB::commit();
            // $deleted = true;
        } catch (\Exception $e) {
            DB::rollBack();
        }

        if (! $deleted) {
            return $this->sendError('Error while processing', [], 500);
        }

        return $this->sendResponse([], 'Resource deleted successfully');
    }

    public function inspection_checklist(Request $request, InventoryReceipt $inventory_receipt){

        if ('approved' == $inventory_receipt->quality_control_status) {
            return $this->sendResponse([], 'Resource quality control status was already approved');
        }

        $InventoryAdjustmentData = [
            'color_assignment' => $request->input('color_assignment'),
            'color_assignment_remarks' => $request->input('color_assignment_remarks'),
            'label_information' => $request->input('label_information'),
            'label_information_remarks' => $request->input('label_information_remarks'),
            'packing_quality' => $request->input('packing_quality'),
            'packing_quality_remarks' => $request->input('packing_quality_remarks'),
            'smell' => $request->input('smell'),
            'smell_remarks' => $request->input('smell_remarks'),
            'texture' =>  $request->input('texture'),
            'texture_remarks' =>  $request->input('texture_remarks'),
            'recommendation' =>  $request->input('recommendation'),
            'inspection_remarks' =>  $request->input('inspection_remarks'),
            'checked_by' => Auth::user()->id,
            'quality_control_status' => 'approved',
            'status' => 'pending'
    	];

        $results = $inventory_receipt->update($InventoryAdjustmentData);

        $this->params['results'] = $results;
        return $this->sendResponse($this->params, 'Resource update successfully');
    }

    public function change_status(Request $request, InventoryReceipt $inventory_receipt){
        $status = $request->input('status');

        if ('pending' == $inventory_receipt->quality_control_status) {
            return $this->sendResponse([], 'Resource quality controll status is not approved');
        }

        if ('approved' == $inventory_receipt->status) {
            return $this->sendResponse([], 'Resource status was already approved');
        }

        if($status == 'approved'){

                $inventory_receipt->approved_date = Carbon::now();
                $inventory_receipt->status = $status;
                $inventory_receipt->approved_by = Auth::user()->id;

                $data = InventoryReceipt::where('id',$inventory_receipt->id)
                ->with(['items'])
                ->first();

                $count = $data->items->count();
                $array[] = $count;
                for ($a=0; $a < $count ; $a++) {

                $item = Item::where('id',$data->items[$a]->item_id)
                ->first();

                 $item_lot_number = [
                    'item_id' => $data->items[$a]->item_id,
                    'unit_cost' => $item->unit_cost,
                    'srp' => $item->selling_price,
                    'warehouse_id' => $data->items[$a]->warehouse_id,
                    'warehouse_pallet_id' => $data->items[$a]->warehouse_pallet_id,
                    'lot_number' => $data->items[$a]->lot_number,
                    'expiry_date' => $data->items[$a]->expiry_date,
                    'manufacture_date' => $data->items[$a]->manufacture_date,
                    'date_received' => Carbon::now(),
                    'quantity_received' => $data->items[$a]->inventory_received,
                    'batch_number_sold' => 0
                ];

                $results = ItemBatch::create($item_lot_number);

                //add quantity on item quantity master
                $quantity = ItemQuantity::where('item_id',$results->item_id)
                ->where('warehouse_id', $results->warehouse_id)
                ->where('warehouse_pallet_id', $results->warehouse_pallet_id)
                ->first();

                if ($quantity === null) {
                    $item_data = [
                        'item_id' =>  $results->item_id,
                        'warehouse_id' => $results->warehouse_id,
                        'warehouse_pallet_id' =>$results->warehouse_pallet_id,
                        'onhand_quantity' =>  $results->quantity_received,
                        'last_receipt_quantity' => $results->quantity_received,
                        'last_receipt_date' => $results->created_at,
                        'vendor_id' => 0,
                    ];
                     ItemQuantity::create($item_data);
                }
               else{
                $quantity->last_receipt_quantity = $results->quantity_received;
                $quantity->onhand_quantity += $results->quantity_received; 
                $quantity->last_receipt_date = $results->created_at;
                $quantity->save();
               }

              //add quantity on item master
               $add_quantity = Item::where('id',$results->item_id)
               ->first();

               $add_quantity->total_quantity += $results->quantity_received; 
               $add_quantity->stock_level += $results->quantity_received; 
               $add_quantity->save();

            }
        }

        elseif($status == 'cancelled'){
               $inventory_receipt->status == $status;
               $inventory_receipt->approved_date = null;
               $inventory_receipt->approved_by = null;
        }

        $results = $inventory_receipt->update();

        $this->params['results'] = $inventory_receipt;
        return $this->sendResponse($this->params, 'Resource updated successfully');
    }
}
