<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\InventoryAdjustmentResource;
use App\Http\Resources\ItemBatchResource;
use App\Models\InventoryAdjustment;
use App\Models\Item;
use App\Models\ItemBatch;
use App\Models\ItemQuantity;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

use function PHPUnit\Framework\isNull;

class InventoryAdjustmentsController extends Controller
{
    public function __construct()
    {

    }
    /**
     * @OA\Get(
     * path="/api/v1/inventory/adjustments",
     * tags={"inventory/adjustments"},
     * operationId="inventory/adjustments Index",
     * description="Get List of inventory/adjustments",
     * security={
     *       {"api_key": {}}
     *     },
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="List of inventory/adjustments successfully retrieved"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/InventoryAdjustment"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     *
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Display a listing of the inventory/adjustments.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $lists = InventoryAdjustment::with('items:id,item_name')->get();
        $lists = InventoryAdjustmentResource::collection(InventoryAdjustment::all());
        $this->params['results_count'] = $lists->count();
        $this->params['results'] = $lists;
        return $this->sendResponse($this->params, 'Successfully retrieved');
    }


    public function store(Request $request)
    {
        if(($expiry_date = $request->expiry_date)){
            $expiry_date = Carbon::createFromFormat('m-d-Y', $expiry_date)->format('Y-m-d');
        }
        
        $InventoryAdjustmentData = [
            'item_id' => $request->input('item_id'),
            'lot_number' => $request->input('lot_number'),
            'audit_actual_count' => $request->input('audit_actual_count'),
            'adjusted_quantity' => $request->input('adjusted_quantity'),
            'journal_entry' => $request->input('journal_entry'),
            'reason' => $request->input('reason'),
            'prepared_by' => Auth::user()->id,
            'adjusted_by' => Auth::user()->id,
            'status' => 'pending',
    	];

        $iar_data = InventoryAdjustment::create($InventoryAdjustmentData);  
        $iar_data->expiry_date = $expiry_date;
        $iar_data->iar_code ='IAR-'.sprintf('%04d', $iar_data->id);
        $iar_data->save();
        
        $this->params['results_count'] = $iar_data->count();
        $this->params['results'] = $iar_data;

        return $this->sendResponse($this->params, 'Resource created successfully.', false, Response::HTTP_CREATED);
    }

    public function show($id)
    {
        $data =  InventoryAdjustmentResource::collection(InventoryAdjustment::all())
        ->where('id',$id);
        $results = $data->flatten();

        $this->params['results_count'] = 1;
        $this->params['results'] = $results;

        return $this->sendResponse($this->params, 'Resource retrieved successfully.');
    }

            /**
     * @OA\PUT(
     * path="/api/v1/inventory/adjustments/{id}",
     * tags={"inventory/adjustments"},
     * operationId="inventory/adjustments Update",
     * description="Update a inventory/adjustments by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="inventory/adjustments id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *  @OA\RequestBody(
     *    required=true,
     *    description="Update inventory/adjustments",
     *    @OA\JsonContent(
     *    required={"adjusted_qty","reason","adjusted_by","status"},
     *      @OA\Property(property="adjusted_qty", type="string", example="Bell-Kenz Tower"),
     *      @OA\Property(property="reason", type="string", example="324354657"),
     *      @OA\Property(property="adjusted_by", type="string", example="123 Sakalam St."),
     *      @OA\Property(property="status", type="string", example="Matarik Bdlg")
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="inventory/adjustments successfully updated"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/InventoryAdjustment"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=400,
     *    description="Bad Request",
     *    @OA\JsonContent(
     *            @OA\Property(property="success", type="boolean", example=false),
     *       @OA\Property(property="message", type="string", example="Validation Error"),
     *    )
     *  ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description="Department Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example="inventory/adjustments Not Found"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Update a WarehouseCenter by ID
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InventoryAdjustment $inventory_adjustment)
    {
        if(($expiry_date = $request->expiry_date)){
            $expiry_date = Carbon::createFromFormat('m-d-Y', $expiry_date)->format('Y-m-d');
        }

        $updated = [
            'item_id' => $request->input('item_id'),
            'lot_number' => $request->input('lot_number'),
            'audit_actual_count' => $request->input('audit_actual_count'),
            'adjusted_quantity' => $request->input('adjusted_quantity'),
            'journal_entry' => $request->input('journal_entry'),
            'reason' => $request->input('reason'),
            'adjusted_by' => Auth::user()->id,
            'status' => 'pending',
        ];

       $inventory_adjustment->expiry_date = $expiry_date;
       $results = $inventory_adjustment->update($updated);

        $this->params['results_count'] = 1;
        $this->params['result'] = $inventory_adjustment;
        return $this->sendResponse($this->params, 'Resouce updated successfully.');
    }

    public function destroy(InventoryAdjustment $inventory_adjustment)
    {
       $results = $inventory_adjustment->delete();

       $this->params['results_count'] = 1;
       $this->params['result'] = $results;
       return $this->sendResponse($this->params, 'Resource deleted successfully');
    }
    /**
     * @OA\POST(
     * path="/api/v1/inventory-transfer/{inventoryTransfer}/change-status",
     * tags={"inventory/adjustments"},
     * operationId="inventory/adjustments",
     * description="Change_status of Inventory by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="inventory id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *  @OA\RequestBody(
     *    required=true,
     *    description="Change_status of a inventory by ID",
     *    @OA\JsonContent(
     *       required={"status"},
     *        @OA\Property(property="status", type="boolean",  example=1),
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="inventory/adjustments successfully Change_status"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/InventoryAdjustment"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=400,
     *    description="Bad Request",
     *    @OA\JsonContent(
     *            @OA\Property(property="success", type="boolean", example=false),
     *       @OA\Property(property="message", type="string", example="Validation Error"),
     *    )
     *  ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description="inventory Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example="inventory/adjustments"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Change_to_credited a inventory/adjustments
     *
     * @return \Illuminate\Http\Response
     */


    public function change_status(Request $request, InventoryAdjustment $inventory_adjustment)
    {
        $status = 'approved';

        if ('approved' == $inventory_adjustment->status) {
            return $this->sendResponse([], 'Resource status was already approved');
        }

        if($status == 'approved'){

            $inventory_adjustment->status = $status;
            $inventory_adjustment->approved_by = auth()->user()->id;

            //find Item_batches and update its quantity_receive for quantity adjustment
            $update_batch = ItemBatch::where('item_id',$inventory_adjustment->item_id)
            ->where('lot_number',$inventory_adjustment->lot_number)
            ->where('expiry_date',$inventory_adjustment->expiry_date)
            ->first();
       
            if($update_batch === null){
                return $this->sendResponse([], 'Resource does not exists');
            }

            $update_batch->quantity_received =  $update_batch->quantity_received + $inventory_adjustment->adjusted_quantity;
            $update_batch->save();

            //find the item_quantity master and update its quantity by item_id and warehouse
            $item_quantity = ItemQuantity::where('item_id',$inventory_adjustment->item_id)
            ->where('warehouse_id',$update_batch->warehouse_id)
            ->where('warehouse_pallet_id',$update_batch->warehouse_pallet_id)
            ->first();

            if($item_quantity === null){
                return $this->sendResponse([], 'Resource does not exists');
            }

            $item_quantity->onhand_quantity = $item_quantity->onhand_quantity + $inventory_adjustment->adjusted_quantity;
            $item_quantity->save();

            $item_master = Item::where('id',$inventory_adjustment->item_id)
            ->first();

            if($item_master === null){
                return $this->sendResponse([], 'Resource does not exists');
            }

            $item_master->quantity_on_hand = $item_master->quantity_on_hand + $inventory_adjustment->adjusted_quantity;
            $item_master->stock_level = $item_master->stock_level + $inventory_adjustment->adjusted_quantity;
            $item_master->save();

            
        }

        $inventory_adjustment->save();

        $this->params['results_count'] = 1;
        $this->params['result'] = $inventory_adjustment;
        return $this->sendResponse($this->params, 'Resource updated successfully.');
    }

    public function get_item_batches(Request $request){

        $results =  ItemBatchResource::collection(ItemBatch::where('item_id', 'LIKE', $request->input('item_id'))
        ->where('lot_number', 'LIKE',$request->input('lot_number'))
        ->where('expiry_date','LIKE', $request->input('expiry_date'))
        ->get());

        if($results->count() == 0)
        {
            return $this->sendResponse([], 'Resource does not exists');
        }
 
        $this->params['results_count'] = $results->count();
        $this->params['results'] = $results;
        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

}
