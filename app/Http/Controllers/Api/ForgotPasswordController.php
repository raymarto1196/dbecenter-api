<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    protected function sendResetLinkResponse(Request $request, $response) {
        return response()->json([
            'error'=>false,
            'msg' => "Reset Link successfully sent. Kindly check your email."
        ], Response::HTTP_OK);
    }

    protected function sendResetLinkFailedResponse(Request $request, $response) {
        return response()->json([
            'error'=>true,
            'msg' => trans($response)
        ], Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
