<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\DocumentsResource;
use App\Models\Document;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;

class DocumentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $results = Document::where(['path' => '/' ])->get();
        
       $this->params['results_count'] = $results->count();
       $this->params['results'] = $results;
       return $this->sendResponse($results, 'Resource successfully retrieved.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => [
                'required',
                Rule::unique('documents')->where(function ($query) use($request) {
                    return $query->where('name', $request->name)
                                 ->where('path', $request->path);
                })
            ],
            'path' => 'required',
            'source' => '',
            'physical_filing' => '',
            'latest_activity' => '',
        ]);


        $validated['user_id'] = auth()->user()->id;
        $validated['type'] = 0;

        $data = Document::create($validated);

        $path = '/documents/'.$data->id;
  
        Storage::makeDirectory($path);

        return $this->sendResponse($data, 'Resource successfully created.', false, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function show(Document $document)
    {
        $path = '/' . $document->id;
        // if(strlen($document->path) > 1){
        //     $path = $document->path . '/' . $document->id;
        // } 

        $results = Document::where('path', $path )->get();
        
       return $this->sendResponse($results, 'Resource successfully retrieved.');
    }

    public function navigateBack(Document $document) 
    {
        $results = Document::where('path', $document->path )->get();
        
        return $this->sendResponse($results, 'Resource successfully retrieved.');
    }


    /**
     * Update the specified resource in storage.
     * Move Folder to different path
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Document $document)
    {
        $validated = $request->validate([
            'path' => 'required',  
        ]);

        $document->update($validated);
        return $this->sendResponse($document, 'Resource updated successfully');
    }

    public function uploadFile(Request $request) 
    {
        $validated = $request->validate([
            'name' => [
                'required',
                Rule::unique('documents')->where(function ($query) use($request) {
                    return $query->where('name', $request->name)
                                 ->where('path', $request->path);
                })
            ],
            'path' => 'required',
            'source' => '',
            'physical_filing' => '',
            'latest_activity' => '',
            'folder' => '',
            'file' => '',
        ]);

        $validated['user_id'] = auth()->user()->id;
        $validated['type'] = 1;

        $data = Document::create($validated);

        if($request->has('file') && $request->input('file')){
            $base64_image = $request->input('file');

            if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                $data = substr($base64_image, strpos($base64_image, ',') + 1);
                // $extension = getFileExtension($base64_image);
                $file = base64_decode($data);
                $filename =  $request->name;
                $results->file = $filename;

                Storage::put('/documents/'.  $validated['folder'] .'/'. $filename, $file);
                return $this->sendResponse('/documents/'.  $validated['folder'] .'/'. $filename, 'Resource updated successfully');
            }


        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function destroy(Document $document)
    {
        //
    }
}
