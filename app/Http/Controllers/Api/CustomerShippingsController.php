<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerShippingsController extends Controller
{

    public function index(Customer $customer)
    {

        $this->params['results_count'] = $customer->shipping_address()->count();

        $this->params['results'] = $customer->shipping_address()->get();

        return $this->sendResponse($this->params, 'Resource retrieve successfully');

    }


    public function store(Request $request, Customer $customer)
    {
        $request->validate([
            'barangay' => ['required'],
            'city' => ['required'],
            'province' => ['required'],
        ]);

        $shipping_data = [
            'lba_code' => $request->input('lba_code'),
            'lba_name' => $request->input('lba_name'),
            'street' => $request->input('street'),
            'barangay' => $request->input('barangay'),
            'city' => $request->input('city'),
            'province' => $request->input('province'),
            'zip' => $request->input('zip'),
            'contact_number' => $request->input('contact_number', $customer->contact_number),
        ];

        $shipping_address = $customer->shipping_address()->create($shipping_data);

        $this->params['results_count'] = 1;
        $this->params['results'] = $shipping_address;

        return $this->sendResponse($this->params, 'Resource created successfully');
    }
}
