<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\ItemQuantity;
use Illuminate\Http\Request;
use App\Models\WarehouseCenter;
use App\Models\MaintenanceCodeSetup;
use App\Models\Warehouse;
use App\Models\WarehousePallet;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class WarehouseController extends Controller
{
    private $tablename = 'Warehouse Centers';

      /**
     * @OA\Get(
     * path="/api/v1/warehouse-centers",
     * tags={"WarehouseCenter"},
     * operationId="WarehouseCenterIndex",
     * description="Get List of WarehouseCenter",
     * security={
     *       {"api_key": {}}
     *     },
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="List of WarehouseCenter successfully retrieved"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/WarehouseCenter"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     *
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Display a listing of the WarehouseCenter.
     *
     * @return \Illuminate\Http\Response
     */    
    public function index(Request $request)
    {
        //do not delete
        // if (! $this->auth_user->can('delete_vendor')) {
        //
        //     return response()->json([
        //         'responseMessage' => 'You do not have the required authorization.',
        //         'responseStatus'  => 403,
        //     ], Response::HTTP_NOT_FOUND
        //
        //   );
        // }

        // $data = WarehouseCenter::all();
        // foreach ($data as $key => $warehouse) {
        // 	$warehouse->floors;
        //     foreach ($warehouse->floors as $key => $floor) {
        //         $floor->pallets = json_decode($floor->pallets);
        //         $floor->pedestrian = json_decode($floor->pedestrian);
        //     }
        // }
        // $this->params['results_count'] = 1;
        // $this->params['results'] = $data;

        // return $this->sendResponse($this->params, 'Resource retrieved successfully');

        $results = Warehouse::where([
            [function ($query) use ($request){
                if (($search_string = $request->search_string)) {
                    $search_string = '%' . $search_string . '%';
                    $query->join('warehouse_pallets','warehouse_pallets.warehouse_id', '=', 'warehouses.id');
                    $query->orWhere('warehouses.id', 'LIKE', $search_string)->get();
                    $query->orWhere('warehouses.warehouse_name', 'LIKE', $search_string)->get();
                    // DB::table('warehouse_center_floors')->join('warehouse_center_floors','warehouse_center_floors.warehouse_center_id', '=', 'warehouse_centers.id')
                    // ->orWhere('warehouse_center_floors.level', 'LIKE', $search_string)->get();

                }
                if(($start_date = $request->start_date) && ($end_date = $request->end_date)){
                    $start_date = Carbon::createFromFormat('m-d-Y H:i:s', $start_date)->format('Y-m-d H:i:s');
                    $end_date = Carbon::createFromFormat('m-d-Y H:i:s', $end_date)->format('Y-m-d H:i:s');
                    $query->whereBetween('created_at', [$start_date, $end_date])->get();
                }
                
                if ($request->has('qa_status') && $request->input('qa_status')) {
                    $query->where('qa_status', 'approved');
                }
        
         }]
            ])->with('bin_pallets')
            ->get();
            // ->paginate(
            //         $request->pageLimit
            // );
            $this->params['results_count'] = $results->count();
            $this->params['results'] = $results;
            return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

    public function show($id)
    {
        $results = Warehouse::find($id);
        $results->load('bin_pallets');

        $this->params['results'] = $results;
        return $this->sendResponse($this->params, 'Resource retrieved successfully');
        

    }
         /**
     * @OA\Post(
     * path="/api/v1/warehouse-centers",
     * tags={"WarehouseCenter"},
     * operationId="WarehouseCenterStore",
     * description="Create WarehouseCenter",
     * security={
     *       {"api_key": {}}
     *     },
     *  @OA\RequestBody(
     *    required=true,
     *    description="Create WarehouseCenter",
     *    @OA\JsonContent(
     *    required={"warehouse_name","warehouse_code","street_no","building","barangay","city","region","country","zipcode"},
     *      @OA\Property(property="warehouse_name", type="string", example="Bell-Kenz Tower"),
     *      @OA\Property(property="warehouse_code", type="string", example="324354657"),
     *      @OA\Property(property="street_no", type="string", example="123 Sakalam St."),
     *      @OA\Property(property="building", type="string", example="Matarik Bdlg"),
     *      @OA\Property(property="barangay", type="string", example="Mabago"),
     *      @OA\Property(property="city", type="string", example="Quezon City"),
     *      @OA\Property(property="region", type="string", example="NCR"),
     *      @OA\Property(property="country", type="string", example="PH"),
     *      @OA\Property(property="zipcode", type="string", example="1000"),
     *      @OA\Property(property="floors", type="object",
 *                      type="array",
 *                      @OA\Items(
 *                          @OA\Property(property="level", type="string", example="1f"),
 *                          @OA\Property(property="row", type="integer", example="10"),
 *                          @OA\Property(property="columns", type="integer", example="10"),
 *                          @OA\Property(property="pallet_division", type="integer", example="10"),
 *                          @OA\Property(property="pedestrian", type="string", example="null"),
 *                          @OA\Property(property="total_pallet_occupied", type="integer", example="0"),
 *                          @OA\Property(property="total_pallet_count", type="integer", example="100")
 *    ) ),
     *    ),
     * ),
     * @OA\Response(
     *    response=400,
     *    description="Bad Request",
     *    @OA\JsonContent(
     *            @OA\Property(property="success", type="boolean", example=false),
     *       @OA\Property(property="message", type="string", example="Validation Error"),
     *    )
     *  ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     *
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Create New WarehouseCenter.
     *
     * @return \Illuminate\Http\Response
     */



    public function store(Request $request, Warehouse $warehouse)
    {
        //do not delete
        // if (! $this->auth_user->can('create_warehouse')) {
        //
        //     return response()->json([
        //         'responseMessage' => 'You do not have the required authorization.',
        //         'responseStatus'  => 403,
        //     ], Response::HTTP_NOT_FOUND
        //
        //   );
        // }

        $site_data = [
    		'warehouse_name' => $request->get('warehouse_name'),
    		'warehouse_code' => $request->get('warehouse_code'),
    		'street_no' => $request->get('street_no'),
    		'building' => $request->get('building'),
    		'barangay' => $request->get('barangay'),
    		'city' => $request->get('city'),
    		'region' => $request->get('region'),
    		'country' => $request->get('country'),
            'pedestrian' => $request->get('pedestrian'),
            'zipcode' => $request->get('zipcode'),
            'status' => "pending",
            'requested_by' => Auth::user()->id,
    	];

        $results = Warehouse::create($site_data);

        if ($request->has('bin_pallets') && ($request->input('bin_pallets') != null) && is_array($request->input('bin_pallets'))) {
    		$bin_pallets = $request->input('bin_pallets');

    		// add bin_pallets on inventory bin site
    		foreach ($bin_pallets as $key => $pallet) {

		    	$pallet_data = [
		    		'warehouse_id' =>$results->id,
                    'bin_name' => $pallet['bin_name'],
                    'bin_status' => 0,
                    'rows' => $pallet['rows'],
                    'columns' => $pallet['columns'],
                    'level' => $pallet['level'],
		    	];

		    	$pallets = $results->bin_pallets()->create($pallet_data);

            }
        }

        $this->params['results_count'] = 1;
        $this->params['results'] = $results->load('bin_pallets');
        // $this->params['results'] = $results->load('site_mstrs')->load('site_mstrs.bin_pallets');

        return $this->sendResponse($this->params, 'Resource created successfully', false, Response::HTTP_CREATED);
    }

        /**
     * @OA\PUT(
     * path="/api/v1/warehouse-centers/{id}",
     * tags={"WarehouseCenter"},
     * operationId="WarehouseCenter Update",
     * description="Update a WarehouseCenter by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="WarehouseCenter id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *  @OA\RequestBody(
     *    required=true,
     *    description="Update WarehouseCenter",
     *    @OA\JsonContent(
     *    required={"warehouse_name","warehouse_code","street_no","building","barangay","city","region","country","zipcode"},
     *      @OA\Property(property="warehouse_name", type="string", example="Bell-Kenz Tower"),
     *      @OA\Property(property="warehouse_code", type="string", example="324354657"),
     *      @OA\Property(property="street_no", type="string", example="123 Sakalam St."),
     *      @OA\Property(property="building", type="string", example="Matarik Bdlg"),
     *      @OA\Property(property="barangay", type="string", example="Mabago"),
     *      @OA\Property(property="city", type="string", example="Quezon City"),
     *      @OA\Property(property="region", type="string", example="NCR"),
     *      @OA\Property(property="country", type="string", example="PH"),
     *      @OA\Property(property="zipcode", type="string", example="1000"),
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="WarehouseCenter successfully updated"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/WarehouseCenter"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=400,
     *    description="Bad Request",
     *    @OA\JsonContent(
     *            @OA\Property(property="success", type="boolean", example=false),
     *       @OA\Property(property="message", type="string", example="Validation Error"),
     *    )
     *  ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description="Department Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example="WarehouseCenter Not Found"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Update a WarehouseCenter by ID
     *
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Warehouse $warehouse)
    {
        //do not delete
        // if (! $this->auth_user->can('update_warehouse')) {
        //
        //     return response()->json([
        //         'responseMessage' => 'You do not have the required authorization.',
        //         'responseStatus'  => 403,
        //     ], Response::HTTP_NOT_FOUND
        //
        //   );
        // }

        $site_data = [
    		'warehouse_name' => $request->input('warehouse_name'),
    		'warehouse_code' => $request->input('warehouse_code'),
    		'street_no' => $request->input('street_no'),
    		'building' => $request->input('building'),
    		'barangay' => $request->input('barangay'),
    		'city' => $request->input('city'),
    		'region' => $request->input('region'),
    		'country' => $request->input('country'),
            'pedestrian' => $request->input('pedestrian'),
            'zipcode' => $request->input('zipcode'),
    	];

        $results = $warehouse->update($site_data);
 
        if ($request->has('bin_pallets') && ($request->input('bin_pallets') != null) && is_array($request->input('bin_pallets'))) {
    		$bin_pallets = $request->input('bin_pallets');

    		foreach ($bin_pallets as $key => $pallet) {

                $find_pallet = WarehousePallet::find($pallet['id']);
                $get_array[] = $pallet['id'];

                if($find_pallet == null){

                   $pallet_data = [
                    'warehouse_id' => $warehouse->id,
                    'bin_name' => $pallet['bin_name'],
                    'bin_status' => 0,
                    'rows' => $pallet['rows'],
                    'columns' => $pallet['columns'],
                    'level' => $pallet['level'],
                ];
               
                    $get_id = $warehouse->bin_pallets()->create($pallet_data);
                    array_push($get_array,$get_id->id);

                }
                else {

                    $find_pallet->warehouse_id = $warehouse->id;
                    $find_pallet->bin_name = $pallet['bin_name'];
                    $find_pallet->bin_status = 0;
                    $find_pallet->rows = $pallet['rows'];
                    $find_pallet->columns = $pallet['columns'];
                    $find_pallet->level = $pallet['level'];
                    $find_pallet->save();

                };
            }

            $remove_null = array_filter($get_array);
            WarehousePallet::where('warehouse_id', $warehouse->id)->whereNotIn('id',$remove_null)->delete();
        }

        if (empty($request->pallets))
        {
            WarehousePallet::where('warehouse_id', $warehouse->id)->delete();
        }

        $this->params['results_count'] = 1;
        $this->params['results'] = $results;
        return $this->sendResponse($this->params, 'Resource updated successfully');

    }
    
      /**
     * @OA\Delete(
     * path="/api/v1/warehouse-centers/{id}",
     * tags={"WarehouseCenter"},
     * operationId="WarehouseCenterDelete",
     * description="Delete a WarehouseCenter by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="WarehouseCenter",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="WarehouseCenter successfully deleted"),
     *        )
     *     ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description="WarehouseCenter Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example="WarehouseCenter Not Found"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Delete a WarehouseCenter by ID
     *
     * @return \Illuminate\Http\Response
     */


    public function destroy(Warehouse $warehouse)
    {
        //do not delete
        // if (! $this->auth_user->can('delete_warehouse')) {
        //
        //     return response()->json([
        //         'responseMessage' => 'You do not have the required authorization.',
        //         'responseStatus'  => 403,
        //     ], Response::HTTP_NOT_FOUND
        //
        //   );
        // }

        $warehouse->delete();
        return $this->sendResponse([], 'Resource deleted successfully');
    }


    /*
    *  PALETTES GENERATION 
    */
    private function generatePallet($floor)
    {
        $letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $columns = isset($floor['columns'])? intval($floor['columns']): 1;
        $rows = isset($floor['rows'])? intval($floor['rows']): 1;
        $div = isset($floor['pallet_division'])? intval($floor['pallet_division']): 1;
        $pallets = [];
        for ($r=1; $r <= $rows ; $r++) {
            for ($c=0; $c < $columns; $c++) { 
                if ($div > 1) {
                    for ($d=1; $d <= $div; $d++) {

                        $pallet = ['name' => $letters[$c].$r.'-'.$d, 'occupied' => false];
                        
                        array_push($pallets, $pallet);
                    }
                }else{
                    $pallet = ['name' => $letters[$c].$r, 'occupied' => false];
                    array_push($pallets, $pallet);
                }
            }
        }

        $floor['pallets'] = json_encode($pallets);
        return $floor;
    }

    public function change_status (Request $request,Warehouse $warehouse)
    {
        // $warehouseCenter->active = !$warehouseCenter->active;
        $warehouse->status = $request->input('status');

        if( $warehouse->status == "approved")
        {
            $validated = $request->validate([
                'requested_by_remarks' => '',
                'reviewed_by_remarks' => '',
                'approved_by_remarks' => '',
            ]);

            $status_data = [
                'approved_by' => Auth::user()->id,
                'requested_by_remarks' => $request->input('requested_by_remarks'),
                'reviewed_by_remarks' =>  $request->input('reviewed_by_remarks'),
                'approved_by_remarks' =>  $request->input('reviewed_by_remarks'),
            ];
        }
   
        $warehouse->update($status_data);

        $this->params['results_count'] = 1;
        $this->params['results'] = $warehouse;
        return $this->sendResponse($this->params, 'Resource updated successfully');
    }

    public function pallets(Request $request){
        $results = WarehousePallet::with(['warehouse:id,warehouse_name,warehouse_code,street_no,building,barangay,city,region,country,zipcode'])->get();

        $this->params['results_count'] = $results->count();
        $this->params['results'] = $results;
        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

    public function get_warehouse_pallets($warehouse_id, $warehouse_pallet_id){

        $results = WarehousePallet::with(['warehouse:id,warehouse_name,warehouse_code,street_no,building,barangay,city,region,country,zipcode'])
        ->whereHas('warehouse', function($q) use($warehouse_id) {
            $q->where('id', '=', $warehouse_id); 
         })
        ->where('id', $warehouse_pallet_id)
        ->get();

        if($results->isEmpty()){
            return $this->sendResponse([], 'Resource does not exists');
        }

        $this->params['results_count'] = 1;
        $this->params['results'] = $results;
        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

    public function set_active_inactive(Request $request, Warehouse $warehouse) {

        $warehouse->active = !$warehouse->active;
        $warehouse->save();

        $this->params['results_count'] = 1;
        $this->params['results'] = $warehouse;
        return $this->sendResponse($this->params, 'Resource updated successfully');
    }
    
}
