<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Item;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    // SORT, SEARCH, FILTER
    public function search(Request $request, $keyword=null)
    {
        $this->params['s'] = $keyword;
        $q = Item::query();

        // When category is provided
        if ($request->category) {
            // filter results having categories
        }

        // Do a fulltext search for items[name,description]
        $q->where();

        $results = $q->get();

        // Format results to specific resource to reduce response size
        // Have a SearchSuggestion resource do the thing...

        $this->params['results'] = $results;

        return $this->sendResponse($this->params);
    }
}
