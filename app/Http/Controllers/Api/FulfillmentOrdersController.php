<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\FulfillmentOrder;
use App\Models\FulfillmentOrderItem;
use App\Models\PickingInstruction;
use App\Models\PickingSlipEntry;
use App\Models\SalesOrder;
use App\Models\SalesOrderItem;

class FulfillmentOrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query  = FulfillmentOrder::query();

        $results = $query->get();

        $this->params['results_count'] = $results->count();
        $this->params['results'] = $results;

        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

    public function store(Request $request)
    {
        $sales_order_id = $request->input('sales_order_id');
        $sales_order_items = SalesOrderItem::where('sales_order_id', $sales_order_id)->get();
        // return $this->sendResponse($sales_order_items, 'Resource created succesfully.', false, Response::HTTP_CREATED);

        foreach($sales_order_items as $item) {

            // Create Picking Instructions
            $pick_instruction_props = [
                'picking_instruction' => 'lorem ipsum',
                'item_id' => $item->id,
                'priority_1' => 'lorem ipsum',
                'priority_2' => 'lorem ipsum',
                'priority_3' => 'lorem ipsum',
                'comment' => 'lorem ipsum'
            ];

            $pick_instruction = PickingInstruction::create($pick_instruction_props);

            // Create PickSlip Entry
            $pickslip_entry_props = [
                'sales_order_id' => $sales_order_id, // required
                'picking_instruction_id' => $pick_instruction->id, // required FK
                'item_id' => $item->id, // required FK
                'status' => ''
            ];

            $pickslip_entry = PickingSlipEntry::create($pickslip_entry_props);

            // return             $pickslip_entry->id;

            // Create FOTransactionWork
            $fulfillment_order_props = [
                'sales_order_id' => $sales_order_id,
                'item_id' => $item->id,
                'picked' => 0,
                'marked' => 0,
                'packed' => 0,
                'valuation_method' => 'FIFO',
            ];

            $fulfillment_order = FulfillmentOrder::create($fulfillment_order_props);

            // Create FOLine
            $fulfillment_order_item_props = [
                'sales_order_id' => $sales_order_id,
                'item_id' => $item->id,
                'selling_price' => $item->selling_price,
                'unit_cost' => $item->unit_cost,
                'extended_pricing' => $item->extended_pricing,
                'fulfillment_order_id' => $fulfillment_order->id,
                'pickslip_entry_id' => $pickslip_entry->id,
            ];

            $fulfillment_order_item = FulfillmentOrderItem::create($fulfillment_order_item_props);
        }

        $this->params['picklist'] = $fulfillment_order;

        return $this->sendResponse($this->params, 'Resource created succesfully.', false, Response::HTTP_CREATED);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store2(Request $request)
    // {

    //     $fo_picking_instruction = [
    //         'picking_instruction' => $request->post('picking_instruction'),
    //         'priority_1' => $request->post('priority_1'),
    //         'priority_2' => $request->post('priority_2'),
    //         'priority_3' =>  $request->post('priority_3'),
    //         'comment' =>  $request->post('comment'),
    //         'item_id' =>  $request->post('item_id'),
    //     ];

    //     $results = PickingInstruction::create($fo_picking_instruction);

    //     $fo_picking_slip = [
    //         'inv_picking_instruction_id' => $results->id,
    //         'sop_sales_order_id' => $request->post('sop_sales_order_id'),
    //         'picklist_id' => $request->post('picklist_id'),
    //         'item_id' => $results->item_id,
    //         'instruction_id' => $request->post('instruction_id'),
    //         'status' => $request->post('status'),
    //     ];
    //     $results->instruction()->create($fo_picking_slip);

    //     $this->params['results'] = $results;
    //     return $this->sendResponse($this->params, 'Resource created succesfully.', false, Response::HTTP_CREATED);

    // }


    // public function transaction_works(Request $request, $id)
    // {
    //     $fo_instruction = PickingInstruction::find($id);
    //     $fo_slip = PickingSlipEntry::find($fo_instruction->id);

    //     $fo_data = [
    //         'sop_sales_order_id' => $fo_slip->sop_sales_order_id,
    //         'item_id' =>  $fo_instruction->item_id,
    //         'activated' => $request->post('activated'),
    //         'picked' => null,
    //         'marked' => null,
    //         'packed' => null,
    //         'valuation_method' => $request->post('valuation_method'),
    //     ];
    //     $results = FulfillmentOrder::create($fo_data);

    //     $fo_status_data = [
    //         'inv_fulfillment_order_id' => $results->id,
    //         'sop_sales_order_id' => $results->sop_sales_order_id,
    //         'inv_picking_slip_entry_id' => $fo_slip->id,
    //         'item_id' => $results->item_id,
    //         'selling_price' => $request->post('selling_price'),
    //         'unit_cost' => $request->post('unit_cost'),
    //         'extended_pricing' => $request->post('extended_pricing'),
    //     ];
    //     $results->fulfillment_order()->create($fo_status_data);

    //     $this->params['results'] = $results;
    //     return $this->sendResponse($this->params, 'Resource created succesfully.');

    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(FulfillmentOrder $fulfillment_order)
    {
        $this->params['results_count'] = 1;
        $this->params['results'] = $fulfillment_order;

        return $this->sendResponse($this->params, 'Resource retrieved successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FulfillmentOrder $fulfillment_order)
    {
         //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $results = FulfillmentOrder::find($id);
        $results->delete();

        return $this->sendResponse([], 'Resource deleted successfully');
    }

    public function update_status(Request $request, FulfillmentOrder $fulfillment_order){
        $fo_data = [
            'sop_sales_order_id' => $fulfillment_order->id,
            'activated' => $request->post('activated'),
            'picked' => $request->post('picked'),
            'marked' => $request->post('marked'),
            'packed' => $request->post('packed'),
            'valuation_method' => $request->post('valuation_method'),
        ];

        $results = $fulfillment_order->transaction_work()->update($fo_data);

        $this->params['results'] = $results;

        return $this->sendResponse($this->params, 'Resource created succesfully.');
    }

    public function update_picked_status(Request $request, FulfillmentOrder $fulfillment){
        $status = FulfillmentOrder::find(1)->get();

        dd($status);
    }
}
