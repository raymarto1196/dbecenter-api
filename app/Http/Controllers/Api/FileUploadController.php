<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FileUploadController extends Controller
{
    public function upload(Request $request) {
        $request->validate([
            'file' => 'required|mimes:png,pdf,xlx,csv|max:2048',
        ]);

        $filename = time().'.'.$request->file->extension();

        $request->file->move(public_path('uploads'), $filename);

        $params['message'] = 'File uploaded successfully';

        $params['filename'] = $filename;

        return response()->json($params);

    }
}
