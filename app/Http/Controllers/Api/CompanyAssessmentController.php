<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\AssessmentQuestionnaire;
use App\Models\Company;
use App\Models\CompanyAssessment;
use App\Models\User;
use Illuminate\Http\Request;

class CompanyAssessmentController extends Controller
{
    public function AssesmentQuestionnaires(Request $request){
        $results = AssessmentQuestionnaire::where([
            [function ($query) use ($request) {
                if (($search_string = $request->search_string)) {
                    $search_string = '%' . $search_string . '%';
                    $query->orWhere('company_type', 'LIKE', $search_string);
                }
            }]
            ])->get();
        
        $this->params['message'] = 'Resource retrieved successfully';
        $this->params['results_count'] = $results->count();
        $this->params['results'] = $results;
    
        return $this->sendResponse($this->params);
       }

    public function update_assessment(Request $request){

        $request->validate([
            'assessment_results_list' => 'array'
           ]);

        $user = ['user_id' =>$this->auth_user->id];
        $company = Company::where('user_id',$this->auth_user->id)->first();

        $assesstment_data = [
            'company_id'    => $company->id,
            'assessment_results_list' => $request->input('assessment_results_list')
        ];

        $result = CompanyAssessment::UpdateOrCreate($user,$assesstment_data);

        $this->params['message'] = 'Resource updated successfully';
        $this->params['results_count'] = 1;
        $this->params['results'] = $result;

        return $this->sendResponse($this->params);
    }

    public function AssesmentResults(){
        $result = User::findOrFail($this->auth_user->id);
        $result->load('company_assessment');

        $this->params['message'] = 'Resource retrieved successfully';
        $this->params['results_count'] = 1;
        $this->params['results'] = $result;
        return $this->sendResponse($this->params);

    }
}
