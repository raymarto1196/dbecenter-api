<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\StaticModuleRoutes;
use Illuminate\Http\Request;

class StaticModuleRoutesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->params['results_count'] = StaticModuleRoutes::count();
        $results = StaticModuleRoutes::all();
        $results = $results->toArray();
        
        $result = array();
        $group = 1;
        $collapsible = false;
        $key_of_collapsible_route = null;
        foreach($results as $key => $val) {
            $current_group = $val['group'];
            $is_collapse = $val['collapsible'] == 1 ? true : false;

            if($group != $current_group){
                $group = $current_group;
                $collapsible = $is_collapse;
                if($collapsible) {
                    $key_of_collapsible_route = $key;
                } else {
                    $key_of_collapsible_route = null;
                }
      
                $result[] = $val;
            }  else {
                if($collapsible) {
                    $result[$key_of_collapsible_route]['views'][] = $val;
                } else {
                    $result[] = $val;
                }
            }
        }
        

        $this->params['results'] = $result;
        return $this->sendResponse($this->params, 'Resource successfully retrieved.');

    }
}
