<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PurchaseRequestItemResource;
use App\Http\Resources\PurchaseRequestResource;
use App\Models\Item;
use App\Models\PurchaseOrder;
use App\Models\PurchaseOrderItem;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\PurchaseRequest;
use App\Models\PurchaseRequestItem;
use App\Models\UserActivityLog;
use App\Models\VendorCreditTerms;
use Auth;
use Error;
use Exception;
use Carbon\Carbon;

class PurchaseRequestsController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $results =  PurchaseRequestResource::collection(PurchaseRequest::all());

    $this->params['results_count'] = $results->count();
    $this->params['results'] = $results;
    return $this->sendResponse($this->params, 'Resource retrieved successfully');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      if ($request->has('items') && ($request->input('items') != null) && is_array($request->input('items'))) {
        $items = $request->input('items');

        foreach ($items as $key => $item) {
        $data = [
          'requested_by' => Auth::user()->id,
          'department' => Auth::user()->profile->department_id,
          'date_needed' => $request->input('date_needed'),
          'particulars' => $request->input('particulars'),
          'description' => $request->input('description'),
          'status' => 'pending',
        ];      

        $results = PurchaseRequest::create($data);
        $results->reference_number ='PR-'.sprintf($this->code_format, $results->id);
        $results->save();

        $item_data = Item::where('id',$item['item_id'])
        ->first();

        $pr_data = [
          'purchase_request_id' => $results->id,
          'item_id' => $item['item_id'],
          'vendor_id' => $item_data->vendor_id,
          'quantity_ordered' => $item['quantity_ordered'],
          'uom' => $item_data->uom,
          'unit_cost' => $item_data->unit_cost,
        ];

         $data = $results->item()->create($pr_data);

         $pr_approver = [
          'approver_1' => 0,
          'approver_2' => 0,
         ];

         $results->approval()->create($pr_approver);
         $get_array[] =  $results->load('item','approval');
        }
      }
          // Append record to user activity logs


      $this->params['results_count'] = count($get_array);
      $this->params['results'] = $get_array;
      return $this->sendResponse($this->params, 'Resource successfully created.', false, Response::HTTP_CREATED);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show(Request $request, PurchaseRequest $purchase_request)
  {
    $results = new PurchaseRequestResource($purchase_request);

    $this->params['results_count'] = 1;
    $this->params['results'] = $results;

    return $this->sendResponse($this->params, 'Resource retrieved successfully');
      
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, PurchaseRequest $purchase_request)
  {
        $data = [
          'date_needed' => $request->input('date_needed'),
          'particulars' => $request->input('particulars'),
          'description' => $request->input('description'),
        ];      

        $purchase_request->update($data);

        if ($request->has('items') && ($request->input('items') != null) && is_array($request->input('items'))) {
        $items = $request->input('items');

        foreach ($items as $key => $item) {
                
        $obj = PurchaseRequestItem::find($item['id']);
        $get_array[] = $item['id'];

        $item_data = Item::where('id',$item['item_id'])
        ->first();

          if($obj === null){
              $pr_item_data = [
              'vendor_id' => $item_data->vendor_id,
              'quantity_ordered' => $item['quantity_ordered'],
              'uom' => $item_data->uom,
              'unit_cost' => $item_data->unit_cost,
              ];

              $get_id = $purchase_request->item()->create($pr_item_data);
              array_push($get_array,$get_id->id);
          }    
          else{
              $obj->purchase_request_id = $purchase_request->id;
              $obj->item_id = $item['item_id'];
              $obj->vendor_id = $item_data->vendor_id;
              $obj->quantity_ordered = $item['quantity_ordered'];
              $obj->uom = $item_data->uom;
              $obj->unit_cost = $item_data->unit_cost;
              $obj->save();
        }

          $remove_null = array_filter($get_array);
          PurchaseRequestItem::where('purchase_request_id', $purchase_request->id)->whereNotIn('id',$remove_null)->delete();
        }
        if (empty($request->items))
        {
          PurchaseRequestItem::where('purchase_request_id', $purchase_request->id)->delete();
        }

        }

        $this->params['results_count'] = 1;
        $this->params['data'] =  $purchase_request->load('item');
        return $this->sendResponse($this->params, 'Resource updated successfully');  
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {   
      try {
        $pr = PurchaseRequest::find($id)->delete();
        if($pr){
          $this->params['result'] = $pr ;
        }
        return $this->sendResponse($this->params, 'Resource deleted successfully');
      } catch ( Error $e) {   // Catch Error
        $this->params['result'] = $e->getMessage() ;
        return response()->json($this->params['result'], 500);
      }
  }

  public function approve_po1(Request $request,PurchaseRequest $purchase_request)
  {
     $status = $request->input('status');

     if (true == $purchase_request->approval->approver_1) {
      return $this->sendResponse([], 'Resource status was already approved');
     }

     if($status == true){
          $purchase_request->approval->approver_1 = $status;
     }else{
          $validated = $request->validate([
              'disapproval_reason' => 'required',
          ]);
          $purchase_request->approval->disapproval_reason1 = $request->disapproval_reason;
          $purchase_request->approval->approver_1 = $status;
     }

     $purchase_request->approval->save();

     return $this->sendResponse([], 'Resource updated successfully');
  }

  public function approve_po2(Request $request, PurchaseRequest $purchase_request)
  {
      $status = $request->input('status');

      if (false == $purchase_request->approval->approver_1) {
          return $this->sendResponse([], 'Resource approver_1 status is not approved');
      }

      if (true == $purchase_request->approval->approver_2) {
          return $this->sendResponse([], 'Resource status was already approved');
      }

      if($status == true){
           $purchase_request->approval->approver_2 = $status;
           $purchase_request->status = "approved";

           // After Purchase request is approved by the two head approver Purchase request will become Purchase order
           $purchase_request->approved_by = Auth::user()->id;

           // Gather the payment scheme of an item from vendor credit terms 
           $get_payment_terms = VendorCreditTerms::where('vendor_id',$purchase_request->item->vendor_id)
           ->where('item_id', $purchase_request->item->item_id)
           ->first();  
   
           $get_item_free_goods = Item::where('id', $purchase_request->item->item_id)->first();
    
           if ($get_payment_terms == null) {
             return $this->sendResponse([], 'Resource VendorCreditTerms does not exists');
           }
   
           $grand_total = $purchase_request->item->quantity_ordered * $purchase_request->item->unit_cost;
           $down_payment = $get_payment_terms->down_payment /100;
           $payment_upon_delivery = $get_payment_terms->upon_delivery /100;
           $days_percentage = $get_payment_terms->days_percentage /100;
           
           $data = [
             'reference_number' => $purchase_request->reference_number,
             'vendor_id' => $purchase_request->item->vendor_id,
             'payment_scheme_dp' => $down_payment,
             'payment_scheme_pud' => $payment_upon_delivery,
             'payment_scheme_pdc' => $days_percentage,
             'payment_terms' => $get_payment_terms->down_payment.'% upon PO '. $get_payment_terms->upon_delivery.'% Upon Delivery '.$get_payment_terms->days_percentage.'% '.$get_payment_terms->days_upon_delivery.'days PDC',
             'estimated_delivery_date' => Carbon::now()->addDays($get_item_free_goods->vendor_lead_time),
             'date_needed' => $purchase_request->date_needed,
             'particulars' => $purchase_request->particulars,
             'description' => $purchase_request->description,
             'downpayment' => $grand_total * $down_payment,
             'payment_upon_delivery' => $grand_total * $payment_upon_delivery,
             'pdc' => $grand_total * $days_percentage,
             'grand_total' => $grand_total,
             'status' => 'pending',  
             ];
  
           $purchase_order = PurchaseOrder::create($data);
           $purchase_order->reference_number ='PO-'.sprintf($this->code_format, $purchase_order->id);
           $purchase_order->save();
   
           $item = [
             'item_id' => $purchase_request->item->item_id,
             'vendor_id' => $purchase_request->item->vendor_id,
             'unit_cost' => $purchase_request->item->unit_cost,
             'uom' => $purchase_request->item->uom,
             'quantity_ordered' => $purchase_request->item->quantity_ordered,
             'free_goods'=> $get_item_free_goods->free_goods,
             'total_quantity' => $purchase_request->item->quantity_ordered + $get_item_free_goods->free_goods,
             'total' => $purchase_request->item->quantity_ordered * $purchase_request->item->unit_cost, // Calculate Total Price
           ];
           $purchase_order->items()->create($item);
   
           $approval = [
               'approver1' => 0,
               'approver2' => 0,
               'approver3' => 0,
   
             ];

           $purchase_order->approval()->create($approval);

      }else{
           $validated = $request->validate([
               'disapproval_reason' => 'required',
           ]);
           $purchase_request->approval->disapproval_reason2 = $request->disapproval_reason;
           $purchase_request->approval->approver_2 = $status;
           $purchase_request->status = "cancelled";
      }
      $purchase_request->save();
      $purchase_request->approval->save();

      return $this->sendResponse([], 'Resource updated successfully');
  }


}
