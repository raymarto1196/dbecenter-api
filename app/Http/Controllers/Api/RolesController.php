<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Spatie\Permission\Models\Role;

/**
 *
 * @OA\Schema(

 * @OA\Xml(name="Roles"),
 * @OA\Property(property="id", type="integer",  example="1"),
 * @OA\Property(property="name", type="string",  example="ADMINISTRATOR"),
 * @OA\Property(property="guard_name", type="string", example="web"),
 * @OA\Property(property="created_at", type="string", format="date-time", example="2019-02-25T12:59:20Z"),
 * @OA\Property(property="updated_at", type="string",  format="date-time", example="2019-02-25T12:59:20Z")
 * )
 *
 * Class role
 *
 */


class RolesController extends Controller
{
    /**
     * @OA\Get(
     * path="/api/v1/roles",
     * tags={"Role"},
     * operationId="rolesIndex",
     * description="Get List of roles",
     * security={
     *       {"api_key": {}}
     *     },
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="List of roles successfully retrieved"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/RolesController"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     *
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Display a listing of the roles.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->params['results_count'] = Role::count();
        $this->params['results'] = Role::withCount('users')->get();
        return $this->sendResponse($this->params, 'Roles successfully retrieved.');
    }

     /**
     * @OA\Post(
     * path="/api/v1/roles",
     * tags={"Role"},
     * operationId="RolesStore",
     * description="Create Roles",
     * security={
     *       {"api_key": {}}
     *     },
     *  @OA\RequestBody(
     *    required=true,
     *    description="Create Roles",
     *    @OA\JsonContent(
     *       required={"name","guard_name"},
     *        @OA\Property(property="name", type="string",  example="ADMINISTRATOR"),
     *        @OA\Property(property="guard_name", type="string",  example="web"),
     *
     *
     *    ),
     * ),
     * @OA\Response(
     *    response=400,
     *    description="Bad Request",
     *    @OA\JsonContent(
     *            @OA\Property(property="success", type="boolean", example=false),
     *       @OA\Property(property="message", type="string", example="Validation Error"),
     *    )
     *  ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     *
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Create New Roles.
     *
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $rule = [
            'name' => 'required|unique:roles,name'
        ];

        if ($request->has('user_ids') && $request->input('user_ids')) {
            $rule['user_ids'] = ['array', 'exists:users,id'];
        }

        $request->validate($rule);

        $role_props = [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
        ];

        $role = Role::create($role_props);

        if  ($request->input('user_ids')) {
            $users = User::whereIn('id', $request->input('user_ids'))->get();
            foreach ($users as $user) {
                $user->assignRole($role->id);
            }
        }

        $this->params['results'] = $role;

        return $this->sendResponse($this->params, 'Resource successfully created.', false, Response::HTTP_CREATED);
    }

    /**
     * @OA\PUT(
     * path="/api/v1/roles/{id}",
     * tags={"Role"},
     * operationId="RolesUpdate",
     * description="Update a Role by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="role id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *  @OA\RequestBody(
     *    required=true,
     *    description="Update a Role by ID",
     *    @OA\JsonContent(
     *       required={"name","guard_name"},
     *       @OA\Property(property="name", type="integer",  example="ADMINISTRATOR"),
     *       @OA\Property(property="guard_name", type="string",  example="web"),
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="User successfully updated"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/RolesController"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=400,
     *    description="Bad Request",
     *    @OA\JsonContent(
     *            @OA\Property(property="success", type="boolean", example=false),
     *       @OA\Property(property="message", type="string", example="Validation Error"),
     *    )
     *  ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description=" Role Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example=" Role Not Found"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Update a Role by ID
     *
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Role $role)
    {
        $rules = [
            'name' => 'required|unique:roles,name,'.$role->id. 'id',
            'permissions' => 'array'
        ];

        if ($request->has('user_ids') && $request->input('user_ids')) {
            $rule['user_ids'] = ['array', 'exists:users,id'];
        }

        // Validation
        $request->validate($rules);

        $attached_permissions = $role->permissions->pluck('id');
        $permissions = $request->input('permissions', $attached_permissions);

        $role_props = [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
        ];

        if ($request->input('user_ids')) {
            $users = User::whereIn('id', $request->input('user_ids'))->get();

            foreach ($users as $user) {
                $user->assignRole($role->id);
            }
        }
    
        $remove_users = User::whereNotIn('id',$request->input('user_ids'))->get();
        foreach ($remove_users as $remove_user) {
            $remove_user->removeRole($role->id);
        }

        $role->update($role_props);

        $role->syncPermissions($permissions);

        $this->params['results'] = $role;

        return $this->sendResponse($this->params, 'Resource successfully updated');
    }

     /**
     * @OA\Get(
     * path="/api/v1/roles/{id}",
     * tags={"Role"},
     * operationId="roleShow",
     * description="Get a roles by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="roles id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="Resource successfully retrieved"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/RolesController"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description="Role Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example="Role Not Found"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Get Role By ID.
     *
     * @return \Illuminate\Http\Response
     */

    public function show(Request $request, Role $role)
    {
        $role->syncPermissions([3,4]);
        $role->users_count = $role->users()->count();
        $role->load('permissions');
        $this->params['result'] = $role;

        return $this->sendResponse($this->params, 'Resource successfully retrievd');
    }

    public function destroy(Request $request, Role $role)
    {
        if($role->id == 1 || $role->id == 2 || $role->id == 3)
        {
            return $this->sendResponse([], $role->name.' cannot be deleted');
        }

        $role->delete();
        $this->params['result'] = [];

        return $this->sendResponse($this->params, 'Resource successfully deleted');
    }

    public function add_user_roles(){

    }
}
