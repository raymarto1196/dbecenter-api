<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\DeliveryReceiptResource;
use App\Http\Resources\InvoiceResource;
use App\Models\DeliveryReceipt;
use App\Models\Invoice;
use App\Models\SalesOrder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class InvoicesController extends Controller
{
    public function index_last_072021(Request $request) {
        $results = SalesOrder::where([
            [function ($query) use ($request){

            if (($search_string = $request->search_string)) {
                $search_string = '%' . $search_string . '%';
                $query->orWhere('created_at', 'LIKE', $search_string)->get();
                $query->orWhere('po_number', 'LIKE', $search_string)->get();
                $query->orWhere('so_number', 'LIKE', $search_string)->get();
                $query->orWhere('customer_id', 'LIKE', $search_string)->get();
                $query->orWhere('estimated_delivery_date', 'LIKE', $search_string)->get();
                $query->orWhere('net_amount', 'LIKE', $search_string)->get();
                $query->orWhere('gross_amount', 'LIKE', $search_string)->get();
                $query->orWhere('received_by', 'LIKE', $search_string)->get();
                $query->orWhere('status', 'LIKE', $search_string)->get();

            }
            if(($start_date = $request->start_date) && ($end_date = $request->end_date)){

                $start_date = Carbon::createFromFormat('m-d-Y H:i:s', $start_date)->format('Y-m-d H:i:s');
                $end_date = Carbon::createFromFormat('m-d-Y H:i:s', $end_date)->format('Y-m-d H:i:s');
                $query->whereBetween('created_at', [$start_date, $end_date])->get();
            }
        }]

        ])
        ->get();

        $this->params['results_count'] = $results->count();
        $this->params['results'] = $results;
        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

    public function index(Request $request)
    {
        $data = InvoiceResource::collection(Invoice::all());

        $this->params['total_request'] = $data->count();
        $this->params['results'] = $data;
        return $this->sendResponse($this->params, 'Resource retrieved successfully');

    }

    public function show(Invoice $invoice)
    {
        $result = new InvoiceResource($invoice);

        $this->params['results'] = $result;
        return $this->sendResponse($this->params, 'Resource successfully retrieved.');
    }


    public function show2(Request $request, SalesOrder $sales_order)
    {
        $this->params['results_count'] = 1;
        $this->params['results'] = $sales_order->load('items', 'statuses');
        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

    public function store(Request $request, DeliveryReceipt $delivery_receipt)
    {
        $si_number = sprintf($this->code_format,  (Invoice::max('id')+1));

        $sales_invoice = [
            'reference_number'          => $delivery_receipt->reference_number,
            'sales_order_id'            => $delivery_receipt->sales_order_id,
            'picklist_id'               => $delivery_receipt->id,
            'customer_id'               => $delivery_receipt->customer_id,
            'delivery_receipt_id'       => $delivery_receipt->id,
            'terms'                     => $delivery_receipt->terms,
            'customer_address'          => $delivery_receipt->customer_address,
            'delivery_receipt_number'   => $delivery_receipt->delivery_receipt_number,
            'sales_invoice_number'      => $request->input('sales_invoice_number', $si_number),
            'sales_invoice_date'        => $request->input('sales_invoice_date', now()),
            'picked_by'                 => $delivery_receipt->picked_by,
            'marked_by'                 => $delivery_receipt->marked_by,
            'packed_by'                 => $delivery_receipt->packed_by,
        ];
        
        $data = Invoice::create($sales_invoice); 

        $this->params['results_count'] = 1;   
        $this->params['results'] = $data;
        return $this->sendResponse($this->params, 'Resource successfully created.', false, Response::HTTP_CREATED);
    }

    public function store2(Request $request)
    {
        $so_props = [
    		'po_number' => $request->input('po_number'),
    		'customer_id' => $request->input('customer_id'),
    		'customer_name' => $request->input('customer_name'),
            'estimated_delivery_date' => $request->input('estimated_delivery_date'),
            'net_amount' => $request->input('net_amount'),
            'gross_amount' => $request->input('gross_amount'),
            'received_by' => $request->input('received_by'),
            'status' => $request->input('status'),
    	];

    	$newSO = SalesOrder::create($so_props);
        //$newSO->order_number = 'SO-' sprintf('%04d', $newSO->id);
        $newSO->so_number = 'BK-'.Carbon::now()->format('ymd').'-'. sprintf('%06d', $newSO->id);
        $newSO->save();

    	if ($request->has('items') && ($request->input('items') != null) && is_array($request->input('items'))) {
    		$items = $request->input('items');

    		// add items to sales order
    		foreach ($items as $key => $item) {

		    	$SOItemData = [
		    		'item_id' => $item['item_id'],
                    'unit_price' => $item['unit_price'],
                    'quantity' => $item['quantity'],
                    'gross_amount' => $item['gross_amount'],
                    'discount' => $item['discount'],
                    'net_amount' => $item['net_amount'],
                    'promo' => $item['promo']
		    	];

		    	$newSO->items()->create($SOItemData);
	    	}
    	}

        $newSO_status = [
            'status' => $request->input('status'),
            'remarks' => $request->input('remarks'),
            'updated_by' => auth()->user()->id
        ];

        $newSO->statuses()->create($newSO_status);

        $newSO->load('items', 'statuses');

        $this->params['results_count'] = 1;
        $this->params['results'] = $newSO;

        return $this->sendResponse($this->params, 'Resource created successfully.', false, Response::HTTP_CREATED);

    }

    public function update(Request $request, Invoice $invoice)
    {
    	$sales_invoice = [
            'sales_invoice_number' => $request->input('sales_invoice_number'),
            'sales_invoice_date'   => $request->input('sales_invoice_date'),
    	];

        $result = $invoice->update($sales_invoice);
        $this->params['results'] = $result;
        return $this->sendResponse($this->params, 'Resource successfully updated.');

    }

    public function update2(Request $request, SalesOrder $sales_order)
    {

        $so_props = [
    		'so_number' => $request->input('so_number', $sales_order->so_number),
    		'po_number' => $request->input('po_number', $sales_order->po_number),
    		'customer_name' => $request->input('customer_name', $sales_order->customer_name),
            'estimated_delivery_date' => $request->input('estimated_delivery_date', $sales_order->estimated_delivery_date),
            'net_amount' => $request->input('net_amount', $sales_order->net_amount),
            'gross_amount' => $request->input('gross_amount', $sales_order->gross_amount),
            'received_by' => $request->input('received_by', $sales_order->received_by),
    	];


        $sales_order->update($so_props);

        $sales_order->load('items', 'statuses');

        return $this->sendResponse($sales_order, 'Resource updated successfully');

    }

    public function change_status(Request $request, SalesOrder $sales_order)
    {
        $request->validate([
            'status' => ['required']
        ]);

        // return $request->status;

        $sales_order->status = $request->input('status');

        $new_status = [
            'status' => $request->input('status'),
            'remarks' => $request->input('remarks'),
            'updated_by' => auth()->user()->id
        ];

        $sales_order->statuses()->create($new_status);
        $sales_order->save();

        $sales_order->load('items', 'statuses');

        return $this->sendResponse($sales_order, 'Resource updated successfully');

    }


    public function destroy(SalesOrder $sales_order)
    {
        $sales_order->delete();
        return $this->sendResponse([], 'Resource deleted successfully');

        // $deleted = false;
        // DB::beginTransaction();
        // try {

        //     // Do the operations

        //     // Commmit Transaction
        //     DB::commit();
        //     $deleted = true;
        // } catch (\Exception $e) {
        //     DB::rollBack();
        // }

        // if (! $deleted) {
        //     return $this->sendError('Error while processing', [], 500);
        // }

        // return $this->sendResponse([], 'Resource deleted successfully');
    }

    public function createStatusForOrder(Request $request, SalesOrder $sales_order) {
        return $sales_order;
    }



}

