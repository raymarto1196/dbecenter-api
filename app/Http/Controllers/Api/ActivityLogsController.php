<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ActivityLog;

class ActivityLogsController extends Controller
{
    public function __construct() {}

    public function __invoke(Request $request, $model=null)
    {
        $query = ActivityLog::query();

        $results = $query->get();
        $results_count = $results->count();

        $this->params['results_count'] = $results_count;
        $this->params['results'] = $results;

        return $this->sendResponse($this->params, 'Resource retrived successfully');
    }
}
