<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\ProjectBid;
use Illuminate\Http\Request;

class ProjectBidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item_data = Item::findOrFail($request->input('item_id'));

        $data = [
            'user_id'       => $this->auth_user->id,
            'item_id'       => $item_data->id,
            'contractor_id' => $item_data->user_id,
            'price'         => $request->input('price')
        ];

        $result = ProjectBid::Create($data);

        $this->params['message'] = 'Resource created successfully';
        $this->params['results_count'] = 1;
        $this->params['results'] = $result;

        return $this->sendResponse($this->params);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = ProjectBid::findOrFail($id);

        $this->params['message'] = 'Resource retrieved successfully';
        $this->params['results_count'] = 1;
        $this->params['results'] = $result;

        return $this->sendResponse($this->params);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectBid $projectBid)
    {
        $data = [
            'price' => $request->input('price')
        ];

        $result = $projectBid->update($data);

        $this->params['message'] = 'Resource updated successfully';
        $this->params['results_count'] = 1;
        $this->params['results'] = $result;

        return $this->sendResponse($this->params);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = ProjectBid::findOrFail($id);
        $result->delete();

        $this->params['message'] = 'Resource deleted successfully';
        $this->params['results_count'] = 1;
        $this->params['results'] = true;
        return $this->sendResponse($this->params);
    }

    public function get_item_bids($item_id){
        $result = Item::findOrFail($item_id);

        $this->params['message'] = 'Resource retrieved successfully';
        $this->params['results_count'] = 1;
        $this->params['results'] = $result->load('bids.user.profile:user_id,firstname,lastname,profile_picture');

        return $this->sendResponse($this->params);
    }

    public function get_vendor_bids(){
        $result = ProjectBid::where('user_id',$this->auth_user->id)->get();

        $this->params['message'] = 'Resource retrieved successfully';
        $this->params['results_count'] = $result->count();
        $this->params['results'] = $result->load('item:id,item_name,item_image,identifier');
        return $this->sendResponse($this->params);
    }
}
