<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ViewUserData;
use Illuminate\Http\Request;

class ViewUserDataController extends Controller
{
    public function index() {
        $query = ViewUserData::query();

        $query->where('name', 'like', 'Admin%');

        $results = $query->get();

        return response()->json($results);
    }
}
