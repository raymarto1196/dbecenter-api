<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\StandardIndustryCode;
use Illuminate\Http\Request;

class StandardIndustryCodeController extends Controller
{
   public function get_naic_codes(Request $request){
    $results = StandardIndustryCode::where([
        [function ($query) use ($request) {
            if (($search_string = $request->search_string)) {
                $search_string = '%' . $search_string . '%';
                $query->orWhere('code', 'LIKE', $search_string);
                $query->orWhere('description', 'LIKE', $search_string);
            }
        }]
        ])->get();
    
    $this->params['message'] = 'Resource retrieved successfully';
    $this->params['results_count'] = $results->count();
    $this->params['results'] = $results;

    return $this->sendResponse($this->params);
   }
}
