<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ItemTaxOption;
use Illuminate\Http\Request;

class ItemTaxOptionsController extends Controller
{
    public function index()
    {
        $this->params = ItemTaxOption::all();
        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }
}
