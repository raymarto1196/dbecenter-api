<?php

namespace App\Http\Controllers\Api;
use App\Models\Courier;
use App\Models\CourierWaybill;
use App\Models\CourierService;
use App\Models\Customer;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class CouriersController extends Controller
{

    protected $documents = ['courier_logo','business_permit','cor','aol_gis','itr','bir_cert','cert_of_good_credit','audited_fs','tax_clearance'];
    protected $allowedfileExtension = ['pdf','jpg','png','docx'];
     /**
     * @OA\Get(
     * path="/api/v1/couriers",
     * tags={"Courier Management"},
     * operationId="CourierIndex",
     * description="Get List of Courier",
     * security={
     *       {"api_key": {}}
     *     },
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="List of Courier successfully retrieved"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/Courier"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     *
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Display a listing of the Courier.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //do not delete
        // if (! $this->auth_user->can('list_courier')) {
        //
        //     return response()->json([
        //         'responseMessage' => 'You do not have the required authorization.',
        //         'responseStatus'  => 403,
        //     ], Response::HTTP_NOT_FOUND
        //
        //   );
        // }

        // $query = Courier::all('waybill');

        // $results = $query->get();

        // $results = Courier::all();

        // $this->params['results_accredited'] = $results->where('accredited', 1)->count();
        // $this->params['results_not_accredited'] = $results->where('accredited', 0)->count();
        // $this->params['results_count'] = $results->count();
        // $this->params['results'] = $results;

        // return $this->sendResponse($this->params, 'Resource retrieved successfully');

        $results = Courier::where([
            [function ($query) use ($request){
                if (($search_string = $request->search_string)) {

                    $search_string = '%' . $search_string . '%';
                    $query->orWhere('company_name', 'LIKE', $search_string)
                    ->get();
                }

                if(($start_date = $request->start_date) && ($end_date = $request->end_date)){

                    $start_date = Carbon::createFromFormat('m-d-Y H:i:s', $start_date)->format('Y-m-d H:i:s');
                    $end_date = Carbon::createFromFormat('m-d-Y H:i:s', $end_date)->format('Y-m-d H:i:s');
                    $query->whereBetween('created_at', [$start_date, $end_date])->get();
                }
            }]
            ])->get();
        // ])->paginate(
        //     $request->pageLimit
        // );

        $this->params['results_accredited'] = $results->where('accredited',  1)->count();
        $this->params['results_not_accredited'] = $results->where('accredited', 0)->count();
        $this->params['results_count'] =$results->count();
        $this->params['results'] =$results;
        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

    /**
     * @OA\Post(
     * path="/api/v1/couriers",
     * tags={"Courier Management"},
     * operationId="CourierStore",
     * description="Create Courier",
     * security={
     *       {"api_key": {}}
     *     },
     *  @OA\RequestBody(
     *    required=true,
     *    description="Create Courier",
     *    @OA\JsonContent(
     *       required={"company_name","registered_address","satellite_office","warehouse_address","telephone_no","website_address","bank_details","email_address","dti_sec_reg_no","tin_vat_reg_no","business_reg_no","iso_certificate_no","service",},
 * @OA\Property(property="company_name", type="integer",  example="Air 21"),
 * @OA\Property(property="registered_address", type="string",  example="13 Annapolis St. Cubao, Quezon City Philippines"),
 * @OA\Property(property="satellite_office", type="string", example="Manila"),
 * @OA\Property(property="warehouse_address", type="string", example="Manila"),
 * @OA\Property(property="telephone_no", type="string", example="02 (711) 4473"),
 * @OA\Property(property="website_address", type="string", example="proprietorship"),
 * @OA\Property(property="bank_details", type="string", example="f123s3"),
 * @OA\Property(property="email_address", type="string", format="email", example="raymart@gmail.com"),
 * @OA\Property(property="dti_sec_reg_no", type="string", example="3123"),
 * @OA\Property(property="tin_vat_reg_no", type="string", example="0123"),
 * @OA\Property(property="business_reg_no", type="string", example="3211"),
 * @OA\Property(property="business_reg_date", type="string", example="019-02-25"),
 * @OA\Property(property="iso_certificate_no", type="string", example="J123"),
 * @OA\Property(property="service", type="string", example="1"),
 * @OA\Property(property="iso_certified", type="string", example=1),
 * @OA\Property(property="dti_sec_reg_date", type="string", example="1"),
 * @OA\Property(property="tin_vat_reg_date", type="string", example="1"),
 * @OA\Property(property="attach_pic_logo", type="string", format="file", example="couriers/asdsddddsdsss/4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
 * @OA\Property(property="business_permit", type="string", format="file", example="couriers/asdsddddsdsss/4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
 * @OA\Property(property="cor", type="string", format="file", example="couriers/asdsddddsdsss/4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
 * @OA\Property(property="aol_gis", type="string", format="file", example="couriers/asdsddddsdsss/4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
 * @OA\Property(property="itr", type="string", format="file", example="couriers/asdsddddsdsss/4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
 * @OA\Property(property="bir_cert", type="string", format="file", example="couriers/asdsddddsdsss/4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
 * @OA\Property(property="cert_of_good_credit", type="string", format="file", example="couriers/asdsddddsdsss/4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
 * @OA\Property(property="audited_fs", type="string", format="file", example="couriers/asdsddddsdsss/4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
 * @OA\Property(property="tax_clearance", type="string", format="file", example="couriers/asdsddddsdsss/4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
 * @OA\Property(property="contacts", type="object",
 *                       type="array",
 *                       @OA\Items(
 *                          @OA\Property(property="name", type="integer", example="raymart"),
 *                          @OA\Property(property="position", type="integer",example="assistant"),
 *                          @OA\Property(property="email", type="string",example="ray@gmail.com"),
 *                     ) ),
  * @OA\Property(property="services", type="object",
 *                       type="array",
 *                       @OA\Items(
 *                          example="NCR",
 *                          example="LUZON"
 *                     ) ),
     *    ),
     * ),
     * @OA\Response(
     *    response=400,
     *    description="Bad Request",
     *    @OA\JsonContent(
     *            @OA\Property(property="success", type="boolean", example=false),
     *       @OA\Property(property="message", type="string", example="Validation Error"),
     *    )
     *  ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     *
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Create New Courier.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Courier $courier)
    {
        //do not delete
        // if (! $this->auth_user->can('create_courier')) {
        //
        //     return response()->json([
        //         'responseMessage' => 'You do not have the required authorization.',
        //         'responseStatus'  => 403,
        //     ], Response::HTTP_NOT_FOUND
        //
        //   );
        // }

        $validated = $request->validate([
            'company_name' => 'required|unique:couriers,company_name',
            'registered_address' => 'required',
            'warehouse_address' => 'required',
            'satellite_office' => 'required',
            'telephone_no' => 'required',
            'website_address' => 'required',
            'bank_details' => 'required',
            'email_address' => 'required|email',
            'dti_sec_reg_no' => 'required',
            'tin_vat_reg_no' => 'required',
            'business_reg_date' => 'required',
            'business_reg_no' => 'required',
            'iso_certified' => 'required|integer',
            'dti_sec_reg_date' => 'required',
            'tin_vat_reg_date' => 'required',
            'iso_certificate_no' => 'required',
            'services' => 'array',
            'contacts' => 'array',
            'serviceable_areas' => 'array'
        ]);

        // Save Courier's properties
        $courier = $courier->create($validated);

        $foldername = "couriers/courier-{$courier->id}/";

        foreach ($this->documents as $document) {
            /**
             * Store file uploaded from file object
             */
            // if ($file = $request->file($document)) {
            //     $extension = $file->getClientOriginalExtension();
            //     $filename = $document.'.'.$extension;
            //     $courier->$document = uploadFileToPath($foldername, $file, $filename);
            // }

            /**
             * Store file uploaded from Base64
             */
            if($request->has($document) && $request->input($document)){
                $base64_image = $request->input($document);

                // Currently accepts image only
                if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                    $data = substr($base64_image, strpos($base64_image, ',') + 1);
                    $extension = getFileExtension($base64_image);
                    $file = base64_decode($data);
                    $filename = $document.'.'.$extension;

                    if(Storage::put($foldername. $filename, $file)){
                        $image_url = Storage::url($foldername. $filename);
                        $courier->$document = $image_url;
                    }
                }
            }
        }



        // Resave courier to attach the documents uploaded
        $courier->save();

        $this->params['results_count'] = 1;
        $this->params['results'] = $courier;

        return $this->sendResponse($this->params, 'Resource created successfully', false, Response::HTTP_CREATED);
    }
     /**
     * @OA\Get(
     * path="/api/v1/couriers/{id}",
     * tags={"Courier Management"},
     * operationId="CourierShow",
     * description="Get a Courier by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="Courier id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="Courier successfully retrieved"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/Courier"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description="Courier Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example="Courier Not Found"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Get Courier By ID.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Courier $courier)
    {
        //do not delete
        // if (! $this->auth_user->can('read_courier')) {
        //
        //     return response()->json([
        //         'responseMessage' => 'You do not have the required authorization.',
        //         'responseStatus'  => 403,
        //     ], Response::HTTP_NOT_FOUND
        //
        //   );
        // }

        $this->params['results_count'] = 1;
        $this->params['results'] = $courier;

        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

    /**
     * @OA\PUT(
     * path="/api/v1/couriers/{id}",
     * tags={"Courier Management"},
     * operationId="couriersUpdate",
     * description="Update a couriers by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="couriers id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     * @OA\RequestBody(
     *    required=true,
     *    description="Update a couriers by ID",
     *    @OA\JsonContent(
      *       required={"company_name","registered_address","satellite_office","warehouse_address","telephone_no","website_address","bank_details","email_address","dti_sec_reg_no","tin_vat_reg_no","business_reg_no","iso_certified","iso_certificate_no","service",},
     * @OA\Property(property="company_name", type="integer",  example="Air 21"),
     * @OA\Property(property="registered_address", type="string",  example="13 Annapolis St. Cubao, Quezon City Philippines"),
     * @OA\Property(property="satellite_office", type="string", example="Manila"),
     * @OA\Property(property="warehouse_address", type="string", example="Manila"),
     * @OA\Property(property="telephone_no", type="string", example="02 (711) 4473"),
     * @OA\Property(property="website_address", type="string", example="proprietorship"),
     * @OA\Property(property="bank_details", type="string", example="f123s3"),
     * @OA\Property(property="email_address", type="string",format="email", example="raymart@gmail.com"),
     * @OA\Property(property="dti_sec_reg_no", type="string", example="3123"),
     * @OA\Property(property="tin_vat_reg_no", type="string", example="0123"),
     * @OA\Property(property="business_reg_no", type="string",  example="3211"),
     * @OA\Property(property="business_reg_date", type="string", example="019-02-25"),
     * @OA\Property(property="iso_certificate_no", type="string", example="J123"),
     * @OA\Property(property="dti_sec_reg_date", type="string", example="J123"),
     * @OA\Property(property="tin_vat_reg_date", type="string", example="J123"),
     * @OA\Property(property="service", type="string", example="1"),
     * @OA\Property(property="iso_certified", type="string", example=1),
     * @OA\Property(property="attach_pic_logo", type="string", format="file", example="couriers/asdsddddsdsss/4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
     * @OA\Property(property="business_permit", type="string", format="file", example="couriers/asdsddddsdsss/4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
     * @OA\Property(property="cor", type="string", format="file", example="couriers/asdsddddsdsss/4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
     * @OA\Property(property="aol_gis", type="string", format="file", example="couriers/asdsddddsdsss/4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
     * @OA\Property(property="itr", type="string", format="file", example="couriers/asdsddddsdsss/4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
     * @OA\Property(property="bir_cert", type="string", format="file", example="couriers/asdsddddsdsss/4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
     * @OA\Property(property="cert_of_good_credit", type="string", format="file", example="couriers/asdsddddsdsss/4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
     * @OA\Property(property="audited_fs", type="string", format="file", example="couriers/asdsddddsdsss/4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
     * @OA\Property(property="tax_clearance", type="string", format="file", example="couriers/asdsddddsdsss/4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
     * @OA\Property(property="contacts", type="object",
     *                       type="array",
     *                       @OA\Items(
     *                          @OA\Property(property="name", type="integer", example="raymart"),
     *                          @OA\Property(property="position", type="integer",example="assistant"),
     *                          @OA\Property(property="email", type="string",example="ray@gmail.com"),
     *                     ) ),
     * @OA\Property(property="services", type="object",
     *                       type="array",
     *                       @OA\Items(
     *                          example="NCR",
     *                          example="LUZON"
     *                     ) ),
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="courier successfully updated"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/Courier"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=400,
     *    description="Bad Request",
     *    @OA\JsonContent(
     *            @OA\Property(property="success", type="boolean", example=false),
     *       @OA\Property(property="message", type="string", example="Validation Error"),
     *    )
     *  ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description="courier Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example="courier Found"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Update a courier by ID
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Courier $courier)
    {
        //do not delete
        // if (! $this->auth_user->can('update_courier')) {
        //
        //     return response()->json([
        //         'responseMessage' => 'You do not have the required authorization.',
        //         'responseStatus'  => 403,
        //     ], Response::HTTP_NOT_FOUND
        //
        //   );
        // }

        $validated = $request->validate([
            'company_name' => 'required|unique:couriers,company_name,'.$courier->id.'id',
            'registered_address' => 'required',
            'warehouse_address' => 'required',
            'satellite_office' => 'required',
            'telephone_no' => 'required',
            'website_address' => 'required',
            'bank_details' => 'required',
            'email_address' => 'required|email',
            'dti_sec_reg_no' => 'required',
            'tin_vat_reg_no' => 'required',
            'business_reg_date' => 'required',
            'business_reg_no' => 'required',
            'iso_certified' => 'required|integer',
            'dti_sec_reg_date' => 'required',
            'tin_vat_reg_date' => 'required',
            'iso_certificate_no' => 'required',
            'services' => 'array',
            'contacts' => 'array',
            'serviceable_areas' => 'array'
        ]);

        $courier->update($validated);

        /**
         * File uploads
         * Upload documents to items folder
         */
        $foldername = "couriers/courier-{$courier->id}/";

        foreach ($this->documents as $document) {
            /**
             * Store file uploaded from file object
             */
            // if ($file = $request->file($document)) {
            //     $extension = $file->getClientOriginalExtension();
            //     $filename = $document.'.'.$extension;
            //     $courier->$document = uploadFileToPath($foldername, $file, $filename);
            // }

            /**
             * Store file uploaded from Base64
             */
            if($request->has($document) && $request->input($document)){
                $base64_image = $request->input($document);

                // Currently accepts image only
                if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                    $data = substr($base64_image, strpos($base64_image, ',') + 1);
                    $extension = getFileExtension($base64_image);
                    $file = base64_decode($data);
                    $filename = $document.'.'.$extension;

                    if(Storage::put($foldername. $filename, $file)){
                        $image_url = Storage::url($foldername. $filename);
                        $courier->$document = $image_url;
                    }
                }
            }
        }

        // Resave courier to attach the documents uploaded
        $courier->save();

        $this->params['results_count'] = 1;
        $this->params['results'] = $courier;

        return $this->sendResponse($this->params, 'Resource updated successfully');
    }

    /**
     * @OA\Delete(
     * path="/api/v1/couriers/{id}",
     * tags={"Courier Management"},
     * operationId="departmentDelete",
     * description="Delete a courier by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="courier",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="courier successfully deleted"),
     *        )
     *     ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description="courier Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example="courier Not Found"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Delete a courier by ID
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Courier $courier)
    {   
        //do not delete
        // if (! $this->auth_user->can('delete_courier')) {
        //
        //     return response()->json([
        //         'responseMessage' => 'You do not have the required authorization.',
        //         'responseStatus'  => 403,
        //     ], Response::HTTP_NOT_FOUND
        //
        //   );
        // }

        $courier->delete();

        return $this->sendResponse([], 'Resource deleted successfully');
    }

    /**
     * @OA\PUT(
     * path="/api/v1/couriers/{id}/updatewaybill",
     * tags={"Courier Management"},
     * operationId="couriersUpdate waybill",
     * description="Update a couriers waybill by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="couriers waybill id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     * @OA\RequestBody(
     *   required=true,
     *   description="Update a couriers waybill by ID",
     *   @OA\JsonContent(
     *   required={"shippers_number","Shippers_name","package_code","width","height","item_description","DR","PR","SR","paragraph_text","service_type","no_boxes","s_phone"},
     * @OA\Property(property="shippers_number", type="integer",  example="s13"),
     * @OA\Property(property="Shippers_name", type="string",  example="air 21"),
     * @OA\Property(property="package_code", type="string", example="3123"),
     * @OA\Property(property="width", type="string", example="31"),
     * @OA\Property(property="height", type="string", example="11"),
     * @OA\Property(property="item_description", type="string", example="dasd"),
     * @OA\Property(property="DR", type="string", example="f123s3"),
     * @OA\Property(property="PR", type="string", example="d23"),
     * @OA\Property(property="SR", type="string", example="3123"),
     * @OA\Property(property="paragraph_text", type="string", example="sadsad"),
     * @OA\Property(property="service_type", type="string", format="email", example="3211"),
     * @OA\Property(property="no_boxes", type="string", example="019-02-25"),
     * @OA\Property(property="s_phone", type="string", example="J123"),
     * @OA\Property(property="s_mobile", type="string", example="1"),
     * @OA\Property(property="s_pickup_address", type="string", example="1"),
     * @OA\Property(property="s_postal_code", type="string", example="1"),
     * @OA\Property(property="s_shippers_detail_email", type="string", example="ray@gmail.com"),
     * @OA\Property(property="cosignee_name", type="string", example="1"),
     * @OA\Property(property="address_option", type="string", example="1"),
     * @OA\Property(property="c_phone", type="string", example="1"),
     * @OA\Property(property="c_mobile", type="string", example="1"),
     * @OA\Property(property="c_address", type="string", example="1"),
     * @OA\Property(property="c_postal_code", type="string", example="1"),
     * @OA\Property(property="c_email", type="string", example="ray@gmail.com"),
     * @OA\Property(property="c_special_instruction", type="string", example="1"),
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="courier successfully updated"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/Courier"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=400,
     *    description="Bad Request",
     *    @OA\JsonContent(
     *            @OA\Property(property="success", type="boolean", example=false),
     *       @OA\Property(property="message", type="string", example="Validation Error"),
     *    )
     *  ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description="courier waybill Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example="courier Found"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Update a courier waybill by ID
     *
     * @return \Illuminate\Http\Response
     */
    public function create_waybill(Request $request, Courier $courier)
    {
        $validated = $request->validate([
            'shipper_number' => 'required',
            'shipper_id' => 'required',
            'package_code' => "",
            'length' => "",
            'total_weight' => "",
            'width' => "",
            'height' => "",
            'item_description' => "",
            'delivery_receipt' => "",
            'purchase_request' => "",
            'sales_invoice' => "",
            'description' => "",
            'service_type' => "",
            'no_boxes' => "",

            'shipper_phone' => " ",
            'shipper_mobile' => " ",
            'shipper_pickup_address' => " ",
            'shipper_postal_code' => " ",
            'shipper_detail_email' => 'email',

            'consignee_name' => " ",
            'address_option' => " ",

            'consignee_phone' => " ",
            'consignee_mobile' => " ",
            'consignee_address' => " ",
            'consignee_postal_code' => " ",
            'consignee_email' => 'email',
            'consignee_special_instruction' => " ",

        ]);

        $courierwaybill = $courier->update($validated);

        $this->params['waybill'] = $courierwaybill;
        return $this->sendResponse($this->params, 'Resource successfully updated.', false, Response::HTTP_CREATED);
    }


    /**
     * @OA\PUT(
     * path="/api/v1/couriers/{id}/change_to_credited",
     * tags={"Courier Management"},
     * operationId="CourierChange_to_credited",
     * description="Change_to_credited a Courier by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="Courier id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *  @OA\RequestBody(
     *    required=true,
     *    description="Change_to_credited a Courier by ID",
     *    @OA\JsonContent(
     *       required={"accredited"},
     *        @OA\Property(property="accredited", type="boolean",  example=1),
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="Courier successfully Change_to_creditedd"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/Courier"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=400,
     *    description="Bad Request",
     *    @OA\JsonContent(
     *            @OA\Property(property="success", type="boolean", example=false),
     *       @OA\Property(property="message", type="string", example="Validation Error"),
     *    )
     *  ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description="Courier Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example="Courier Found"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Change_to_credited a Courier by ID
     *
     * @return \Illuminate\Http\Response
     */
    public function change_to_credited(Request $request, Courier $courier)
    {
        $courier->accredited = 1;

        $courier->save();

        $this->params['results_count'] = 1;
        $this->params['results'] = $courier;

        return $this->sendResponse($this->params, 'Resource updated successfully');
    }

    // public function courier_requirments()
    // {
    //     //TODO get Data from costumer table and display in courier requirments for the person who have
    //     //permission to assign a certain courier to that costumer
    //     $results = CustomerCenter::all();
    //     $this->params['results'] = $results;
    //     return $this->sendResponse($this->params, 'Courier successfully retrieved.');
    // }


    // public function add_courier_requirments(Request $request, Courier $courier)$customer_id
    // {
    //     $validated = [
    //         'courier_id' => $courier->id,
    //         'industry' => "asdasd",
    //     ];
    //     $customer = Customer::find($courier->id);$customer_id couriers/{courier}/add_courier_requirments{customer_id}
    //     $customer->update($validated);
    //     $this->params['results'] = $customer;
    //     return $this->sendResponse($this->params, 'Customer Courier successfully update.');
    // }


}
