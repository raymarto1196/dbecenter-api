<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Http\Resources\Dropdowns\VendorResource;
use App\Http\Resources\WarehouseResource;
use App\Models\Vendor;
use App\Models\Warehouse;
use App\Services\DropdownService;
use Illuminate\Http\Request;

class SearchDropdownController extends Controller
{   
    protected $query = [];

    public function search (Request $request) {

        $search_params = $request->query('s');

        abort_if(!in_array($search_params, config('app.searchable_items')), 404);

        $this->query = (new DropdownService($search_params))->toArray();

        $this->params['results'] = $this->query;

        return $this->sendResponse($this->params, null);
    }
}

/**
 * Sample URL Request
 * {{baseUrl}}/dropdowns?s=warehouse
 * {{baseUrl}}/dropdowns?s=vendor
 * {{baseUrl}}/dropdowns?s=pallet_status
 * ...
 */