<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\UserProfile;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\UserRating;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Exceptions\RoleDoesNotExist;
use Spatie\Permission\Models\Permission;
use Carbon\Carbon;

class UserRatingsController extends Controller
{

    /**
     * Rate a user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['userid_rater'] = $this->auth_user->id;
        $userid_ratee = $request->input('userid_ratee');

        // Validation
        // user cannot rate itself
        $request->validate([
            'userid_ratee' => ['required', 'exists:users,id', 'different:userid_rater'],
            'rate' => ['required', 'numeric',' between:1,5']
        ]);

        $data = [
            'rate' => $request->input('rate'),
            'comment' => $request->input('comment')
        ];

        $where = ['userid_rater' => $this->auth_user->id, 'userid_ratee' => $userid_ratee];
        $rating = UserRating::updateOrCreate($where, $data);

        $this->params['results_count'] = 1;
        $this->params['message'] = 'Resource created successfully';
        $this->params['results'] = $rating;

        return $this->sendResponse($this->params, Response::HTTP_CREATED);
    }
}
