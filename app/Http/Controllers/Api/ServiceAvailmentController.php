<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\ServiceAvailment;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ServiceAvailmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item_data = Item::findOrFail($request->input('item_id'));

        $data = [
            'user_id'       => $this->auth_user->id,
            'item_id'       => $item_data->id,
            'vendor_id'     => $item_data->user_id,
            'name'          => $request->input('name'),
            'telephone'     => $request->input('telephone'),
            'description'   => $request->input('description'),
            'address'       => $request->input('address'),
            'item_name'     => $item_data->item_name,
            'identifier'    => $item_data->identifier,
            'reference_number'  => 'S-'.Carbon::now()->format('ymd').sprintf('%06d',  (ServiceAvailment::max('id')+1))
        ];

        $result = ServiceAvailment::Create($data);

        $this->params['message'] = 'Resource created successfully';
        $this->params['results_count'] = 1;
        $this->params['results'] = $result;

        return $this->sendResponse($this->params);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = ServiceAvailment::findOrFail($id);

        $this->params['message'] = 'Resource retrieved successfully';
        $this->params['results_count'] = 1;
        $this->params['results'] = $result;

        return $this->sendResponse($this->params);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = ServiceAvailment::findOrFail($id);
        $result->delete();

        $this->params['message'] = 'Resource deleted successfully';
        $this->params['results_count'] = 1;
        $this->params['results'] = true;
        return $this->sendResponse($this->params);
    }


    public function get_vendor_availed_services(){
        $result = ServiceAvailment::where('vendor_id',$this->auth_user->id)->get();

        $this->params['message'] = 'Resource retrieved successfully';
        $this->params['results_count'] = $result->count();
        $this->params['results'] = $result->load('item:id,item_name,item_image,identifier');
        $this->params['results'] = $result->load('user.profile:user_id,firstname,lastname,profile_picture');
        return $this->sendResponse($this->params);
    }

    public function get_contractor_availed_services(){
        $result = User::findOrFail($this->auth_user->id);
        $result->load('availed_services.item:id,item_name,item_image,identifier');

        $this->params['message'] = 'Resource retrieved successfully';
        $this->params['results_count'] = 1;
        $this->params['results'] = $result;
        return $this->sendResponse($this->params);
    }

    public function change_status(Request $request,$service_id){
        $result = ServiceAvailment::findOrFail($service_id);
        $result->status = $request->input('status');
        $result->save();

        $this->params['message'] = 'Resource updated successfully';
        $this->params['results_count'] = 1;
        $this->params['results'] = $result;
        return $this->sendResponse($this->params);
    }
}
