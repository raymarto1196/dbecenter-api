<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\DeliveryReceiptResource;
use App\Http\Resources\InvoiceResource;
use App\Http\Resources\PicklistResource;
use App\Models\DeliveryReceipt;
use App\Models\DeliveryReceiptItem;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\FulfillmentOrder;
use App\Models\FulfillmentOrderItem;
use App\Models\Invoice;
use App\Models\InvoiceItem;
use App\Models\Item;
use App\Models\ItemQuantity;
use App\Models\PickingInstruction;
use App\Models\PickingSlipEntry;
use App\Models\Picklist;
use App\Models\PicklistItem;
use App\Models\PicklistItemInstruction;
use App\Models\PicklistItemStatus;
use App\Models\PicklistItemWorkflowStatus;
use App\Models\SalesOrder;
use App\Models\SalesOrderItem;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Expr\FuncCall;

class PicklistsController extends Controller
{
    private $reference_number = null;

    public function __construct()
    {
        parent::__construct();

        // $this->status = [("0 - Picking, 1 - Picked, 2 - Marking, 3 - Marked, 4 - Packing, 5 - Packed, 6 - On-Hold")];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query  = Picklist::query();

        $results = PicklistResource::collection($query->get());

        $this->params['results_count'] = $results->count();
        $this->params['results'] = $results;

        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

    public function picklist(Request $request, SalesOrder $sales_order) {
        return DB::table('view_item_picking_instruction')
        ->whereIn('item_id', $sales_order->items()->pluck('id'))
        ->groupby('item_id')
        ->orderby('expiry_date')
        ->get();
        return $sales_order->items()->pluck('id');
    }

    /**
     * Create/Update a Picklist for a given Sales Order
     */
    public function store(Request $request, SalesOrder $sales_order)
    {
        $sales_order_id = $sales_order->id;
        $sales_order_lines = SalesOrderItem::where('sales_order_id', $sales_order_id)->get();

        // $dbQuery = DB::table('view_item_picking_instruction')
        // ->where('warehouse_id', 2)
        // ->where('item_id', 1)
        // ->orderby('expiry_date')
        // ->get();

        // $customer = SalesOrderItem::where('sales_order_id', $sales_order_id)->get();
        // return $this->sendResponse($sales_order_items, 'Resource created succesfully.', false, Response::HTTP_CREATED);

        /**
         * CommentAuthor: KenS
         * CommentDate: 09-01-2021
         * Create Picklist Document from SO Document
         * Create Picklist Line Item from SO Line Item
         * Create Picklist Item Picking Instruction
         */

        $where_so = ['sales_order_id' => $sales_order->id];

        $picklist_header = Picklist::where('sales_order_id', $sales_order->id)->first();

        $this->reference_number = is_null($picklist_header) ? date('Y').'-'.sprintf($this->code_format, (Picklist::max('id')+1)) : $picklist_header->reference_number;

        $picklist_props = [
            'reference_number' => $this->reference_number,
            'sales_order_id' => $sales_order_id,
            'customer_id' => $sales_order->customer_id,
            'quantity_ordered' => '',
            'quantity_allocated' => '',
            'quantity_issued' => '',
            'picked_by' => null,
            'marked_by' => null,
            'packed_by' => null,
            'status' => 'picking',
            'remarks' => $request->input('remarks'),
            'pick_start_date' => null,
            'created_by' => $this->auth_user->id
        ];

        $picklist_header = Picklist::updateOrCreate($where_so, $picklist_props);
        // $picklist_resource = (new PicklistResource($picklist_header));
        // return $picklist_resource;

       /**
        * @CommentAuthor: KenS
        * @CommentDate: 09-13-2021
        * We check for each line item we have for the SO
        */
        foreach ($sales_order_lines as $line) {
            //line: {
            //     "id": 1,
            //     "sales_order_id": 1,
            //     "item_id": 1,
            //     "quantity": 62,
            //     "free_goods": 2,
            //     "unit_cost": "868.00",
            //     "selling_price": "813.65",
            //     "extended_pricing": null,
            //     "gross_amount": "50446.30",
            //     "discount": "0.00",
            //     "vat": null,
            //     "net_amount": "50446.30",
            //     "is_promo": 0,
            //     "created_at": "2021-09-14T03:52:26.000000Z",
            //     "updated_at": "2021-09-14T03:52:26.000000Z"
            // }

            // Item Details
            $item = $line->item;
            $item_id = $item->id;
            $item_name = $item->item_name;
            $item_uom = $item->uom;

            // Order Details
            $line_quantity = $line->quantity;
            $line_free_goods = $line->free_goods;
            $total_quantity_to_pick = $line_quantity + $line_free_goods; // quantity_to_allocate

            $line_item_batches = DB::table('view_item_picking_instruction')
            // ->where('warehouse_id', 2) // [KenS]: temporary disabled since WHSE tagging is not possible on frontend
            ->where('item_id', $item_id)
            ->orderby('expiry_date')
            ->get();

            $frontend = [
                'bin_location' => '',        // VIEW_itembatches.bin_name
                'product_description' => '', // item.item_name
                'unit' => '',                // item.uom
                'batch_number' => '',        // VIEW_itembatches.lot_number
                'expiry_date' => '',        //  VIEW_itembatches.expiry_date
                'quantity' => '',           // line.quantity
                'free_goods' => '',         // line.freegoods
            ];

            // return response()->json(['Item' => $item_name, 'pick_order' => $line_item_batches]);

            // We make sure we find items to allocate for picking
            if (!$line_item_batches && count($line_item_batches) < 1) {
                $availability = 0;
                $inventorystatus = 'NOT AVAILABLE';
            } else {
                $proceed_to_next_line_item = false;

                // We check each batch if we have enough quantity available for picking
                foreach ($line_item_batches as $priority => $value) {
                    /**
                     * TODO:
                     * Check for `batch_number_sold`, `expiry_date`
                     */
                    $priority += 1; // Increment 1 for array starts at index 0

                    $this_batch_available_quantity = $value->quantity_available;

                    if ($this_batch_available_quantity < $total_quantity_to_pick) {
                        // Items not enough:. Allocate all items of that batch
                        $allocated_quantity = $this_batch_available_quantity;
                    } else {
                        // Batch has enough items for allocation
                        $allocated_quantity =  $total_quantity_to_pick;
                        $proceed_to_next_line_item = true;
                    }
            
                    if( $priority > 1){
                        $allocated_quantity =  $line_quantity + $line_free_goods - $this_batch_available_quantity;  
                    }

                    $instruction = PicklistItemInstruction::create([
                        'sales_order_id' => $sales_order_id,
                        'item_id' => $value->item_id,
                        'item_name' => $item_name,
                        'uom' => $item_uom,
                        'bin_location' => $value->bin_name,
                        'lot_number' => $value->lot_number,
                        'expiry_date' => $value->expiry_date,
                        'quantity' => $line_quantity,
                        'free_goods' => $line_free_goods,
                        'available_quantity' => $this_batch_available_quantity,
                        'allocated_quantity' => $allocated_quantity, // Update should have an operation of subtraction
                        'pick_priority' => $priority,
                        'remarks' => ''
                    ]);
 
                    if ($proceed_to_next_line_item) {
                        break;
                    }

                    $total_quantity_to_pick = $value->quantity_available;

                }
            }

            // Create Picklist Item Instruction (INV_Picking_Instruction_Mstr)
            // $picklist_item_instruction_props = [
            //     'sales_order_id' => $sales_order_id,
            //     'item_id' => $line_item->id,
            //     'picking_instruction' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
            //     'priority_1' => 'SAMPLE-BAT000001',
            //     'priority_2' => 'SAMPLE-BAT000002',
            //     'priority_3' => 'SAMPLE-BAT000003',
            //     'comment' => 'lorem ipsum'
            // ];

            // $picklist_item_instruction = PicklistItemInstruction::create($picklist_item_instruction_props);

            // Create Picklist Item Status (INV_PickingSlip_Entry)
            // $pickslist_item_status_props = [
            //     'picklist_item_instruction_id' => $picklist_item_instruction->id,
            //     'sales_order_id' => $sales_order_id,
            //     'item_id' => $line_item->id,
            //     'status' => null
            // ];

            // $pickslist_item_status = PicklistItemStatus::create($pickslist_item_status_props);

             // Create Picklist Item Workflow Status (INV_FO_TransactionWork)
            //  $picklist_item_workflow_status_props = [
            //     'sales_order_id' => $sales_order_id,
            //     'item_id' => $line_item->id,
            //     'activated' => 0,
            //     'picked' => 0,
            //     'marked' => 0,
            //     'packed' => 0,
            //     'valuation_method' => 'LIFO',
            // ];

            // $picklist_item_workflow_status = PicklistItemWorkflowStatus::create($picklist_item_workflow_status_props);

            // Create PicklistItem (INV_FO_Line)
            // $picklist_item_props = [
            //     'picklist_id' => $picklist_header->id,
            //     'sales_order_id' => $sales_order_id,
            //     'item_id' => $line_item->id,
            //     'selling_price' => $line_item->selling_price,
            //     'unit_cost' => $line_item->unit_cost,
            //     'extended_pricing' => $line_item->extended_pricing,
            //     'picklist_item_status_id' => $picklist_item_workflow_status->id,
            //     'picklist_item_workflow_status_id' => $picklist_item_workflow_status->id,
            // ];

            // $picklist_item = PicklistItem::create($picklist_item_props);
        }
        $sales_order->status = 3;
        $sales_order->save();

        $this->params['results_count'] = 1;
        $this->params['results'] = $picklist_header;

        return $this->sendResponse($this->params, 'Resource created succesfully.', false, Response::HTTP_CREATED);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store2(Request $request)
    // {

    //     $fo_picking_instruction = [
    //         'picking_instruction' => $request->post('picking_instruction'),
    //         'priority_1' => $request->post('priority_1'),
    //         'priority_2' => $request->post('priority_2'),
    //         'priority_3' =>  $request->post('priority_3'),
    //         'comment' =>  $request->post('comment'),
    //         'item_id' =>  $request->post('item_id'),
    //     ];

    //     $results = PickingInstruction::create($fo_picking_instruction);

    //     $fo_picking_slip = [
    //         'inv_picking_instruction_id' => $results->id,
    //         'sop_sales_order_id' => $request->post('sop_sales_order_id'),
    //         'picklist_id' => $request->post('picklist_id'),
    //         'item_id' => $results->item_id,
    //         'instruction_id' => $request->post('instruction_id'),
    //         'status' => $request->post('status'),
    //     ];
    //     $results->instruction()->create($fo_picking_slip);

    //     $this->params['results'] = $results;
    //     return $this->sendResponse($this->params, 'Resource created succesfully.', false, Response::HTTP_CREATED);

    // }


    // public function transaction_works(Request $request, $id)
    // {
    //     $fo_instruction = PickingInstruction::find($id);
    //     $fo_slip = PickingSlipEntry::find($fo_instruction->id);

    //     $fo_data = [
    //         'sop_sales_order_id' => $fo_slip->sop_sales_order_id,
    //         'item_id' =>  $fo_instruction->item_id,
    //         'activated' => $request->post('activated'),
    //         'picked' => null,
    //         'marked' => null,
    //         'packed' => null,
    //         'valuation_method' => $request->post('valuation_method'),
    //     ];
    //     $results = FulfillmentOrder::create($fo_data);

    //     $fo_status_data = [
    //         'inv_fulfillment_order_id' => $results->id,
    //         'sop_sales_order_id' => $results->sop_sales_order_id,
    //         'inv_picking_slip_entry_id' => $fo_slip->id,
    //         'item_id' => $results->item_id,
    //         'selling_price' => $request->post('selling_price'),
    //         'unit_cost' => $request->post('unit_cost'),
    //         'extended_pricing' => $request->post('extended_pricing'),
    //     ];
    //     $results->fulfillment_order()->create($fo_status_data);

    //     $this->params['results'] = $results;
    //     return $this->sendResponse($this->params, 'Resource created succesfully.');

    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, SalesOrder $sales_order)
    {
        $data =  PicklistResource::collection(Picklist::all())
        ->where('sales_order_id', $sales_order->id);

        $results = $data->flatten();

        $this->params['results_count'] = 1;
        $this->params['results'] = $results;
        return $this->sendResponse($this->params, 'Resource retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SalesOrder $sales_order)
    {
        // $status = $request->input('status'); // [("0 - Picking, 1 - Picked, 2 - Marking, 3 - Marked, 4 - Packing, 5 - Packed, 6 - On-Hold")]
        // $picklist = $sales_order->picklist;
        // abort_if(!$picklist, 404);

        // $picklist->update([
        //     'status' => intval($status)
        // ]);

        /**
         * CommentAuthor: KenS
         * CommentDate: 09/21/2021
         * Description:  TODO: When picklist is updated we make sure the Sales Order status is also updated
         */

        // return $picklist;

        /**
         * CommentAuthor: Raymart
         * CommentDate: 09/22/2021
         * Description:  comment
         */

        $status = $request->input('status');

        if($status == "picked" && $sales_order->picklist->status == "picking"){
            $sales_order->picklist->status = $status;
            $sales_order->picklist->pick_start_date = $request->input('pick_start_date', now());
            $sales_order->picklist->picked_by = Auth::user()->id;

            $new_status = [
                'status'     => 1,
                'remarks'    => 'picked',
                'user_id'    => Auth::user()->id
            ];
            $sales_order->statuses()->create($new_status);

        }elseif($status == "marking" && $sales_order->picklist->status == "picked"){
            $sales_order->picklist->status = $status;
        }elseif($status == "marked"  && $sales_order->picklist->status == "marking"){
            $sales_order->picklist->status = $status;
            $sales_order->picklist->marked_by = Auth::user()->id;
        }elseif($status == "return_to_picker" && $sales_order->picklist->status == "marking"){
            $validated = $request->validate([
                'returned_reason' => 'required',
            ]);
            $sales_order->picklist->status = "picking";
            $sales_order->picklist->returned_reason = $request->returned_reason;
        }elseif($status == "packing"  && $sales_order->picklist->status == "marked"){
            $sales_order->picklist->status = $status;
        }elseif($status == "packed"  && $sales_order->picklist->status == "packing"){
            $sales_order->picklist->status = $status;
            $sales_order->picklist->marked_by = Auth::user()->id;
        }elseif($status == "return_to_marker" && $sales_order->picklist->status == "packing"){
            $validated = $request->validate([
                'returned_reason' => 'required',
            ]);
            $sales_order->picklist->status = "marking";
            $sales_order->picklist->return_reason = $request->return_reason;
        }elseif($status == "on-hold"){
            $sales_order->picklist->status = $status;
        }elseif($sales_order->picklist->status == "on-hold"){
            $sales_order->picklist->status = $status;
        }
        else{
            return $this->sendResponse([], 'Resource status is '.$sales_order->picklist->status);
        }
         // if status == "over-ride"

        $sales_order->picklist->save();
        $picklist = $sales_order->picklist;
        $this->params['results_count'] = 1;         
        $this->params['results'] = $picklist;
        return $this->sendResponse($this->params, 'Resource successfully updated.');
    }

    public function delivery_receipt(Request $request, SalesOrder $sales_order){

        $dr_number = sprintf($this->code_format,  (DeliveryReceipt::max('id')+1));

        $delivery_receipt = [
            'reference_number'         => $sales_order->picklist->reference_number,
            'sales_order_id'           => $sales_order->picklist->sales_order_id,
            'picklist_id'              => $sales_order->picklist->id,
            'customer_id'              => $sales_order->picklist->customer_id,
            'customer_address'         => $sales_order->shipping_address,
            'terms'                    => $sales_order->customer->payment_mode,
            'delivery_receipt_number'  => $request->input('delivery_receipt_number', $dr_number),
            'delivery_receipt_date'    => $request->input('delivery_receipt_date', now()), 
            'picked_by'                => $sales_order->picklist->picked_by,
            'marked_by'                => $sales_order->picklist->marked_by,
            'packed_by'                => $sales_order->picklist->packed_by,
        ];
        
        $data = DeliveryReceipt::create($delivery_receipt);

        $item_instructions = $sales_order->picklist->item_instructions;

        //check if delivery receipt item has free goods
        //duplicate
        
        for($i = 0; $i < $item_instructions->count(); $i++)
        {
            $item = Item::where('id',$sales_order->picklist->item_instructions[$i]->item_id)->first();

           $delivery_receipt_item = [
               'delivery_receipt_id'    => $data->id,
               'sales_order_id'         => $sales_order->picklist->item_instructions[$i]->sales_order_id,
               'item_id'                => $sales_order->picklist->item_instructions[$i]->item_id,
               'uom'                    => $sales_order->picklist->item_instructions[$i]->uom,
               'lot_number'             => $sales_order->picklist->item_instructions[$i]->lot_number,
               'expiry_date'            => $sales_order->picklist->item_instructions[$i]->expiry_date,
               'quantity'               => $sales_order->picklist->item_instructions[$i]->quantity,
               'free_goods'             => $sales_order->picklist->item_instructions[$i]->free_goods,
               'allocated_quantity'     => $sales_order->picklist->item_instructions[$i]->allocated_quantity,
               'unit_cost'              => $item->selling_price,
               'amount'                 => $sales_order->picklist->item_instructions[$i]->allocated_quantity * $item->selling_price,
           ];

           $result = DeliveryReceiptItem::create($delivery_receipt_item);

           $check_data = DeliveryReceiptItem::where('item_id',$result->item_id)->where('quantity',$result->free_goods)->where('free_goods',0)->first();

               // check if delivery receipt item has free goods
               if($sales_order->picklist->item_instructions[$i]->free_goods != 0 && $check_data === null){
                   
                $delivery_receipt_item = [
                    'delivery_receipt_id'    => $data->id,
                    'sales_order_id'         => $result->sales_order_id,
                    'item_id'                => $result->item_id,
                    'bin_location'           => $result->bin_location,
                    'item_name'              => $result->item_name,
                    'uom'                    => $result->uom,
                    'lot_number'             => $result->lot_number,
                    'expiry_date'            => $result->expiry_date, 
                    'quantity'               => $result->free_goods,
                    'unit_cost'              => 0,
                    'free_goods'             => 0,
                    'allocated_quantity'     => $result->free_goods,
                    'amount'                 => 0
                ];
     
               $free_good =  DeliveryReceiptItem::create($delivery_receipt_item);

               // Minus the free goods to the first array before the generation of the free goods row
               $result->allocated_quantity -= $free_good->quantity;
               $result->amount = $result->allocated_quantity * $item->selling_price;
               $result->save();
           }
        }

        $this->params['results_count'] = 1;   
        $this->params['results'] = $data->load('delivery_receipt_items');
        return $this->sendResponse($this->params, 'Resource successfully updated.');
    }
    
    public function get_delivery_receipt($sales_order , $delivery_receipt_id)
    {
        $data =  DeliveryReceiptResource::collection(DeliveryReceipt::all())
        ->where('sales_order_id',$sales_order)
        ->where('id',$delivery_receipt_id);
        $result = $data->flatten();

        $this->params['results'] = $result;
        return $this->sendResponse($this->params, 'Resource succesresfully retrieved.');
    }

    public function put_delivery_receipt(Request $request, $sales_order , $delivery_receipt_id){

        $result = DeliveryReceipt::where('id',$delivery_receipt_id)->where('sales_order_id', $sales_order)->first();

        $result->delivery_receipt_number = $request->input('delivery_receipt_number');
        $result->delivery_receipt_date = $request->input('delivery_receipt_date');

        $result->save();
        $this->params['results'] = $result->load('delivery_receipt_items');
        return $this->sendResponse($this->params, 'Resource successfully updated.');
    }
}
