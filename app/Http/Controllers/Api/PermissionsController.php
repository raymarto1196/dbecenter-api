<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Spatie\Permission\Models\Permission;

/**
 *
 * @OA\Schema(

 * @OA\Xml(name="Permissions"),
 * @OA\Property(property="id", type="integer",  example="1"),
 * @OA\Property(property="name", type="string",  example="ADMINISTRATOR"),
 * @OA\Property(property="guard_name", type="string", example="web"),
 * @OA\Property(property="created_at", type="string", format="date-time", example="2019-02-25T12:59:20Z"),
 * @OA\Property(property="updated_at", type="string",  format="date-time", example="2019-02-25T12:59:20Z")
 * )
 *
 * Class role
 *
 */


class PermissionsController extends Controller
{
     /**
     * @OA\Get(
     * path="/api/v1/permissions",
     * tags={"Permission"},
     * operationId="rolesIndex",
     * description="Get List of roles",
     * security={
     *       {"api_key": {}}
     *     },
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="List of roles successfully retrieved"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/PermissionsController"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     *
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Display a listing of the roles.
     *
     * @return \Illuminate\Http\Response
     */    
    public function index(Request $request)
    {
        $this->params['results_count'] = Permission::count();
        $this->params['results'] = Permission::all();
        return $this->sendResponse($this->params, 'Permission successfully retrieved.');

        return response()->json($this->params, Response::HTTP_OK);
    }
     /**
     * @OA\Post(
     * path="/api/v1/permissions",
     * tags={"Permission"},
     * operationId="PermissionsStore",
     * description="Create Permissions",
     * security={
     *       {"api_key": {}}
     *     },
     *  @OA\RequestBody(
     *    required=true,
     *    description="Create Permissions",
     *    @OA\JsonContent(
     *       required={"name","guard_name"},
     *        @OA\Property(property="name", type="string",  example="ADMINISTRATOR"),
     *        @OA\Property(property="guard_name", type="string",  example="web"),
     *   
     *      
     *    ),
     * ),
     * @OA\Response(
     *    response=400,
     *    description="Bad Request",
     *    @OA\JsonContent(
     *            @OA\Property(property="success", type="boolean", example=false),
     *       @OA\Property(property="message", type="string", example="Validation Error"),
     *    )
     *  ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     *
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Create New Permissions.
     *
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:permissions,name'
        ]);

        $permission = $request->post('name');
        $this->params['result'] = Permission::create(['name' => $permission]);
        return $this->sendResponse($this->params, 'Permission successfully created.', false, Response::HTTP_CREATED);
    }
   /**
     * @OA\PUT(
     * path="/api/v1/permissions/{id}",
     * tags={"Permission"},
     * operationId="PerimissionsUpdate",
     * description="Update a Perimission by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="Perimission id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *  @OA\RequestBody(
     *    required=true,
     *    description="Update a Perimission by ID",
     *    @OA\JsonContent(
     *       required={"name","guard_name"},
     *       @OA\Property(property="name", type="integer",  example="ADMINISTRATOR"),
     *       @OA\Property(property="guard_name", type="string",  example="web"),
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="User successfully updated"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/PermissionsController"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=400,
     *    description="Bad Request",
     *    @OA\JsonContent(
     *            @OA\Property(property="success", type="boolean", example=false),
     *       @OA\Property(property="message", type="string", example="Validation Error"),
     *    )
     *  ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description=" permissions Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example=" permissions Not Found"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Update a permissions by ID
     *
     * @return \Illuminate\Http\Response
     */


    public function update(Request $request, Permission $permission)
    {
   
        $request->validate([
            'name' => 'required|unique:permissions,name,'.$permission->id. 'id'
        ]);

        $name = $request->post('name');
        $permission->update(['name' => $name]);
        $this->params['result'] = $permission;
        return $this->sendResponse($this->params, 'Permission successfully updated.', false, Response::HTTP_OK);
    }
}
