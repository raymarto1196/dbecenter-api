<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DepartmentsController extends Controller
{
      /**
     * @OA\Get(
     * path="/api/v1/departments",
     * tags={"Department"},
     * operationId="departmentsIndex",
     * description="Get List of Departments",
     * security={
     *       {"api_key": {}}
     *     },
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="List of Departments successfully retrieved"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/Department"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     *
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Display a listing of the Departments.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // TODO: Add searchbycolumns, orderbycolumns 
        $this->params['results_count'] = Department::count();
        $this->params['results'] = Department::all();

        return $this->sendResponse($this->params, 'Departments successfully retrieved.');
    }

       /**
     * @OA\Post(
     * path="/api/v1/departments",
     * tags={"Department"},
     * operationId="departmentsStore",
     * description="Create department",
     * security={
     *       {"api_key": {}}
     *     },
     *  @OA\RequestBody(
     *    required=true,
     *    description="Create department",
     *    @OA\JsonContent(
     *       required={"name"},
     *       @OA\Property(property="name", type="string",  example="Jose"),
     *    ),
     * ),
     * @OA\Response(
     *    response=400,
     *    description="Bad Request",
     *    @OA\JsonContent(
     *            @OA\Property(property="success", type="boolean", example=false),
     *       @OA\Property(property="message", type="string", example="Validation Error"),
     *    )
     *  ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     *
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Create New User Role.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Department $department)
    {
        $validated = $request->validate([
            'name' => 'required|unique:departments,name',
        ]);

        // Update department
        $department->create($validated);

        $this->params['department'] = $department;
        return $this->sendResponse($this->params, 'Department successfully created.', false, Response::HTTP_CREATED);
    }

      /**
     * @OA\Get(
     * path="/api/v1/departments/{id}",
     * tags={"Department"},
     * operationId="departmentsShow",
     * description="Get a departments by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="departments id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="department successfully retrieved"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/Department"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description="Department Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example="department Not Found"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Get department By ID.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $results = Department::find($id);

        $this->params['results'] = $results;
        return $this->sendResponse($this->params, 'Department successfully retrieved.');
    }

     /**
     * @OA\Put(
     * path="/api/v1/departments/{id}",
     * tags={"Department"},
     * operationId="departmentUpdate",
     * description="Update a departments by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="departments id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *  @OA\RequestBody(
     *    required=true,
     *    description="Update a departments by ID",
     *    @OA\JsonContent(
     *       required={"name"},
     *       @OA\Property(property="name", type="string",  example="Jose Dela Cruz"),
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="department successfully updated"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/Department"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=400,
     *    description="Bad Request",
     *    @OA\JsonContent(
     *            @OA\Property(property="success", type="boolean", example=false),
     *       @OA\Property(property="message", type="string", example="Validation Error"),
     *    )
     *  ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description="Department Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example="Department Not Found"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Update a department by ID
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department)
    {
        $validated = $request->validate([
            'name' => 'required|unique:departments,name,'.$department->id.'id',
        ]);

        $department->update($validated);

        $this->params['results_count'] = 1;
        $this->params['result'] = $department;
        return $this->sendResponse($this->params, 'Department successfully updated.');
    }

      /**
     * @OA\Delete(
     * path="/api/v1/departments/{id}",
     * tags={"Department"},
     * operationId="departmentDelete",
     * description="Delete a Department by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="Department",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="Department successfully deleted"),
     *        )
     *     ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description="Department Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example="Department Not Found"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Delete a department by ID
     *
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $department = Department::find($id);
        if (!$department) return response()->json(['error'=>true, 'message'=>'Requested department does not exist'], Response::HTTP_NOT_FOUND);

        // Delete the selected department: Softdelete
        $department->delete();
        return $this->sendResponse([], 'Department successfully deleted.', false);
    }
}
