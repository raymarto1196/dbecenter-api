<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Item;
use App\Models\User;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, CartItem $cart_item)
    {
        $user = ['user_id'=> $this->auth_user->id];
        $cart = Cart::updateOrCreate($user, $user);
        $item = Item::findOrFail($request->input('item_id'));

        $data = [
            'cart_id' => $cart->id,
            'item_id' => $item->id,
            'item_user_id' => $item->user_id,
            'quantity' => $request->input('quantity'),   
            'price' => $request->input('quantity') * $item->unit_cost,
        ];

        $result = $cart_item->create($data);

        $this->params['message'] = 'Resource created successfully';
        $this->params['results_count'] = 1;
        $this->params['results'] = $result;

        return $this->sendResponse($this->params);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = Cart::findOrFail($id);
        $result->load('cart_items.item:id,item_image,item_name,unit_cost');

        $this->params['message'] = 'Resource retrieved successfully';
        $this->params['results_count'] = 1;
        $this->params['results'] = $result;

        return $this->sendResponse($this->params);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cart_item = CartItem::findOrFail($id);
        $data = [
            'quantity' => $request->input('quantity')
        ];

        $result = $cart_item->update($data);

        $this->params['message'] = 'Resource updated successfully';
        $this->params['results_count'] = 1;
        $this->params['results'] = $result;

        return $this->sendResponse($this->params);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = CartItem::findOrFail($id);
        $result->delete();

        $this->params['message'] = 'Resource deleted successfully';
        $this->params['results_count'] = 1;
        $this->params['results'] = true;
        return $this->sendResponse($this->params);

    }

    public function get_user_carts(){
        $user = User::findOrFail($this->auth_user->id);
        $user->load('cart.cart_items.item:id,item_image,item_name,identifier,unit_cost,unit_cost');

        $this->params['message'] = 'Resource retrieved successfully';
        $this->params['results_count'] = 1;
        $this->params['results'] = $user;
        return $this->sendResponse($this->params);
    }
}
