<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CustomerCourierRequirmentResource;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Customer;
use App\Models\MaintenanceCodeSetup;
use App\Models\Item;
use Carbon\Carbon;
use App\Models\Courier;
use App\Models\CustomerCourierRequirement;
use App\Models\CustomerItemDiscount;
use App\Models\Shipping;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class CustomersController extends Controller
{
    protected $documents = ['po_copy','signed_bcaf_tta','registration_certificate','lto','business_permit','mayors_permit','establishment_photo','itr_fs','other_documents','signed_bcaf_tta2','prc_id','government_valid_id','other_documents2'];
    protected $allowedfileExtension = ['pdf','jpg','png','docx'];

    /*
     * Display a listing of the Customers.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index(Request $request, Customer $customer)
    // {
    // 	if ($request->has('count')) {
    // 		$count = $request->get('count');
    // 	}else{
    // 		$count = 10;
    // 	}

    //     $this->params['results_count'] = $customer->count();
    //     $this->params['results'] = $customer->paginate($count)->items();

    //     return $this->sendResponse($this->params, 'Customers successfully retrieved.');
    // }

     /**
     * @OA\Get(
     * path="/api/v1/customers",
     * tags={"Customer Management"},
     * operationId="courierIndex",
     * description="Get List of courier",
     * security={
     *       {"api_key": {}}
     *     },
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="List of courier successfully retrieved"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/Courier"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     *
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Display a listing of the Courier.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //do not delete
        // if (! $this->auth_user->can('list_customer')) {
        //
        //     return response()->json([
        //         'responseMessage' => 'You do not have the required authorization.',
        //         'responseStatus'  => 403,
        //     ], Response::HTTP_NOT_FOUND
        //
        //   );
        // $query  = Customer::query();

        // $results = $query->get();

        // $this->params['results_count'] = $results->count();
        // $this->params['results'] = $results;

        // return $this->sendResponse($this->params, 'Resource retrieved successfully');

        $results = Customer::where([
        [function ($query) use ($request){
            if (($search_string = $request->search_string)) {

                $search_string = '%' . $search_string . '%';
                $query->orWhere('customer_code', 'LIKE', $search_string)->get();
                $query->orWhere('registered_name', 'LIKE', $search_string)->get();
                $query->orWhere('shipping_address', 'LIKE', $search_string)->get();
                $query->orWhere('tin', 'LIKE', $search_string)->get();
                $query->orWhere('contact_point', 'LIKE', $search_string)->get();
                $query->orWhere('contact_num', 'LIKE', $search_string)->get();
                $query->orWhere('terms', 'LIKE', $search_string)->get();
                $query->orWhere('customer_channel', 'LIKE', $search_string)->get();
                $query->orWhere('distribution_channel', 'LIKE', $search_string)->get();
                $query->orWhere('key_account_manager', 'LIKE', $search_string)->get();
                $query->orWhere('sales_account_representative', 'LIKE', $search_string)->get();
                $query->orWhere('ewt', 'LIKE', $search_string)->get();
                $query->orWhere('status', 'LIKE', $search_string)->get();

            }

            if(($start_date = $request->start_date) && ($end_date = $request->end_date)){

                $start_date = Carbon::createFromFormat('m-d-Y H:i:s', $start_date)->format('Y-m-d H:i:s');
                $end_date = Carbon::createFromFormat('m-d-Y H:i:s', $end_date)->format('Y-m-d H:i:s');
                $query->whereBetween('created_at', [$start_date, $end_date])->get();
            }
         }]
         ])->with('courier:id,company_name','shipping_address','billing_address','product_discounts')->get();
        // ])->paginate(
        //     $request->pageLimit
        // );

        $this->params['results_count'] =$results->count();
        $this->params['results'] =$results;
        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

    /**
     * @OA\Post(
     * path="/api/v1/customers",
     * tags={"Customer Management"},
     * operationId="CustomerStore",
     * description="Create Customer",
     * security={
     *       {"api_key": {}}
     *     },
     * @OA\RequestBody(
     * required=true,
     *  description="Create Customer",
     *  @OA\JsonContent(
     *  required={"registered_name","business_trade_name","business_type","vat_type","sales_invoice_delivery_receipt","tin","owner","email_address","contact_num",
     *            "customer_type","customer_channel","billing_address","lba_code","lba_name","shipping_address","lba_code2","lba_name2","credit_limit_allowed",
     *              "terms","distribution_channel","key_account_manager","sales_account_represintative","ewt","sernior_citizen_discount","contact_point"},
     * @OA\Property(property="registered_name", type="integer",  example="Customer 1"),
     * @OA\Property(property="business_trade_name", type="string",  example="Customer Pharmacy"),
     * @OA\Property(property="business_type", type="string", example="Corporation"),
     * @OA\Property(property="vat_type", type="string", example="Vat exempt"),
     * @OA\Property(property="sales_invoice_delivery_receipt", type="string", example="required"),
     * @OA\Property(property="tin", type="string", example="123-456-789-uuu"),
     * @OA\Property(property="owner", type="string", example="john"),
     * @OA\Property(property="email_address", type="string", example="john@gmail.com"),
     * @OA\Property(property="contact_num", type="string", example="09777037164"),
     * @OA\Property(property="customer_type", type="string", format="email", example="drugstore"),
     * @OA\Property(property="customer_channel", type="string", example="jedi"),
     * @OA\Property(property="billing_address", type="string", example="Unit 522,UT2,Galicia st, Brgy 437 Sampaloc,metro"),
     * @OA\Property(property="lba_code", type="string", example="LBA CODE"),
     * @OA\Property(property="lba_name", type="string", example="LBA NAME"),
     * @OA\Property(property="shipping_address", type="string", example="1767,Sobreidad St, Brgy 445 Sampaloc,metro"),
     * @OA\Property(property="lba_code2", type="string", example="LBA CODE"),
     * @OA\Property(property="lba_name2", type="string", example="LBA NAME"),
     * @OA\Property(property="credit_limit_allowed", type="string", example="500000"),
     * @OA\Property(property="terms", type="string", example="90 days"),
     * @OA\Property(property="distribution_channel", type="string", example="2go"),
     * @OA\Property(property="key_account_manager", type="string", example="PAU"),
     * @OA\Property(property="sales_account_represintative", type="string", example="LIN"),
     * @OA\Property(property="ewt", type="string", example="1"),
     * @OA\Property(property="sernior_citizen_discount", type="string", example="1"),
     * @OA\Property(property="contact_point", type="object",
     *                       type="array",
     *                       @OA\Items(
     *                          @OA\Property(property="name", type="integer",example="zes"),
     *                          @OA\Property(property="email", type="integer",example="zes@gmail.com"),
     *                          @OA\Property(property="contact_num", type="string",example="09778372047"),
     *                     ) ),
     *    ),
     * ),
     * @OA\Response(
     *    response=400,
     *    description="Bad Request",
     *    @OA\JsonContent(
     *            @OA\Property(property="success", type="boolean", example=false),
     *       @OA\Property(property="message", type="string", example="Validation Error"),
     *    )
     *  ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     *
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Create New Customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //do not delete
        // if (! $this->auth_user->can('create_customer')) {
        //
        //     return response()->json([
        //         'responseMessage' => 'You do not have the required authorization.',
        //         'responseStatus'  => 403,
        //     ], Response::HTTP_NOT_FOUND
        //
        //   );
        // $query  = Customer::query();
        if(($delivery_schedule_date = $request->delivery_schedule_date) ){
            $delivery_schedule_date = Carbon::createFromFormat('m-d-Y', $delivery_schedule_date)->format('Y-m-d');
        }

        $validated = $request->validate([
            'registered_name' => 'required',
            'business_type' => 'required',
            'tin' => 'required',
            'owner' => 'required',
            'email_address' => 'required|email',
            'contact_number' => 'required',
            'customer_type' => 'required',
            'customer_channel' => 'required',
            'contact_point' => 'array',
            'credit_limit_allowed' => '',
            'terms' => '',
            'distribution_channel' => '',
            'key_account_manager' => '',
            'sales_account_representative' => '',
            'ewt' => '',
            'sernior_citizen_discount' => '',
            'business_trade_name' => '',
            'vat_type' => 'required',
            'sales_invoice_delivery_receipt' => '',
            'accounting_contact_person' => '',
            'accounting_contact_number' => '',
            'accounting_contact_email' => '',
            'accounting_contact_designation' => '',
            'purchasing_contact_person' => '',
            'purchasing_contact_number' => '',
            'purchasing_contact_email' => '',
            'purchasing_contact_designation' => '',
            'pharmacy_contact_person' => '',
            'pharmacy_contact_number' => '',
            'pharmacy_contact_email' => '',
            'pharmacy_contact_designation' => '',
            'authorized_purchaser' => '',
            'authorized_purchaser_designation' => '',
            'authorized_receiver' => '',
            'authorized_receiver_designation' => '',
            'secretary_contact_person' => '',
            'secretary_contact_number' => '',
            'delivery_schedule_time' => '',
            'payment_mode' => '',
            'payment_terms' => '',
            'po_based' => '',
            'primary_movers' => 'array',
            'charges' => '',
            'return_policy' => '',
            'credit_limit' => '',
            'tagged_owned' => '',
            'signed_bcaf_tta_commitment_date' => '',
            'registration_certificate_commitment_date' => '',
            'lto_commitment_date' => '',
            'business_permit_commitment_date' => '',
            'mayors_permit_commitment_date' => '',
            'establishment_photo_commitment_date' => '',
            'itr_fs_commitment_date' => '',
            'other_documents_commitment_date' => '',
            'signed_bcaf_tta2_commitment_date' => '',
            'prc_id_commitment_date' => '',
            'government_valid_id_commitment_date' => '',
            'other_documents2_commitment_date' => '',

        ]);

        $customer = Customer::create($validated);
        $codesetup = MaintenanceCodeSetup::find(5);
        $customer->customer_code = $codesetup->code_prefix.sprintf('%06d', $customer->id);
        $customer->delivery_schedule_date = $delivery_schedule_date;

        $foldername = "customers/customer-{$customer->id}/";

        foreach ($this->documents as $document) {

            /**
             * Store file uploaded from file object
             */
            // if ($file = $request->file($document)) {
            //     $extension = $file->getClientOriginalExtension();
            //     $filename = $document.'.'.$extension;
            //     $customer->$document = uploadFileToPath($foldername, $file, $filename);
            // }

            /**
             * Store file uploaded from Base64
             */
            if($request->has($document) && $request->input($document)){
                $base64_image = $request->input($document);

                // Currently accepts image only
                if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                    $data = substr($base64_image, strpos($base64_image, ',') + 1);
                    $extension = getFileExtension($base64_image);
                    $file = base64_decode($data);
                    $filename = $document.'.'.$extension;

                    if(Storage::put($foldername. $filename, $file)){
                        $image_path = $foldername.$filename;
                        $customer->$document = $image_path;

                        $res = $document.'_upload_date';
                        $customer->$res = Carbon::now();

                    }
                }
            }
        }

        if ($request->has('shipping_address') && ($request->input('shipping_address') != null) && is_array($request->input('shipping_address'))) {
    		$shipping_address = $request->input('shipping_address');

            foreach ($shipping_address as $key => $shipping) {

                $shipping_data = [
                    'lba_name' => $shipping['lba_name'],
                    'lba_code' => $shipping['lba_code'],
                    'street' => $shipping['street'],
                    'barangay' => $shipping['barangay'],
                    'city' => $shipping['city'],
                    'province' => $shipping['province'],
                    'zip' => $shipping['zip'],
                    'country' => $shipping['country'],
                    'contact_number' => $customer->contact_number,
                ];

               $customer->shipping_address()->create($shipping_data);

            }
        }

        if ($request->has('billing_address') && ($request->input('billing_address') != null)) {
    		$billing_address = $request->input('billing_address');

            foreach ($billing_address as $key => $cm_address) {
            $address_data = [
                'lba_name' => $cm_address['lba_name'],
                'lba_code' => $cm_address['lba_code'],
                'street' => $cm_address['street'],
                'barangay' => $cm_address['barangay'],
                'city' => $cm_address['city'],
                'province' => $cm_address['province'],
                'zip' => $cm_address['zip'],
                'country' => $cm_address['country'],
                'contact_number' => $customer->contact_number 
            ];

            $customer->billing_address()->create($address_data);

             }
        }

        if ($request->has('product_discounts') && $request->input('product_discounts') && is_array($request->input('product_discounts'))) {
            $product_discounts = $request->input('product_discounts');

            foreach($product_discounts as $key => $item) {

                $discount_data = [
                'customer_id' => $customer->id,
                'discount' => $item['discount'],
                'srp' => $item['srp'],
                'rebates' => $item['rebates'],
                'deals' => $item['deals'],
                ];
                $customer->product_discounts()->create($discount_data);
            }

        }
        $customer->save();
        $customer->load('courier','shipping_address','billing_address','product_discounts');

        $this->params['results_count'] = 1;
        $this->params['results'] = $customer;

        return $this->sendResponse($this->params, 'Resource created successfully.', false, Response::HTTP_CREATED);
    }

     /**
     * @OA\Get(
     * path="/api/v1/customers/{id}",
     * tags={"Customer Management"},
     * operationId="CustomerShow",
     * description="Get a Customer by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="Customer id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="Customer successfully retrieved"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/Customer"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description="Customer Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example="Customer Not Found"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Get Customer By ID.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //do not delete
        // if (! $this->auth_user->can('read_customer')) {
        //
        //     return response()->json([
        //         'responseMessage' => 'You do not have the required authorization.',
        //         'responseStatus'  => 403,
        //     ], Response::HTTP_NOT_FOUND
        //
        //   );
        // $query  = Customer::query();

        $customer = Customer::find($id);

        // $customer->load('product_discounts:customer_item_discounts.item_name');
        $customer->load('product_discounts','csm_bank_accounts','shipping_address','billing_address');

        $this->params['results_count'] = 1;
        $this->params['results'] = $customer;

        return $this->sendResponse($this->params, 'Resource retrieved successfully.');
    }


     /**
     * @OA\PUT(
     * path="/api/v1/customers/{id}",
     * tags={"Customer Management"},
     * operationId="customersUpdate",
     * description="Update a customers by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="customers id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     * @OA\RequestBody(
     *    required=true,
     *    description="Update a customers by ID",
     *    @OA\JsonContent(
     *    required={"registered_name","business_trade_name","business_type","vat_type","sales_invoice_delivery_receipt","tin","owner","email_address","contact_num",
     *            "customer_type","customer_channel","billing_address","lba_code","lba_name","shipping_address","lba_code2","lba_name2","credit_limit_allowed",
     *              "terms","distribution_channel","key_account_manager","sales_account_represintative","ewt","sernior_citizen_discount","contact_point"},
     * @OA\Property(property="registered_name", type="integer",  example="Customer 2"),
     * @OA\Property(property="business_trade_name", type="string",  example="Customer Pharmacy"),
     * @OA\Property(property="business_type", type="string", example="Corporation"),
     * @OA\Property(property="vat_type", type="string", example="Vat exempt"),
     * @OA\Property(property="sales_invoice_delivery_receipt", type="string", example="required"),
     * @OA\Property(property="tin", type="string", example="123-456-789-uuu"),
     * @OA\Property(property="owner", type="string", example="john"),
     * @OA\Property(property="email_address", type="string", example="john@gmail.com"),
     * @OA\Property(property="contact_num", type="string", example="09777037164"),
     * @OA\Property(property="customer_type", type="string", format="email", example="drugstore"),
     * @OA\Property(property="customer_channel", type="string", example="jedi"),
     * @OA\Property(property="billing_address", type="string", example="Unit 522,UT2,Galicia st, Brgy 437 Sampaloc,metro"),
     * @OA\Property(property="lba_code", type="string", example="LBA CODE"),
     * @OA\Property(property="lba_name", type="string", example="LBA NAME"),
     * @OA\Property(property="shipping_address", type="string", example="1767,Sobreidad St, Brgy 445 Sampaloc,metro"),
     * @OA\Property(property="lba_code2", type="string", example="LBA CODE"),
     * @OA\Property(property="lba_name2", type="string", example="LBA NAME"),
     * @OA\Property(property="credit_limit_allowed", type="string", example="500000"),
     * @OA\Property(property="terms", type="string", example="90 days"),
     * @OA\Property(property="distribution_channel", type="string", example="2go"),
     * @OA\Property(property="key_account_manager", type="string", example="PAU"),
     * @OA\Property(property="sales_account_represintative", type="string", example="LIN"),
     * @OA\Property(property="ewt", type="string", example="1"),
     * @OA\Property(property="sernior_citizen_discount", type="string", example="1"),
     * @OA\Property(property="contact_point", type="object",
     *                       type="array",
     *                       @OA\Items(
     *                          @OA\Property(property="name", type="integer",example="zess"),
     *                          @OA\Property(property="email", type="integer",example="zes@gmail.com"),
     *                          @OA\Property(property="contact_num", type="string",example="09778372047"),
     *                     ) ),
     *
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="customer successfully updated"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/Customer"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=400,
     *    description="Bad Request",
     *    @OA\JsonContent(
     *            @OA\Property(property="success", type="boolean", example=false),
     *       @OA\Property(property="message", type="string", example="Validation Error"),
     *    )
     *  ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description="customer Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example="customer Found"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Update a customer by ID
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {

        //do not delete
        // if (! $this->auth_user->can('update_customer')) {
        //
        //     return response()->json([
        //         'responseMessage' => 'You do not have the required authorization.',
        //         'responseStatus'  => 403,
        //     ], Response::HTTP_NOT_FOUND
        //
        //   );
        // $query  = Customer::query();
        if(($delivery_schedule_date = $request->delivery_schedule_date) ){
            $delivery_schedule_date = Carbon::createFromFormat('m-d-Y', $delivery_schedule_date)->format('Y-m-d');
        }

        $foldername = "customers/customer-{$customer->id}/";

        $validated = $request->validate([
            'registered_name' => 'required',
            'business_type' => 'required',
            'vat_type' => 'required',
            'tin' => 'required',
            'owner' => 'required',
            'email_address' => 'required|email',
            'contact_number' => 'required',
            'customer_type' => 'required',
            'customer_channel' => '',
            'contact_point' => 'array',
            'terms' => '',
            'distribution_channel' => '',
            'key_account_manager' => '',
            'sales_account_representative' => '',
            'ewt' => '',
            'senior_citizen_discount' => '',
            'business_trade_name' => '',
            'sales_invoice_delivery_receipt' => '',
            'accounting_contact_person' => '',
            'accounting_contact_number' => '',
            'accounting_contact_email' => '',
            'accounting_contact_designation' => '',
            'purchasing_contact_person' => '',
            'purchasing_contact_number' => '',
            'purchasing_contact_email' => '',
            'purchasing_contact_designation' => '',
            'pharmacy_contact_person' => '',
            'pharmacy_contact_number' => '',
            'pharmacy_contact_email' => '',
            'pharmacy_contact_designation' => '',
            'authorized_purchaser' => '',
            'authorized_purchaser_designation' => '',
            'authorized_receiver' => '',
            'authorized_receiver_designation' => '',
            'secretary_contact_person' => '',
            'secretary_contact_number' => '',
            'delivery_schedule_time' => '',
            'payment_mode' => '',
            'payment_terms' => '',
            'po_based' => '',
            'primary_movers' => 'array',
            'charges' => '',
            'return_policy' => '',
            'credit_limit' => '',
            'tagged_owned' => '',
            'signed_bcaf_tta_commitment_date' => '',
            'registration_certificate_commitment_date' => '',
            'lto_commitment_date' => '',
            'business_permit_commitment_date' => '',
            'mayors_permit_commitment_date' => '',
            'establishment_photo_commitment_date' => '',
            'itr_fs_commitment_date' => '',
            'other_documents_commitment_date' => '',
            'signed_bcaf_tta2_commitment_date' => '',
            'prc_id_commitment_date' => '',
            'government_valid_id_commitment_date' => '',
            'other_documents2_commitment_date' => '',
        ]);

        foreach ($this->documents as $document) {
            /**
             * Store file uploaded from file object
             */
            // if ($file = $request->file($document)) {
            //     $extension = $file->getClientOriginalExtension();
            //     $filename = $document.'.'.$extension;
            //     $customer->$document = uploadFileToPath($foldername, $file, $filename);
            // }

            /**
             * Store file uploaded from Base64
             */
            if($request->has($document) && $request->input($document)){
                $base64_image = $request->input($document);

                // Currently accepts image only
                if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                    $data = substr($base64_image, strpos($base64_image, ',') + 1);
                    $extension = getFileExtension($base64_image);
                    $file = base64_decode($data);
                    $filename = $document.'.'.$extension;

                    if(Storage::put($foldername. $filename, $file)){
                        $image_path = $foldername.$filename;
                        $customer->$document = $image_path;

                        $res = $document.'_upload_date';
                        $customer->$res = Carbon::now();
                    }
                }
            }
        }
        $customer->delivery_schedule_date = $delivery_schedule_date;
        $customer->update($validated);

        if ($request->has('product_discounts') && $request->input('product_discounts') && is_array($request->input('product_discounts'))) {
            $product_discounts = $request->input('product_discounts');

            DB::table('customer_item_discounts')// the pivot table
            ->where('customer_id', $customer->id)
            ->delete();

            foreach($product_discounts as $key => $item) {

                $dobj = CustomerItemDiscount::find($item['id']);
                $get_array[] = $item['id'];

                if($dobj == null){

                $discount_data = [
                    'customer_id' => $customer->id,
                    'item_id' => $item['item_id'],
                    'discount' => $item['discount'],
                    'srp' => $item['srp'],
                    'rebates' => $item['rebates'],
                    'deals' => $item['deals'],
                    ];

                    $get_id = $customer->product_discounts()->create($discount_data);
                    array_push($get_array,$get_id->id);
            }
                else {
                    $dobj->customer_id = $customer->id;
                    $dobj->item_id = $item['item_id'];
                    $dobj->discount = $item['discount'];
                    $dobj->srp = $item['srp'];
                    $dobj->rebates = $item['rebates'];
                    $dobj->deals = $item['deals'];
                    $dobj->save();
                }
        }

        $remove_null = array_filter($get_array);
        CustomerItemDiscount::where('customer_id', $customer->id)->whereNotIn('id',$remove_null)->delete();
    }

    if (empty($request->product_discounts))
    {
        CustomerItemDiscount::where('customer_id', $customer->id)->delete();
    }

        if ($request->has('shipping_address') && ($request->input('shipping_address') != null) && is_array($request->input('shipping_address'))) {
    		$shipping_address = $request->input('shipping_address');

            foreach ($shipping_address as $key => $shipping) {

                    $obj = Shipping::find($shipping['id']);
                    $get_array[] = $shipping['id'];
                    if($obj == null){

                       $shipping_data = [
                        'lba_name' => $shipping['lba_name'],
                        'lba_code' => $shipping['lba_code'],
                        'street' => $shipping['street'],
                        'barangay' => $shipping['barangay'],
                        'city' => $shipping['city'],
                        'province' => $shipping['province'],
                        'zip' => $shipping['zip'],
                        'country' => $shipping['country'],
                        'contact_number' => $customer->contact_number,
                    ];
                        $get_id = $customer->shipping_address()->create($shipping_data);
                        array_push($get_array,$get_id->id);

                    }
                    else {
                        $obj->lba_name = $shipping['lba_name'];
                        $obj->lba_code = $shipping['lba_code'];
                        $obj->street = $shipping['street'];
                        $obj->barangay = $shipping['barangay'];
                        $obj->country = $shipping['country'];
                        $obj->province = $shipping['province'];
                        $obj->zip = $shipping['zip'];
                        $obj->city = $shipping['city'];
                        $obj->contact_number = $customer->contact_number;
                        $obj->save();
                    }
            }

            $remove_null = array_filter($get_array);
            Shipping::where('customer_id', $customer->id)->whereNotIn('id',$remove_null)->delete();
        }

        if (empty($request->shipping_address))
        {
            Shipping::where('customer_id', $customer->id)->delete();
        }

        if ($request->has('billing_address') && ($request->input('billing_address') != null)) {
    		$billing_address = $request->input('billing_address');

            foreach ($billing_address as $key => $csm_address) {
            $address_data = [
                'lba_name' => $csm_address['lba_name'],
                'lba_code' => $csm_address['lba_code'],
                'street' => $csm_address['street'],
                'barangay' => $csm_address['barangay'],
                'city' => $csm_address['city'],
                'province' => $csm_address['province'],
                'zip' => $csm_address['zip'],
                'country' => $csm_address['country'],
                'contact_number' => $customer->contact_num
            ];

            $customer->billing_address()->update($address_data);

            }
        }

        $customer->load('shipping_address','billing_address','product_discounts');
        $this->params['results_count'] = 1;
        $this->params['results'] = $customer ;

        return $this->sendResponse($this->params, 'Resource updated successfully');
    }

     /**
     * @OA\Delete(
     * path="/api/v1/customers/{id}",
     * tags={"Customer Management"},
     * operationId="departmentDelete",
     * description="Delete a Customer by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="Customer",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="Customer successfully deleted"),
     *        )
     *     ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description="Customer Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example="Customer Not Found"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Delete a Customer by ID
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //do not delete
        // if (! $this->auth_user->can('delete_customer')) {
        //
        //     return response()->json([
        //         'responseMessage' => 'You do not have the required authorization.',
        //         'responseStatus'  => 403,
        //     ], Response::HTTP_NOT_FOUND
        //
        //   );
        // $query  = Customer::query();

        $customer = Customer::find($id);

        if (!$customer) {
            return response()->json(['error'=>true, 'message'=>'Requested resource does not exist'], Response::HTTP_NOT_FOUND);
        }

        $customer->delete();

        return $this->sendResponse([], 'Resource deleted successfully');
    }
     /**
     * @OA\PUT(
     * path="/api/v1/customers/{id}/change_status",
     * tags={"Customer Management"},
     * operationId="Customer Change_to_Status to ACTIVE or INACTIVE",
     * description="Change_to_Status to ACTIVE or INACTIVE a Customer by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="Customer id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     * @OA\RequestBody(
     *    required=true,
     *    description="Change_to_Status to ACTIVE or INACTIVE a Customer by ID",
     *    @OA\JsonContent(
     *       required={"status"},
     *        @OA\Property(property="status", type="string",  example="ACTIVE"),
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="Customer successfully add_courier_requirments"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/Customer"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=400,
     *    description="Bad Request",
     *    @OA\JsonContent(
     *            @OA\Property(property="success", type="boolean", example=false),
     *       @OA\Property(property="message", type="string", example="Validation Error"),
     *    )
     *  ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description="Customer Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example="Customer Found"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Change_to_Status to ACTIVE or INACTIVE a Customer by ID
     *
     * @return \Illuminate\Http\Response
     */
    public function set_active_inactive(Request $request, Customer $customer)
    {
        $customer->active = !$customer->active;
        $customer->save();

        $this->params['results_count'] = 1;
        $this->params['results'] = $customer;
        return $this->sendResponse([], 'Resource updated successfully');
    }

    public function get_all_customer_courier_requirements(Request $request)
    {
         $results =  CustomerCourierRequirmentResource::collection(CustomerCourierRequirement::all());

         $this->params['results_count'] = $results->count();
         $this->params['results'] = $results;

         return $this->sendResponse($this->params, 'Resource retrieved succesfully.');
    }

    public function customer_with_courier_requirements(Request $request, CustomerCourierRequirement $customer_courier)
    {

            $shipping_data = [
                'customer_id' => $request->post('customer_id'),
                'shipping_address_id' => $request->post('shipping_address_id'),
                'courier_id' => $request->post('courier_id'),
                'booker_id' => Auth::user()->id,
                'area' => $request->post('area'),
                'has_special_rqmts' => 1,
            ];

            $results = $customer_courier->create($shipping_data);

            $this->params['results'] = $results;
            return $this->sendResponse($this->params, 'Resource updated succesfully.');

         }

        public function update_customer_courier_requirements(Request $request, $id)
        {
            $find = CustomerCourierRequirement::find($id);
            $find->customer_id = $request->post('customer_id');
            $find->shipping_address_id = $request->post('shipping_address_id');
            $find->courier_id = $request->post('courier_id');
            $find->booker_id = Auth::user()->id;
            $find-> area = $request->post('area');
            $find-> has_special_rqmts = 1;
            $results = $find->save();

            $this->params['results'] = $results;
            return $this->sendResponse($this->params, 'Resource updated succesfully.');

        }

       public function get_customer_courier_requirements(Request $request, $id)
       {
            $collection =  CustomerCourierRequirmentResource::collection(CustomerCourierRequirement::all())
            ->where('id',$id)
            ->where('has_special_rqmts',1);
            $results = $collection->flatten();

            $this->params['results_count'] = 1;
            $this->params['results'] = $results;
            return $this->sendResponse($this->params, 'Resource retrieved successfully.');

       }

       public function remove_customer_courier_requirements($id)
       {
            $customer = CustomerCourierRequirement::find($id);
            if (!$customer) {
                return response()->json(['error'=>true, 'message'=>'Requested resource does not exist'], Response::HTTP_NOT_FOUND);
            }

            $customer->delete();

            return $this->sendResponse([], 'Resource deleted successfully');

        }


}
