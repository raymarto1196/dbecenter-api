<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\BankAccount;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class BankAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query  = BankAccount::query();

        $results = $query->get();

        $this->params['results_count'] = $results->count();
        $this->params['results'] = $results;

        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,  BankAccount $bank_account)
    {
        
        if ($request->has('bank_accounts') && $request->input('bank_accounts') && is_array($request->input('bank_accounts'))) {
            $customer_id = $request->input('customer_id');
            $bank_accounts = $request->input('bank_accounts');
      
            foreach($bank_accounts as $key => $bank) {
                $banksAccountData = [
		    		'customer_id' => $customer_id,
                    'bank_id' => $bank['bank_id'],
                    'bank_account' => $bank['bank_account'],
                    'transit_number' =>$bank['transit_number'],
                    'dd_transit_number' =>$bank['dd_transit_number'],
                    'currency' =>$bank['currency'],
                    'active' => 1,
                    'days_depto_clear' =>$bank['days_depto_clear'],
                    'current_balance' =>$bank['current_balance'],
                    'deposit' =>$bank['deposit'],
                    'withdrawal' =>$bank['withdrawal'],
		    	];

		    	$results = $bank_account->create($banksAccountData);
                
            }
            
            $this->params['results'] = $bank_accounts;

            return $this->sendResponse($this->params, 'Resource created succesfully.');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = BankAccount::find($id);
        $customer->get();

        $this->params['results_count'] = 1;
        $this->params['results'] = $customer;

        return $this->sendResponse($this->params, 'Resource retrieved successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BankAccount $bank_account)
    {
        $validated = $request->validate([
            'bank_id' => '',
            'bank_account' => '', 
            'transit_number' => '',
            'dd_transit_number' => '',
            'currency' => '',
            'active' => '', 
            'days_depto_clear' => '',
            'current_balance' => '',
            'deposit' => '',
            'withdrawal' => '',
        ]);

        $results = $bank_account->update($validated);
        $this->params['results'] = $results;
        return $this->sendResponse($this->params, 'Resource updated succesfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bank_accounts = BankAccount::find($id);
        $bank_accounts->delete();

        return $this->sendResponse([], 'Resource deleted successfully');
    }
}
