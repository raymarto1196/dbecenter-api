<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\CartItem;
use App\Models\Item;
use App\Models\Order;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order_id_array = [];
        $order_array = [];
        if ($request->has('items') && ($request->input('items') != null) && is_array($request->input('items'))) {
            $items = $request->input('items');

            //first seperate items by vendor ID
            foreach ($items as $key => $item) {
                $item_data = Item::findOrFail($item['item_id']);
                
                $data = [
                    'user_id'           => $this->auth_user->id,
                    'vendor_id'         => $item_data->user_id,
                    'name'              => $request->input('name'),
                    'telephone'         => $request->input('telephone'),
                    'description'       => $request->input('description'),
                    'shipping_address'  => $request->input('shipping_address'),
                    'status'            => 'pending',
                    'items'             => []

                ];

                if(!in_array($item_data->user_id,$order_id_array)){
                    array_push($order_id_array,$item_data->user_id);
                    array_push($order_array,$data);
                }

                $item = [
                    'item_id'           => $item_data->id,
                    'vendor_id'         => $item_data->user_id,
                    'quantity_ordered'  => $item['quantity_ordered'],
                    'item_name'         => $item_data->item_name,
                    'cart_item_id'      => $item['cart_item_id'],
                    'identifier'        => $item_data->identifier,
                    'total'             => $item_data->unit_cost * $item['quantity_ordered'],
                ];

                // check if data vendor id exist in order_array then add as items[]
                if(in_array($item_data->user_id,$order_id_array)){
                    $index = array_search($item_data->user_id, array_column($order_array, 'vendor_id'));
                    array_push($order_array[$index]['items'],$item);
                };
            }
            
        }

        //second insert as Order the seperated Order Items
        foreach ($order_array as $key => $order){
                $orderData = [
                    'user_id'           => $order['user_id'],
                    'vendor_id'         => $order['vendor_id'],
                    'name'              => $order['name'],
                    'telephone'         => $order['telephone'],
                    'description'       => $order['description'],
                    'shipping_address'  => $order['shipping_address'],
                    'status'            => $order['status'],
                    'reference_number'  => 'O-'.Carbon::now()->format('ymd').sprintf('%06d',  (Order::max('id')+1))
                ];
                $result = Order::Create($orderData);

                foreach($order['items'] as $key => $order_item){
                $order_itemData = [
                    'item_id'           => $order_item['item_id'],
                    'vendor_id'         => $order_item['vendor_id'],
                    'quantity_ordered'  => $order_item['quantity_ordered'],
                    'item_name'         => $order_item['item_name'],
                    'cart_item_id'      => $order_item['cart_item_id'],
                    'identifier'        => $order_item['identifier'],
                    'total'             => $order_item['total'],
                  ];     
                  $success = $result->order_items()->create($order_itemData);
                  $result->total_price += $success->total;
                  $result->save();

                    if($success){
                        $cart_item = CartItem::findOrFail($order_item['cart_item_id']);
                        $cart_item->delete();
                    }
                }


        }

        $this->params['message'] = 'Resource created successfully';
        $this->params['results_count'] = 1;
        $this->params['results'] = $result->load('order_items');

        return $this->sendResponse($this->params);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = Order::findOrFail($id);
        $result->delete();

        $this->params['message'] = 'Resource deleted successfully';
        $this->params['results_count'] = 1;
        $this->params['results'] = true;
        return $this->sendResponse($this->params);
    }

    public function get_user_orders(){
        $user = User::findOrFail($this->auth_user->id);
        $user->load(['orders.order_items.item:id,item_name,item_image','orders.order_items.vendor_company:user_id,name']);
        // $user->load('orders.order_items.vendor:id,firstname,lastname,profile_picture');

        $this->params['message'] = 'Resource retrieved successfully';
        $this->params['results_count'] = 1;
        $this->params['results'] = $user;
        return $this->sendResponse($this->params);
    }

    public function get_contractor_orders(){
        $result = Order::where('vendor_id',$this->auth_user->id)->get();
        $result->load(['order_items.item:id,item_name,item_image','profile:user_id,firstname,lastname']);

        $this->params['message'] = 'Resource retrieved successfully';
        $this->params['results_count'] = $result->count();
        $this->params['results'] = $result;
        return $this->sendResponse($this->params);
    }

    public function change_status(Request $request,$order_id){
        $result = Order::findOrFail($order_id);
        $result->status = $request->input('status');
        $result->save();

        $this->params['message'] = 'Resource updated successfully';
        $this->params['results_count'] = 1;
        $this->params['results'] = $result;
        return $this->sendResponse($this->params);
    }
}
