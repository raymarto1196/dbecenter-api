<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\SalesOrderResource;
use App\Models\Customer;
use App\Models\CustomerItemDiscount;
use App\Models\Item;
use App\Models\SalesOrder;
use App\Models\Shipping;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class SalesOrdersController extends Controller
{
    public function index_last_072021(Request $request) {
        $results = SalesOrder::where([
            [function ($query) use ($request){

            if (($search_string = $request->search_string)) {
                $search_string = '%' . $search_string . '%';
                $query->orWhere('created_at', 'LIKE', $search_string);
                $query->orWhere('po_number', 'LIKE', $search_string);
                $query->orWhere('so_number', 'LIKE', $search_string);
                $query->orWhere('customer_id', 'LIKE', $search_string);
                $query->orWhere('estimated_delivery_date', 'LIKE', $search_string);
                $query->orWhere('net_amount', 'LIKE', $search_string);
                $query->orWhere('gross_amount', 'LIKE', $search_string);
                $query->orWhere('received_by', 'LIKE', $search_string);
                $query->orWhere('status', 'LIKE', $search_string);

            }
            if(($start_date = $request->start_date) && ($end_date = $request->end_date)){

                $start_date = Carbon::createFromFormat('m-d-Y H:i:s', $start_date)->format('Y-m-d H:i:s');
                $end_date = Carbon::createFromFormat('m-d-Y H:i:s', $end_date)->format('Y-m-d H:i:s');
                $query->whereBetween('created_at', [$start_date, $end_date])->get();
            }
        }]

        ])
        ->get();

        $this->params['results_count'] = $results->count();
        $this->params['results'] = $results;
        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }
    
    public function index(Request $request)
    {
        $results =  SalesOrderResource::collection(SalesOrder::all());

        $this->params['results_count'] = $results->count();
        $this->params['results'] = $results;
        return $this->sendResponse($this->params, 'Resource retrieved successfully');

    }


    public function index2(Request $request)
    {
        $query = SalesOrder::with('items', 'statuses');

        $results =  $query->get();

        $this->params['results_count'] = $results->count();
        $this->params['results'] = $results;

        return $this->sendResponse($this->params, 'Resource retrieved successfully');

    }

    public function show(Request $request, SalesOrder $sales_order)
    {
        $data = new SalesOrderResource($sales_order);

        $this->params['results_count'] = 1;
        $this->params['results'] = $data;
        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

    public function store(Request $request)
    {   
        $customer = Customer::findOrFail($request->input('customer_id'));

        $shipping_address = Shipping::findOrFail($request->input('shipping_address'));

        $reference_number = date('ymd').'-'.sprintf($this->code_format, (SalesOrder::max('id')+1));

        $so_props = [
    		'reference_number'          => "BK-{$reference_number}",
    		'customer_po_number'        => $request->input('customer_po_number', null),
    		'customer_id'               => $request->input('customer_id'),
            'billing_address'           => $customer->billing_address->street.', '.$customer->billing_address->barangay.' '.$customer->billing_address->city.' '.$customer->billing_address->province.' '.$customer->billing_address->country.' '.$customer->billing_address->zip,
            'shipping_address'          => $shipping_address->street.', '.$shipping_address->barangay.' '.$shipping_address->city.' '.$shipping_address->province.' '.$shipping_address->country.' '.$shipping_address->zip,
            'customer_id'               => $request->input('customer_id'),
            'special_instruction'       => $request->input('special_instruction'),    
            'estimated_delivery_date'   => $request->input('estimated_delivery_date'), 
            'is_rush'                   => $request->input('is_rush'), 
            'payment_terms'             => $customer->payment_terms, 
            'received_by'               => null,
            'booker_id'                 => Auth::user()->id,
            'status'                    => 1,
    	];

    	$sales_order = SalesOrder::create($so_props);

    	if ($request->has('items') && ($request->input('items') != null) && is_array($request->input('items'))) {
    		$items = $request->input('items');

    		// add items to sales order
    		foreach ($items as $key => $item) {
                $item_details = Item::findOrFail($item['item_id']);

                //check if item is vatabable or not 
                if($item_details->vat_exemption == true){
                    $vat = $item['quantity'] * $item_details->unit_cost * .12;
                }else{
                    $vat = 0;
                }

		    	$so_item_data = [
		    		'item_id'       => $item['item_id'],
                    'quantity'      => $item['quantity'],
                    'free_goods'    => 0,
                    'unit_cost'     => $item_details->unit_cost,
                    'selling_price' => $item_details->selling_price,
                    'gross_amount'  => $item['quantity'] * $item_details->selling_price,
                    'discount'      => 0,
                    'vat'           => $vat,
                    'net_amount'    =>  $item['quantity'] * $item_details->selling_price - $vat,
                    // 'promo' => $item['promo']
		    	];

		    	$so_item = $sales_order->items()->create($so_item_data);
    
                $sales_order->gross_amount += $so_item->gross_amount;
                $sales_order->discount     += $so_item->discount;
                $sales_order->net_amount   += $so_item->net_amount;
                $sales_order->save();
	    	}
    	}
        
        $sales_order_status = [
            'status' => 1,
            'remarks' => $request->input('remarks'),
            'user_id' => auth()->user()->id
        ];

        $sales_order->statuses()->create($sales_order_status);

        $sales_order_evaluation = [
            'status' => 'pending',
        ];

        $sales_order->cc_evaluation()->create($sales_order_evaluation);


        $sales_order->load('items', 'statuses' ,'cc_evaluation');

        $this->params['results_count'] = 1;
        $this->params['results'] = $sales_order;

        return $this->sendResponse($this->params, 'Resource created successfully.', false, Response::HTTP_CREATED);

    }

    public function update(Request $request, SalesOrder $sales_order)
    {
        $customer = Customer::findOrFail($request->input('customer_id'));

        $shipping_address = Shipping::findOrFail($request->input('shipping_address'));

        $so_props = [
    		'customer_po_number'         => $request->input('customer_po_number', null),
    		'customer_id'                => $request->input('customer_id'),
            'billing_address'            => $customer->billing_address->street.', '.$customer->billing_address->barangay.' '.$customer->billing_address->city.' '.$customer->billing_address->province.' '.$customer->billing_address->country.' '.$customer->billing_address->zip,
            'shipping_address'           => $shipping_address->street.', '.$shipping_address->barangay.' '.$shipping_address->city.' '.$shipping_address->province.' '.$shipping_address->country.' '.$shipping_address->zip,
            'customer_id'                => $request->input('customer_id'),
            'special_instruction'        => $request->input('special_instruction'),    
            'estimated_delivery_date'    => $request->input('estimated_delivery_date'), 
            'is_rush'                    => $request->input('is_rush'), 
            'payment_terms'              => $customer->payment_terms, 
            'received_by'                => null,
            'status'                     => $request->input('status'),
    	];

        $sales_order->update($so_props);

        $sales_order->load('items', 'statuses');

        return $this->sendResponse($sales_order, 'Resource updated successfully');

    }

    public function change_status(Request $request, SalesOrder $sales_order)
    {
        $request->validate([
            'status' => ['required']
        ]);

        $sales_order->status = $request->input('status');

        $new_status = [
            'status'     => $request->input('status'),
            'remarks'    => $request->input('remarks'),
            'user_id'    => Auth::user()->id
        ];

        $sales_order->statuses()->create($new_status);
        $sales_order->save();

        $sales_order->load('items', 'statuses');

        return $this->sendResponse($sales_order, 'Resource updated successfully');

    }

    public function destroy(SalesOrder $sales_order)
    {
        $sales_order->delete();
        return $this->sendResponse([], 'Resource deleted successfully');
    }

    public function createStatusForOrder(Request $request, SalesOrder $sales_order) {
        return $sales_order;
    }


    public function get_customer(Request $request)
    {
        $data = User::findOrFail(Auth::user()->id);

        $this->params['results_count'] = 1;
        $this->params['results'] = $data->load('customers');

        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }
}

