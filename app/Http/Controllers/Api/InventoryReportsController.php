<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\InventoryReportView;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Item;
use App\Models\WarehouseCenter;
use App\Models\InventoryTransfer;
use App\Models\WarehousePalletAssignmentView;
use DB;

class InventoryReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = InventoryReportView::selectRaw(
           'id,
            item_name,
            item_code,
            item_description,
            warehouse_id,
            warehouse_name,
            SUM(balance) as total_amount')
        ->groupBy('item_name','item_code','warehouse_name')->get();

        $this->params['results_count'] = $results->count();
        $this->params['results'] = $results;
        return $this->sendResponse($this->params, 'resource retrieved successfully');

    }

    public function get_reports(Request $request)
    { 

            $results = InventoryReportView::all();
            
            $this->params['results_count'] = $results->count();
            $this->params['results'] = $results;
            
            return $this->sendResponse($this->params, 'Resource retrieved successfully');
        // } 

    }

    public function get_warehouse_pallet_assignment(Request $request)
    {        
        $results = WarehousePalletAssignmentView::where('item_code', 'LIKE', '%'.$request->input('item_code').'%')
        ->where('item_name', 'LIKE', '%'.$request->input('item_name').'%')
        ->where('company', 'LIKE', '%'.$request->input('company').'%')
        ->where('shelf_life_status', 'LIKE', '%'.$request->input('shelf_life_status').'%')
        ->where('lot_number', 'LIKE', '%'.$request->input('lot_number').'%')
        ->where('warehouse_name', 'LIKE', '%'.$request->input('warehouse_name').'%')
        ->where('level', 'LIKE', '%'.$request->input('level').'%')
        ->where('bin_name', 'LIKE', '%'.$request->input('bin_name').'%')
        ->where('bin_status', 'LIKE', '%'.$request->input('bin_status').'%')
        ->where('ssl_status', 'LIKE', '%'.$request->input('ssl_status').'%')
        ->get();

        $this->params['results_count'] = $results->count();
        $this->params['results'] = $results;
        
        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

    public function get_pallet_status(Request $request)
    {        
        $results = WarehousePalletAssignmentView::where('bin_status', 'LIKE', '%'.$request->input('bin_status').'%')
        ->get();

        $this->params['results_count'] = $results->count();
        $this->params['results'] = $results;
        
        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }
}
