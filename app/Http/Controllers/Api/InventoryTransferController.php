<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\InventoryTransferItemsResource;
use App\Http\Resources\InventoryTransferResource;
use App\Models\InventoryReceipt;
use App\Models\InventoryReceiptItem;
use App\Models\InventoryTransfer;
use App\Models\Item;
use App\Models\Vendor;
use App\Models\MaintenanceCodeSetup;
use App\Models\InventoryTransferItem;
use App\Models\ItemBatch;
use App\Models\ItemQuantity;
use App\Models\Warehouse;
use App\Models\WarehousePallet;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class InventoryTransferController extends Controller
{
      /**
     * @OA\Get(
     * path="/api/v1/inventory-transfers",
     * tags={"Inventory Transfer"},
     * operationId="Inventory TransferIndex",
     * description="Get List of Inventory Transfer",
     * security={
     *       {"api_key": {}}
     *     },
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="List of Inventory Transfer successfully retrieved"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/InventoryTransfer"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     *
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Display a listing of the Inventory Transfer.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = InventoryTransferResource::collection(InventoryTransfer::all());

        $this->params['total_approved'] = $data->where('status', 'approved')->whereNotNull('approved_date')->count();
        $this->params['total_request'] = $data->count();
        $this->params['results'] = $data;
        $this->params['approved_results'] = null;
        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

	
     /**
     * @OA\Post(
     * path="/api/v1/inventory-transfers",
     * tags={"Inventory Transfer"},
     * operationId="Inventory TransferStore",
     * description="Create Inventory Transfer",
     * security={
     *       {"api_key": {}}
     *     },
     *  @OA\RequestBody(
     *    required=true,
     *    description="Create Inventory Transfer",
     *    @OA\JsonContent(
     *       required={"itr_item_list","item_id","supplier_id","batch_no","location","pallet_assignment","quantity","dimension","weight","quantity_pack","shelf_life_status","min_temp","min_humid"},
     * @OA\Property(property="itr_item_list", type="object",
     *              type="array",
     *              @OA\Items(
     *       @OA\Property(property="item_id", type="boolean", example=1),
     *       @OA\Property(property="supplier_id", type="string", example="1"),
	 *       @OA\Property(property="batch_no", type="string", example="B-001"),
	 *       @OA\Property(property="location", type="string", example="RA 3F"),
	 *       @OA\Property(property="pallet_assignment", type="string", example="5A1-585"),
	 *       @OA\Property(property="quantity", type="string", example="670"),
	 *       @OA\Property(property="dimension", type="string", example="80x120x30"),
	 *       @OA\Property(property="weight", type="string", example="1.2"),
	 *       @OA\Property(property="quantity_pack", type="string", example="100"),
	 *       @OA\Property(property="shelf_life_status", type="string", example="Above 12 months"),
	 *       @OA\Property(property="min_temp", type="string", example="23.0c"),
	 *       @OA\Property(property="min_humid", type="string", example="50%"),
     *       @OA\Property(property="requestor", type="string",  example="ADMIN"),
     *       @OA\Property(property="status", type="boolean",  example="0"),
     *       @OA\Property(property="approved_date", type="string",  example="2021-04-08"),  
     *             ),
     *         ),    
     *   ) 
     * ),
     * 
     * 
     * @OA\Response(
     *    response=400,
     *    description="Bad Request", 
     *    )
     *  ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     *
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Create New Inventory Transfer.
     *
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request, InventoryTransfer $model)
    {
        $InventoryTransferData = [
            'requested_by' => Auth::user()->id,
            'status' => "pending"
    	];
   
        $newIVTI = $model->create($InventoryTransferData);  
        $newIVTI->itr_code = 'ITR-'.sprintf('%04d', $newIVTI->id);
        $newIVTI->save();

        if ($request->has('items') && ($request->input('items') != null) && is_array($request->input('items'))) {
    		$items = $request->input('items');

    		// add items on inventory receipt
    		foreach ($items as $key => $item) {

		    	$newIVTItemData = [
		    		'inventory_transfer_id' =>$newIVTI->id,
                    'warehouse_id_origin' => $item['warehouse_id_origin'],
                    'warehouse_pallet_id_origin' => $item['warehouse_pallet_id_origin'],
                    'item_id' => $item['item_id'],
                    'warehouse_id' => $item['warehouse_id'],
                    'warehouse_pallet_id' => $item['warehouse_pallet_id'],
                    'quantity' => $item['quantity'],
		    	];

                $check_quantity = ItemBatch::where('item_id',$item['item_id'])
                ->where('warehouse_id',$item['warehouse_id_origin'])
                ->where('warehouse_pallet_id',$item['warehouse_pallet_id_origin'])
                ->first();
                
                if ($check_quantity === null) {
                    return $this->sendResponse([],'Resource item_bataches is null');
                }
                //if quantity to transfer is less than to the quantity requested error
                if ($check_quantity->quantity_received < $item['quantity']) {
                    return $this->sendResponse([], 'Resource to transfer is greater than to the quantity requested');
                }

		    	$results = $newIVTI->items()->create($newIVTItemData);
            }

        }

        $newIVTI->load('items');
        $this->params['results_count'] = 1;
        $this->params['results'] = $newIVTI;
        return $this->sendResponse($this->params, 'Resource successfully created.', false, Response::HTTP_CREATED);
    }


      /**
     * @OA\Get(
     * path="/api/v1/inventory-transfers/{id}",
     * tags={"Inventory Transfer"},
     * operationId="Inventory TransferShow",
     * description="Get a Inventory Transfer by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="Inventory Transfer id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="InventoryTransfer successfully retrieved"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/InventoryTransfer"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description="InventoryTransfer Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example="department Not Found"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Get Inventory Transfer By ID.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data =  InventoryTransferResource::collection(InventoryTransfer::all())
        ->where('id',$id);

        $results = $data->flatten();
     
        $this->params['results_count'] = 1;
        $this->params['results'] = $results;
        return $this->sendResponse($this->params, 'Resource successfully retrieved.');
    }
  
     /**
     * @OA\Put(
     * path="/api/v1/inventory-transfers/{id}",
     * tags={"Inventory Transfer"},
     * operationId="Inventory TransferUpdate",
     * description="Update a Inventory Transfers by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="Inventory Transfers id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     * @OA\RequestBody(
     *    required=true,
     *    description="Update a Inventory Transfers by ID",
     *    @OA\JsonContent(
     *    required={"item_id","supplier_id","batch_no","location","pallet_assignment","quantity","dimension","weight","quantity_pack","shelf_life_status","min_temp","min_humid","requestor","status","approved_date"},
     *    @OA\Property(property="item_id", type="string", example="1"),
	 *    @OA\Property(property="supplier_id", type="string", example="1"),
	 *    @OA\Property(property="batch_no", type="string", example="B-001"),
	 *    @OA\Property(property="location", type="string", example="RA 3F"),
	 *    @OA\Property(property="pallet_assignment", type="string", example="5A1-585"),
	 *    @OA\Property(property="quantity", type="string", example="670"),
	 *    @OA\Property(property="dimension", type="string", example="80x120x30"),
	 *    @OA\Property(property="weight", type="string", example="1.2"),
	 *    @OA\Property(property="quantity_pack", type="string", example="100"),
	 *    @OA\Property(property="shelf_life_status", type="string", example="Above 12 months"),
	 *    @OA\Property(property="min_temp", type="string", example="23.0c"),
	 *    @OA\Property(property="min_humid", type="string", example="50%"),
     *    @OA\Property(property="requestor", type="string",  example="ADMIN"),
     *    @OA\Property(property="status", type="boolean",  example="0"),
     *    @OA\Property(property="approved_date", type="string",  example="2021-04-08"),   
     *       ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="Inventory Transfer successfully updated"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/InventoryTransfer"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=400,
     *    description="Bad Request",
     *    @OA\JsonContent(
     *            @OA\Property(property="success", type="boolean", example=false),
     *       @OA\Property(property="message", type="string", example="Validation Error"),
     *    )
     *  ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description="Inventory Transfer Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example="Inventory Transfer Not Found"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Update a Inventory Transfer by ID
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InventoryTransfer $inventoryTransfer)
    {
        $inventoryTransferData = [
            'updated_at' => Carbon::now(),
    	];

        $inventoryTransfer->update($inventoryTransferData);

        if ($request->has('items') && ($request->input('items') != null) && is_array($request->input('items'))) {
        $items = $request->input('items');

        foreach ($items as $key => $item) {
            
            $obj = InventoryTransferItem::find($item['id']);
            $get_array[] = $item['id'];
       
            if($obj == null){
                $newIVTItemData = [
                    'inventory_transfer_id' =>$inventoryTransfer->id,
                    'warehouse_id_origin' => $item['warehouse_id_origin'],
                    'warehouse_pallet_id_origin' => $item['warehouse_pallet_id_origin'],
                    'item_id' => $item['item_id'],
                    'warehouse_id' => $item['warehouse_id'],
                    'warehouse_pallet_id' => $item['warehouse_pallet_id'],
                    'quantity' => $item['quantity'],
                ];

               $get_id = $inventoryTransfer->items()->create($newIVTItemData);
               array_push($get_array,$get_id->id);
            }
            else{
                    $obj->inventory_transfer_id = $inventoryTransfer->id;
                    $obj->warehouse_id_origin = $item['warehouse_id_origin'];
                    $obj->warehouse_pallet_id_origin = $item['warehouse_pallet_id_origin'];
                    $obj->item_id =  $item['item_id'];
                    $obj->warehouse_id = $item['warehouse_id'];
                    $obj->warehouse_pallet_id = $item['warehouse_pallet_id'];
                    $obj->quantity =  $item['quantity'];
                    $obj->save();
            }
         }
         $remove_null = array_filter($get_array);
         InventoryTransferItem::where('inventory_transfer_id', $inventoryTransfer->id)->whereNotIn('id',$remove_null)->delete();

         if (empty($request->items))
         {
            InventoryTransferItem::where('inventory_transfer_id', $inventoryTransfer->id)->delete();
         }

        } 
        $this->params['results_count'] = 1;
        $this->params['data'] =  $inventoryTransfer->load('items');
        return $this->sendResponse($this->params, 'Resource updated successfully');  
    }


      /**
     * @OA\Delete(
     * path="/api/v1/inventory-transfers/{id}",
     * tags={"Inventory Transfer"},
     * operationId="Inventory TransferDelete",
     * description="Delete a Inventory Transfer by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="Inventory Transfer",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="Inventory Transfer successfully deleted"),
     *        )
     *     ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description="Inventory Transfer Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example="Inventory Transfer Not Found"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Delete a Inventory Transfer by ID
     *
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $results = InventoryTransfer::find($id);
        if (!$results) return response()->json(['error'=>true, 'message'=>'Requested Inventory Transfer does not exist'], Response::HTTP_NOT_FOUND);

        $results->delete();

        return $this->sendResponse([], ' Inventory Transfer successfully deleted.');
    }

              /**
     * @OA\POST(
     * path="/api/v1/inventory-transfer/{id}/change-status",
     * tags={"Inventory Transfer"},
     * operationId="Inventory Transfer_approval_status",
     * description="Change_approval_status a Inventory Transfer by ID",
     * security={
     *       {"api_key": {}}
     *     },
     *      @OA\Parameter(
     *          name="id",
     *          description="Inventory Transfer id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *  @OA\RequestBody(
     *    required=true,
     *    description="approval_status of Inventory Transfer by ID",
     *    @OA\JsonContent(
     *       required={"status"},
     *        @OA\Property(property="status", type="boolean",  example=1),
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=true),
     *      @OA\Property(property="message", type="string", example="Inventory Transfer successfully approval_status"),
     *      @OA\Property(property="data", type="array",  collectionFormat="multi",
     *              @OA\Items(
     *                  type="object", ref="#/components/schemas/InventoryTransfer"
     *              )
     *          ),
     *        )
     *     ),
     * @OA\Response(
     *    response=400,
     *    description="Bad Request",
     *    @OA\JsonContent(
     *            @OA\Property(property="success", type="boolean", example=false),
     *       @OA\Property(property="message", type="string", example="Validation Error"),
     *    )
     *  ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *  ),
     * @OA\Response(
     *    response=404,
     *    description="Inventory Transfer  Not Found",
     *    @OA\JsonContent(
     *      @OA\Property(property="success", type="boolean", example=false),
     *      @OA\Property(property="message", type="string", example="Inventory Transfer Found"),
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Internal Server Error",
     *  )
     * )
     * Change_to_credited Inventory Transfer by ID
     *
     * @return \Illuminate\Http\Response
     */
    
    public function change_status(Request $request ,InventoryTransfer $inventoryTransfer)
    {
        $status = 'approved';

        if ('approved' == $inventoryTransfer->status) {
            return $this->sendResponse([], 'Resource status was already approved');
        }

        if($status == "approved")
        {
            $inventoryTransfer->approved_by = Auth::user()->id;
            $inventoryTransfer->approved_date = Carbon::now();
            $inventoryTransfer->status = $status;

            $data = InventoryTransfer::where('id',$inventoryTransfer->id)
            ->with(['items'])
            ->first();
            $count = $data->items->count();
            $array[] = $count;

                for ($a=0; $a < $count ; $a++) {
                    //get the selected item_batches data by item_id,warehouse_id_origin,warehouse_pallet_id_origin and deduct a quantity
                    $get_lot_number = ItemBatch::where('item_id',$data->items[$a]->item_id)
                    ->where('warehouse_id',$data->items[$a]->warehouse_id_origin)
                    ->where('warehouse_pallet_id',$data->items[$a]->warehouse_pallet_id_origin)
                    ->first();  

                    $get_lot_number->quantity_received = $get_lot_number->quantity_received - $data->items[$a]->quantity;
                    $get_lot_number->save();    

                    //duplicate and save as new item_batches with the same lot number of that item
                    //along with its quantity to deduct from the warehouse_origin and add into the warehouse_destination
                    $item_lot_number = [
                        'item_id' => $get_lot_number->item_id,
                        'warehouse_id' => $data->items[$a]->warehouse_id,
                        'warehouse_pallet_id' => $data->items[$a]->warehouse_pallet_id,
                        'lot_number' => $get_lot_number->lot_number,
                        'date_received' => Carbon::now(),
                        'quantity_received' => $data->items[$a]->quantity,
                        'batch_number_sold' => 1,
                        'manufacture_date' => $get_lot_number->manufacture_date,
                        'expiry_date' => $get_lot_number->expiry_date,
                    ];
    
                    $results = ItemBatch::create($item_lot_number);

                    //TO DO: Deduct the quantity in item_master
    
                    // deduct the quantity from item_quantities
                    $deduct_quantities = ItemQuantity::where('item_id',$data->items[$a]->item_id)
                    ->where('warehouse_id',$data->items[$a]->warehouse_id_origin)
                    ->where('warehouse_pallet_id',$data->items[$a]->warehouse_pallet_id_origin)
                    ->with(['items'])
                    ->first();
                  
                    if ($deduct_quantities === null) {
                        return $this->sendResponse([], 'Resource item_quantites to deduct does not exists');
                    }
    
                    $deduct_quantities->onhand_quantity = $deduct_quantities->onhand_quantity - $results->quantity_received;
                    $deduct_quantities->save();

                    //TO DO: Add, Deducted quantity in item_master
    
                    // Add, Deducted quantity into item_quantities
                    // bellow code will add the deducted quantity into the new warehouse_id and warehouse_pallet_id  
                    $add_quantities = ItemQuantity::where('item_id',$data->items[$a]->item_id)
                    ->where('warehouse_id',$data->items[$a]->warehouse_id)
                    ->where('warehouse_pallet_id',$data->items[$a]->warehouse_pallet_id)
                    ->with(['items'])
                    ->first();
                
                    if ($add_quantities === null) {
                        $item_data = [
                            'item_id' => $data->items[$a]->item_id,
                            'warehouse_id' => $data->items[$a]->warehouse_id,
                            'warehouse_pallet_id' => $data->items[$a]->warehouse_pallet_id,
                            'onhand_quantity' => $data->items[$a]->quantity,
                            'vendor_id' => 0,
                        ];
                        $results = ItemQuantity::create($item_data);

                        $find_warehouse = WarehousePallet::where('warehouse_pallets.id', $results->warehouse_pallet_id)
                        ->first();
        
                        $find_warehouse->bin_status = 1;
                        $find_warehouse->save();
                    } 
                    
                    else {
                        $add_quantities->onhand_quantity = $add_quantities->onhand_quantity + $results->quantity_received;
                        $add_quantities->save();
                    }
           }

        }

        elseif($status == "cancelled" )
        {
            $inventoryTransfer->status = $status;
        }

        $inventoryTransfer->save();

        return $this->sendResponse($inventoryTransfer->load('items'), 'Resource successfully updated');
    }

    public function get_items(){
    
        $items = ItemQuantity::with('items:id,item_code,item_name,item_generic_name,vendor_id,shelf_life')->get(['id','warehouse_id','warehouse_pallet_id','item_id']);
        
        $this->params['results_count'] = $items->count();
        $this->params['results'] = $items;
        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }
    
    public function get_item_batches(Request $request){
      
        $results = ItemBatch::select()
        ->where('item_id', 'LIKE', $request->input('item_id'))
        ->where('warehouse_id', 'LIKE',$request->input('warehouse_id'))
        ->where('warehouse_pallet_id','LIKE', $request->input('warehouse_pallet_id'))
        ->get();
        
        $this->params['results_count'] = $results->count();
        $this->params['results'] = $results;
        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

}
