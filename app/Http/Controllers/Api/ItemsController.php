<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\MaintenanceCodeSetup;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Carbon\Carbon;
use App\Exceptions\Handler;
use App\Models\Warehouse;
use App\Models\WarehousePallet;
use Auth;
use Illuminate\Support\Facades\Storage;

class ItemsController extends Controller
{
    protected $documents = ['small_box_image','big_box_image','sell_unit_box_image','sell_pack_box_image','cpr_document','assay_test', 'fda_document', 'vendor_document', 'artwork', 'box_specification', 'item_image'];
    protected $allowedfileExtension = ['pdf','jpg','png','docx'];

    public function docs()
    {
        return [];
    }

    /**
    * Display a listing of the Items.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        //do not delete
        // if (! $this->auth_user->can('list_item')) {
        //
        //     return response()->json([
        //         'responseMessage' => 'You do not have the required authorization.',
        //         'responseStatus'  => 403,
        //     ], Response::HTTP_NOT_FOUND
        //
        //   );
        // }
        // Role::find(3)->syncPermissions([3,4, 'list_sales_order']);

        // return response()->json($this->auth_user->can('update_inventory_receipt'));
        // return response()->json($this->auth_user->getAllPermissions());
        // $query = Item::query();

        // if ($request->exclude_ids && is_array($request->exclude_ids)) {
        //     $query->whereNotIn('id', $request->exclude_ids);
        // }

        // $results = $query->get();

        // $this->params['results_count'] = $results->count();
        // $this->params['results'] = $results;

        // return $this->sendResponse($this->params, 'Resource retrieved successfully');

        // $results = Item::where([
        // [function ($query) use ($request) {
        //     if (($search_string = $request->search_string)) {
        //         $search_string = '%' . $search_string . '%';
        //         $query->orWhere('item_code', 'LIKE', $search_string);
        //         $query->orWhere('item_name', 'LIKE', $search_string);
        //         $query->orWhere('item_description', 'LIKE', $search_string);
        //         $query->orWhere('item_segment', 'LIKE', $search_string);
        //         $query->orWhere('item_sub_segment', 'LIKE', $search_string);
        //         $query->orWhere('item_group', 'LIKE', $search_string);
        //         $query->orWhere('item_sub_group', 'LIKE', $search_string);
        //         $query->orWhere('pharmacological_category', 'LIKE', $search_string);
        //         $query->orWhere('current_cost', 'LIKE', $search_string);
        //         $query->orWhere('standard_cost', 'LIKE', $search_string);
        //     }

        //     if (($start_date = $request->start_date) && ($end_date = $request->end_date)) {
        //         $start_date = Carbon::createFromFormat('m-d-Y H:i:s', $start_date)->format('Y-m-d H:i:s');
        //         $end_date = Carbon::createFromFormat('m-d-Y H:i:s', $end_date)->format('Y-m-d H:i:s');
        //         $query->whereBetween('created_at', [$start_date, $end_date])->get();
        //     }
        // }]
        // ])->get();

        // $this->params['results_count'] = $results->count();
        // $this->params['message'] = 'Resource Retrieved successfully';
        // $this->params['results'] = $results;
        // return $this->sendResponse($this->params);

        $results = Item::where([
        [function ($query) use ($request) {
            if (($search_string = $request->search_string)) {
                $search_string = '%' . $search_string . '%';
                $query->orWhere('item_name', 'LIKE', $search_string);
                $query->orWhere('identifier', 'LIKE', $search_string);
                $query->orWhere('item_type', 'LIKE', $search_string);
                $query->orWhere('item_description', 'LIKE', $search_string);
            }
        }]
        ])->get();


        $this->params['results_count'] = $results->count();
        $this->params['results'] = $results;
        $this->params['message'] = 'Resource retrieved successfully';

        return $this->sendResponse($this->params);
    }


    /**
     * Create New Items.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Item $model)
    {
        //IMPORTANT: DO NOT DELETE
        // if (! $this->auth_user->can('create_item')) {
        //
        //     return response()->json([
        //         'responseMessage' => 'You do not have the required authorization.',
        //         'responseStatus'  => 403,
        //     ], Response::HTTP_NOT_FOUND
        //
        //   );
        // }

        // $request->validate([]);

        $data = [
            'user_id'          => $this->auth_user->id,
            'identifier'       => $request->input('identifier', 'SAMPLEONLY'),
            'item_type'        => strtoupper($request->input('item_type', 'SERVICES')),
            'category_id'      => $request->input('category_id', 1),
            'item_name'        => $request->input('item_name', 'SAMPLE' . date('YmdHqis')),
            'item_description' => $request->input('item_description', 'SAMPLEDESCRIPTION'),
            'quantity'         => $request->input('quantity', 0),
            'unit_cost'        => $request->input('unit_cost', 0),
            'from'             => $request->input('from'),
            'to'               => $request->input('to'),
            'status'           => 'depends on the Client'
        ];

        // Save item properties
        $result = $model->create($data);

        if($request->has('item_image') && $request->input('item_image')){
            $base64_image = $request->input('item_image');
            $folder_name = '';
            if($result->category_id == 1){
                $folder_name = 'services';
            }elseif($result->category_id == 2){
                $folder_name = 'products';
            }else{
                $folder_name = 'projects';
            }

            // Currently accepts image only
            if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                $data = substr($base64_image, strpos($base64_image, ',') + 1);
                $extension = getFileExtension($base64_image);
                $file = base64_decode($data);
                $filename =  'itemId-'.$result->id.'.'.$extension;
                $result->item_image = $filename;

                if(Storage::put('item/'.$folder_name.'/user/'. $filename, $file)){
                    $image_path = 'item/'.$folder_name.'/user/'.$filename;
                    $result->item_image = $image_path;
                }
            }
        }
        
        $result->save();

        $this->params['results_count'] = 1;
        $this->params['results'] = $result;

        return $this->sendResponse($this->params, Response::HTTP_CREATED);
    }

    /**
     * Get Item By ID.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Item $item)
    {
        //do not delete
        // if (! $this->auth_user->can('read_item')) {
        //
        //     return response()->json([
        //         'responseMessage' => 'You do not have the required authorization.',
        //         'responseStatus'  => 403,
        //     ], Response::HTTP_NOT_FOUND
        //
        //   );
        // }

        $this->params['results_count'] = 1;
        $this->params['results'] = $item->load(['user:id,username','user_profile:user_id,firstname,lastname,profile_picture,address2','user.roles:id,name']);
        $this->params['message'] = 'Resource retrieved successfully';

        return $this->sendResponse($this->params);
    }

    /**
     * Update Item by ID
     *
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Item $item)
    {
        //do not delete
        // if (! $this->auth_user->can('update_item')) {
        //
        //     return response()->json([
        //         'responseMessage' => 'You do not have the required authorization.',
        //         'responseStatus'  => 403,
        //     ], Response::HTTP_NOT_FOUND
        //
        //   );
        // }

        // if (($cpr_expiry = $request->cpr_expiry)) {
        //     $cpr_expiry = Carbon::createFromFormat('m-d-Y', $cpr_expiry)->format('Y-m-d');
        //     $item->cpr_expiry = $cpr_expiry;
        // }

        $data = [
            'user_id' => $this->auth_user->id,
            'identifier' => $request->input('identifier'),
            'item_type' => strtoupper($request->input('item_type')),
            'category_id' => $request->input('category_id'),
            'item_name' => $request->input('item_name'),
            'item_description' => $request->input('item_description'),      
            'quantity' => $request->input('quantity',0),
            'unit_cost' => $request->input('unit_cost'),
            'from'             => $request->input('from'),
            'to'               => $request->input('to'),
        ];

        $item->update($data);

        if($request->has('item_image') && $request->input('item_image')){
            $base64_image = $request->input('item_image');
            $folder_name = '';
            if($item->category_id == 1){
                $folder_name = 'services';
            }elseif($item->category_id == 2){
                $folder_name = 'products';
            }else{
                $folder_name = 'projects';
            }

            // Currently accepts image only
            if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                $data = substr($base64_image, strpos($base64_image, ',') + 1);
                $extension = getFileExtension($base64_image);
                $file = base64_decode($data);
                $filename =  'itemId-'.$item->id.'.'.$extension;
                $item->item_image = $filename;

                if(Storage::put('item/'.$folder_name.'/user/'. $filename, $file)){
                    $image_path = 'item/'.$folder_name.'/user/'.$filename;
                    $item->item_image = $image_path;
                }
            }
        }
        
        // Resave model to attach the documents uploaded
        $item->save();

        $this->params['message'] = 'Resource updated successfully';
        $this->params['results_count'] = 1;
        $this->params['results'] = $item;

        return $this->sendResponse($this->params);
    }

    /**
    * Delete a Item by ID
    *
    * @return \Illuminate\Http\Response
    */
    public function destroy(Item $item)
    {
        //do not delete
        // if (! $this->auth_user->can('delete_item')) {
        //
        //     return response()->json([
        //         'responseMessage' => 'You do not have the required authorization.',
        //         'responseStatus'  => 403,
        //     ], Response::HTTP_NOT_FOUND
        //
        //   );
        // }

        // TODO: Must implement event ItemDeleted for detailed processing...
        $item->delete();
        $this->params['message'] = 'Resource deleted successfully';
        return $this->sendResponse($this->params);
    }
    
    public function item_category(Request $request, $category_type){
        $results = Item::where('category_id', $category_type)->where([
            [function ($query) use ($request) {
                if (($search_string = $request->search_string)) {
                    $search_string = '%' . $search_string . '%';
                    $query->orWhere('item_name', 'LIKE', $search_string);
                    $query->orWhere('identifier', 'LIKE', $search_string);
                    $query->orWhere('item_type', 'LIKE', $search_string);
                    $query->orWhere('item_description', 'LIKE', $search_string);
                }
            }]
            ])->get();
        
        $this->params['message'] = 'Resource retrieved successfully';
        $this->params['results_count'] = $results->count();
        $this->params['results'] = $results;

        return $this->sendResponse($this->params);
    }

    public function user_item_category(Item $item, $category_type){
        
        $results = $item->where('user_id', Auth::user()->id)->where('category_id', $category_type)->get();
        // return $item->find($category_type);

        $this->params['message'] = 'Resource retrieved successfully';
        $this->params['results_count'] = $results->count();
        $this->params['item_star_rating'] = 1;
        $this->params['results'] = $results->load('item_ratings.user.profile:user_id,firstname,lastname,profile_picture')->load('bids.user.profile:user_id,firstname,lastname,profile_picture');

        return $this->sendResponse($this->params);

    }
}
