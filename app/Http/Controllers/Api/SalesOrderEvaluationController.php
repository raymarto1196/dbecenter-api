<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\SalesOrderEvaluationResource;
use App\Models\SalesOrder;
use App\Models\SalesOrderEvaluation;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SalesOrderEvaluationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = SalesOrderEvaluationResource::collection(SalesOrderEvaluation::all());

        $this->params['results_count'] = $results->count();
        $this->params['results'] = $results;
        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SalesOrderEvaluation $sales_order_evaluation)
    {
        $result = new SalesOrderEvaluationResource($sales_order_evaluation);

        $this->params['results_count'] = 1;
        $this->params['results'] = $result;

        return $this->sendResponse($this->params, 'Resource retrieved successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SalesOrderEvaluation $sales_order_evaluation)
    {
        $status = $request->input('status');

        if($status == 'approved'){
            $evaluation = [
                'approved_reason' => $request->input('approved_reason'),
                'status'          => $status
            ];

            $sales_order = SalesOrder::findOrFail($sales_order_evaluation->sales_order_id);
            $sales_order->status = 2; //so status = approved credit collection evaluation 

            $new_status = [
                'status'     => 1,
                'remarks'    => 'credit_pproved',
                'user_id'    => Auth::user()->id
            ];
            $sales_order->save();
            $sales_order->statuses()->create($new_status);

        }elseif($status == 'for_evaluation'){
            $evaluation =[
                'remark' => $request->input('remark'),
                'status' => $status,
            ];

        }elseif($status == 'cancelled'){
            $evaluation = [
                'status' => $status
            ];
        }

        $results = $sales_order_evaluation->update($evaluation);

        $this->params['results'] = $results;
        return $this->sendResponse($this->params, 'Resource updated succesfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
