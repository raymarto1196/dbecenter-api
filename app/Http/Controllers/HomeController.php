<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $sql2 = User::where('id', '=', 1)
                    ->where('status', '=', 1)
                    ->where('email', '=', 1)
                ->toSql();
        // generates:  select * from `users` where `id` = ? and `status` = ? and `email` = ? and `users`.`deleted_at` is null

        $sql = User::where([
            ['id', '=', 1],
            ['status', '=', 1],
            ['email', '=', 1]
            ])->toSql();
        // generates: select * from `users` where (`id` = ? and `status` = ? and `email` = ?) and `users`.`deleted_at` is null

        echo $sql;
        echo '<br />';
        echo $sql2;
        return;
        return view('home');
    }
}
