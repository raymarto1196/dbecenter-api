<?php

namespace App\Http\Controllers;

use App\Models\ContractorRequestItem;
use App\Http\Requests\StoreContractorRequestItemRequest;
use App\Http\Requests\UpdateContractorRequestItemRequest;

class ContractorRequestItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreContractorRequestItemRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreContractorRequestItemRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ContractorRequestItem  $contractorRequestItem
     * @return \Illuminate\Http\Response
     */
    public function show(ContractorRequestItem $contractorRequestItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ContractorRequestItem  $contractorRequestItem
     * @return \Illuminate\Http\Response
     */
    public function edit(ContractorRequestItem $contractorRequestItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateContractorRequestItemRequest  $request
     * @param  \App\Models\ContractorRequestItem  $contractorRequestItem
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateContractorRequestItemRequest $request, ContractorRequestItem $contractorRequestItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ContractorRequestItem  $contractorRequestItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContractorRequestItem $contractorRequestItem)
    {
        //
    }
}
