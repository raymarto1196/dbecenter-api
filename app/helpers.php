<?php
use Illuminate\Support\Facades\Storage;

/**
 * Description: Upload file to a specific folder and return file uploaded relative path
 * Url: site.com/storage/{$path}
 */
if (!function_exists('uploadFileToPath')) {
    function uploadFileToPath( $foldername, $file, $filename) {
        $path = Storage::putFileAs($foldername, $file, $filename);
        return $path;
    }
}

if (!function_exists('getFileExtension')) {
    function getFileExtension($fileData){
        list($type, $fileData) = explode(';', $fileData);
        list(,$extension) = explode('/',$type);
        return $extension;
    }
}
