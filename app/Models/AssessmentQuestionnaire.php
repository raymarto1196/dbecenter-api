<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AssessmentQuestionnaire extends Model
{
    protected $guarded = ['id'];

    protected $casts = [
        'assessment_questionnaire_list' => 'array',
    ];

    use HasFactory;

}
