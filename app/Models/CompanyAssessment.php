<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CompanyAssessment extends Model
{
    protected $guarded = ['id'];

    protected $casts = [
        'assessment_results_list' => 'array',
    ];

    use HasFactory;

    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }    
    
    public function user(): BelongsTo
    {
        return $this->belongsTo(user::class, 'user_id', 'id');
    }
}
