<?php

namespace App\Models;

/**
 *
 * @OA\Schema(
 * @OA\Xml(name="customer"),
 * @OA\Property(property="id", type="integer",  example="1"),
 * @OA\Property(property="customer_code", type="integer",  example="CUS-0001"),
 * @OA\Property(property="registered_name", type="integer",  example="Customer 1"),
 * @OA\Property(property="business_trade_name", type="string",  example="Customer Pharmacy"),
 * @OA\Property(property="business_type", type="string", example="Corporation"),
 * @OA\Property(property="vat_type", type="string", example="Vat exempt"),
 * @OA\Property(property="sales_invoice_delivery_receipt", type="string", example="required"),
 * @OA\Property(property="tin", type="string", example="123-456-789-uuu"),
 * @OA\Property(property="owner", type="string", example="john"),
 * @OA\Property(property="email_address", type="string", example="john@gmail.com"),
 * @OA\Property(property="contact_num", type="string", example="09777037164"),
 * @OA\Property(property="customer_type", type="string", format="email", example="drugstore"),
 * @OA\Property(property="customer_channel", type="string", example="jedo"),
 * @OA\Property(property="billing_address", type="string", example="Unit 522,UT2,Galicia st, Brgy 437 Sampaloc,metro"),
 * @OA\Property(property="lba_code", type="string", example="LBA CODE"),
 * @OA\Property(property="lba_name", type="string", example="LBA NAME"),
 * @OA\Property(property="shipping_address", type="string", example="1767,Sobreidad St, Brgy 445 Sampaloc,metro"),
 * @OA\Property(property="lba_code2", type="string", example="LBA CODE"),
 * @OA\Property(property="lba_name2", type="string", example="LBA NAME"),
 * @OA\Property(property="credit_limit_allowed", type="string", example="500000"),
 * @OA\Property(property="terms", type="string", example="90 days"),
 * @OA\Property(property="distribution_channel", type="string", example="2go"),
 * @OA\Property(property="key_account_manager", type="string", example="PAU"),
 * @OA\Property(property="sales_account_represintative", type="string", example="LIN"),
 * @OA\Property(property="ewt", type="string", example="1"),
 * @OA\Property(property="sernior_citizen_discount", type="string", example="1"),
 * @OA\Property(property="status", type="string", example="ACTIVE"),
 * @OA\Property(property="customer_id", type="integer", example="1"),
 * @OA\Property(property="created_at", type="string", format="date-time", example="2019-02-25T12:59:20Z"),
 * @OA\Property(property="updated_at", type="string",  format="date-time", example="2019-02-25T12:59:20Z"),
 * @OA\Property(property="contact_point", type="object",
 *                       type="array",
 *                       @OA\Items(
 *                          @OA\Property(property="name", type="integer",readOnly="true", example="zes"),
 *                          @OA\Property(property="email", type="integer",readOnly="true",example="zes@gmail.com"),
 *                          @OA\Property(property="contact_num", type="string",readOnly="true",example="09778372047"),
 *                     ) ),
 *)
 * Class customer
 *
 */


use Illuminate\Database\Eloquent\Casts\AsArrayObject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\hasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use DateTime;

class Customer extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = ['id'];

    protected $casts = [
        'contact_point' => AsArrayObject::class,
        'primary_movers' => 'array',
        'delivery_schedule_date' => 'date:m-d-Y',
        'signed_bcaf_tta_upload_date' => 'date:m-d-Y',
        'registration_certificate_upload_date' => 'date:m-d-Y',
        'lto_upload_date' => 'date:m-d-Y',
        'business_permit_upload_date' => 'date:m-d-Y',
        'mayors_permit_upload_date' => 'date:m-d-Y',
        'establishment_photo_upload_date' => 'date:m-d-Y',
        'itr_fs_upload_date' => 'date:m-d-Y',
        'other_document_upload_date' => 'date:m-d-Y',
        'signed_bcaf_tta2_upload_date' => 'date:m-d-Y',
        'prc_id_upload_date' => 'date:m-d-Y',
        'government_valid_id_upload_date' => 'date:m-d-Y',
        'other_documents2_upload_date' => 'date:m-d-Y',
        'signed_bcaf_tta_commitment_date' => 'date:m-d-Y',
        'registration_certificate_commitment_date' => 'date:m-d-Y',
        'lto_commitment_date' => 'date:m-d-Y',
        'business_permit_commitment_date' => 'date:m-d-Y',
        'mayors_permit_commitment_date' => 'date:m-d-Y',
        'establishment_photo_commitment_date' => 'date:m-d-Y',
        'itr_fs_commitment_date' => 'date:m-d-Y',
        'other_document_commitment_date' => 'date:m-d-Y',
        'signed_bcaf_tta2_commitment_date' => 'date:m-d-Y',
        'prc_id_commitment_date' => 'date:m-d-Y',
        'government_valid_id_commitment_date' => 'date:m-d-Y',
        'other_documents2_commitment_date' => 'date:m-d-Y'


    ];


    public function scopeAddresses($query)
    {
        return CustomerAddress::where([
            'morphable_type' => get_class($this),
            // 'morphable_id' => $this->id,
        ]);
    }

    public function courier(): BelongsTo
    {
        return $this->belongsTo(Courier::class);
    }

    public function product_discounts(): HasMany
    {
        return $this->hasMany(CustomerItemDiscount::class);
    }

    public function shipping_address(): HasMany
    {
        return $this->hasMany(Shipping::class);
    }

    public function billing_address(): hasOne
    {
        return $this->hasOne(Address::class);
    }

    public function csm_bank_accounts(): HasMany
    {
        return $this->hasMany(BankAccount::class,'customer_id', 'id');
    }
}
