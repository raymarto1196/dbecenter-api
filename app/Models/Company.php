<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

// use Spatie\Activitylog\Traits\LogsActivity;

class Company extends Model
{
    use HasFactory, SoftDeletes;
    // use LogsActivity;

    protected $guarded = ['id'];

    protected $casts = [
        'naics_codes' => 'array',
        'differentiators' => 'array',
        'capabilities' => 'array',
        'references' => 'array',
        'certifications' => 'array',
        'government_designation' => 'array',
    ];

    protected static $logAttributes = ['*'];

    protected static $logOnlyDirty = true;

    public function users()
    {
        return $this->hasMany(UserProfile::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
