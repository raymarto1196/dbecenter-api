<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderItem extends Model
{
    protected $guarded = ['id'];

    use HasFactory,SoftDeletes;

    public function item(): HasOne
    {
        return $this->hasOne(Item::class ,'id', 'item_id');
    }

    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class, 'id', 'order_id');
    }    
    
    public function vendor_company(): HasOne
    {
        return $this->hasOne(Company::class, 'user_id', 'vendor_id');
    }
}
