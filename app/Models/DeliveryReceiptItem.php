<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class DeliveryReceiptItem extends Model
{
    use HasFactory;
    
    protected $guarded = ['id'];

    public function item_detail()
    {
    	return $this->belongsTo(Item::class,'item_id', 'id');
    }
}
