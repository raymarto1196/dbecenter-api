<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Documentation extends Model
{
    protected $guarded = ['id'];

    use HasFactory;

    public function children()
    { 
        return $this->hasMany(Documentation::class, 'parent_id', 'id')->with('children');
    } 

    public function document_file()
    {
    	return $this->hasMany(DocumentFile::class, 'document_id', 'id');
    }
}
