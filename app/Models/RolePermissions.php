<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RolePermissions extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function accessRoute() {
        return $this->hasOne(StaticModuleRoutes::class, 'id', 'static_module_route_id');
    }
}
