<?php

namespace App\Models;

use App\Notifications\PasswordResetNotification;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 *
 * @OA\Schema(
 * required={"name", "email", "password"},
 * @OA\Xml(name="User"),
 * @OA\Property(property="id", type="integer", readOnly="true", example="1"),
 * @OA\Property(property="name", type="string", readOnly="true", example="Juan Dela Cruz"),
 * @OA\Property(property="email", type="email", readOnly="true", example="juandelacruz@email.com"),
 * @OA\Property(property="email_verified_at", type="string", readOnly="true", format="date-time", example="2019-02-25T12:59:20Z"),
 * @OA\Property(property="created_at", type="string", readOnly="true", format="date-time", example="2019-02-25T12:59:20Z"),
 * @OA\Property(property="updated_at", type="string", readOnly="true", format="date-time", example="2019-02-25T12:59:20Z")
 * )
 *
 * Class User
 *
 */

class User extends Authenticatable
{
    use HasFactory;
    use Notifiable;
    use HasApiTokens;
    use SoftDeletes;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    protected $guard_name = ['api'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    protected static $logAttributes = ['*'];

    // protected static $logOnlyDirty = true;

    /**
     * Get the user associated with the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function profile(): HasOne
    {
        return $this->hasOne(UserProfile::class, 'user_id', 'id');
    }

    public function company(): HasOne
    {
        return $this->hasOne(Company::class);
    }

    public function item(): HasMany
    {
        return $this->hasMany(Item::class, 'user_id');
    }

    public function cart(): HasOne
    {
        return $this->hasOne(Cart::class, 'user_id');
    }

    public function orders(): HasMany
    {
        return $this->hasMany(Order::class, 'user_id');
    }

    public function availed_services(): HasMany
    {
        return $this->hasMany(ServiceAvailment::class, 'user_id');
    }    
    
    public function company_assessment(): HasOne
    {
        return $this->hasOne(CompanyAssessment::class, 'user_id');
    }

    // public function profile() {
    //     return $this->belongsTo(UserProfile::class, 'user_id', 'id');
    // }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordResetNotification($token));
    }

    protected static function boot()
    {
        parent::boot();

        // static::creating(function ($model) {
        //     $model->created_by = is_object(Auth::user()) ? Auth::user()->id : null;
        // });

        // static::updating(function ($model) {
        //     $model->last_updated_by = is_object(Auth::user()) ? Auth::user()->id : null;
        // });

        // static::deleting(function ($model) {
        //     $model->last_updated_by = is_object(Auth::user()) ? Auth::user()->id : null;
        // });
    }

    /**
     * The channels the user receives notification broadcasts on.
     *
     * @return string
     */
    public function receivesBroadcastNotificationsOn()
    {
        return 'users.'.$this->id;
    }

    public function customers(): HasMany
    {
        return $this->hasMany(Customer::class, 'sales_account_representative_id', 'id');
    }
}
