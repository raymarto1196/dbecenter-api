<?php

namespace App\Models;

/**
 *
 * @OA\Schema(

 * @OA\Xml(name="Courier"),
 * @OA\Property(property="id", type="integer",  example="1"),
 * @OA\Property(property="company_name", type="integer",  example="Air 21"),
 * @OA\Property(property="registered_address", type="string",  example="13 Annapolis St. Cubao, Quezon City Philippines"),
 * @OA\Property(property="satellife_office", type="string", example="Manila"),
 * @OA\Property(property="warehouse_address", type="string", example="Manila"),
 * @OA\Property(property="telephone_no", type="string", example="02 (711) 4473"),
 * @OA\Property(property="website_address", type="string", example="proprietorship"),
 * @OA\Property(property="bank_details", type="string", example="f123s3"),
 * @OA\Property(property="email_address", type="string", example="raymart@gmail.com"),
 * @OA\Property(property="dti_sec_reg_no", type="string", example="3123"),
 * @OA\Property(property="tin_vat_reg_no", type="string", example="0123"),
 * @OA\Property(property="buis_reg_no", type="string", format="email", example="3211"),
 * @OA\Property(property="buis_reg_date", type="string", example="019-02-25"),
 * @OA\Property(property="iso_cerificate_no", type="string", example="J123"),
 * @OA\Property(property="service", type="string", example="1"),
 * @OA\Property(property="attach_pic_logo", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
 * @OA\Property(property="business_permit", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
 * @OA\Property(property="cor", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
 * @OA\Property(property="aol_gis", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
 * @OA\Property(property="itr", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
 * @OA\Property(property="bir_cert", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
 * @OA\Property(property="cert_of_good_credit", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
 * @OA\Property(property="audited_fs", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
 * @OA\Property(property="tax_clearance", type="string", format="file", example="vendors/asdsddddsdsss//4uh9wQfSHuYK9gm4s1aW6pzWS3Nm9fosgraXA0Yw.png"),
 * @OA\Property(property="created_at", type="string", format="date-time", example="2019-02-25T12:59:20Z"),
 * @OA\Property(property="updated_at", type="string",  format="date-time", example="2019-02-25T12:59:20Z"),
 * @OA\Property(property="contacts", type="object",
 *                       type="array",
 *                       @OA\Items(
 *                          @OA\Property(property="name", type="integer",readOnly="true", example="raymart"),
 *                          @OA\Property(property="position", type="integer",readOnly="true",example="assistant"),
 *                          @OA\Property(property="email", type="string",readOnly="true",example="ray@gmail.com"),
 *                     ) ),
  * @OA\Property(property="services", type="object",
 *                       type="array",
 *                       @OA\Items(
 *                          example="NCR",
 *                          example="LUZON"
 *                     ) ),
 *)
 * Class Courier
 *
 */


use Illuminate\Database\Eloquent\Casts\AsArrayObject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Courier extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $casts = [
        'contacts' => AsArrayObject::class,
        'serviceable_areas' => 'array',
        'services' => 'array'
    ];

    public function customers(): HasMany
    {
        return $this->hasMany(Customer::class, 'courier_id', 'id');
    }
}
