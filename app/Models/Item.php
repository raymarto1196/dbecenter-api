<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Casts\AsArrayObject;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Item extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = ['id'];
    protected $casts = [
        'weight' => AsArrayObject::class,
        'dimension' => AsArrayObject::class,
        'cpr_expiry' => 'date:m-d-Y',
        'date_drafted' => 'date:m-d-Y',

    ];

    public function inventory_transfers()
    {
        return $this->hasMany(InventoryTransfer::class, 'item_id', 'id');
    }

    public function inventory_adjustments()
    {
        return $this->belongsTo(InventoryAdjustment::class, 'item_id', 'id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function user_profile(): BelongsTo
    {
        return $this->belongsTo(UserProfile::class, 'user_id', 'user_id');
    }

    public function customers(): BelongsToMany
    {
        return $this->belongsToMany(Customer::class);
    }

    public function item_warehouse(): HasMany
    {
        return $this->hasMany(ItemQuantity::class);
    }   

     public function item_ratings(): HasMany
    {
        return $this->hasMany(ItemRating::class, 'item_id', 'id');
    }

    public function bids(): HasMany
    {
        return $this->hasMany(ProjectBid::class, 'item_id', 'id');
    }


    /**
     * Register Model events using Closures
     * Enhancement: This technique is using closure this should be transferred to Event Classes
     */
    protected static function booted()
    {
        static::creating(function ($item) {
            $item->status = strtoupper('pendings');
        });

        static::deleted(function ($item) {
            $item->status = strtoupper('deleted');
            $item->save();
        });
    }
}
