<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class UserProfile extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = ['id'];

    protected static $logAttributes = ['*'];

    protected static $logOnlyDirty = true;

    /**
     * Get the user that owns the UserProfile
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function department(): BelongsTo
    {
        return $this->belongsTo(Department::class);
    }

    // Scope:Active
    public function scopeActive($query)
    {
        return $query->where('status', 'Active');
    }


    // Scope:Inactive
    public function scopeInactive($query)
    {
        return $query->whereStatus('inactive');
    }

    // Scope:Locked
    public function scopeLocked($query)
    {
        //no implementations yet
    }
}
