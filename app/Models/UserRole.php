<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 *
 * @OA\Schema(
 * required={"user_id", "role_name"},
 * @OA\Xml(name="Module"),
 * @OA\Property(property="id", type="integer", readOnly="true", example="1"),
 * @OA\Property(property="user_id", type="integer", readOnly="true", example="1"),
 * @OA\Property(property="role_name", type="string", readOnly="true", example="Role Name"),
 * @OA\Property(property="created_at", type="string", readOnly="true", format="date-time", example="2019-02-25T12:59:20Z"),
 * @OA\Property(property="updated_at", type="string", readOnly="true", format="date-time", example="2019-02-25T12:59:20Z")
 * )
 *
 * Class UserRole
 *
 */


class UserRole extends Model
{
    use HasFactory;

      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'role_name',
    ];

     /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];
}
