<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class CartItem extends Model
{
    protected $guarded = ['id'];

    use HasFactory;

    public function item(): HasOne
    {
        return $this->hasOne(Item::class ,'id', 'item_id');
    }

    public function cart(): BelongsTo
    {
        return $this->belongsTo(User::class, 'id', 'cart_id');
    }
}
