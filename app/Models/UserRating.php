<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserRating extends Model
{
    use HasFactory;

    protected $guarded = ['userid_rater'];

    protected function setKeysForSaveQuery($query)
    {
        // Put appropriate values for your keys here:
        $query
            ->where('userid_rater', '=', $this->getAttribute('userid_rater'))
            ->where('userid_ratee', '=', $this->getAttribute('userid_ratee'));

        return $query;
    }
}
