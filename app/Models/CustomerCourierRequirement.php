<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CustomerCourierRequirement extends Model
{
    protected $guarded = ['id'];
    
    use HasFactory;

    public function customer(): BelongsTo
    {
        return $this->belongsTo(Customer::class, 'customer_id','id');
    }

    public function courier(): BelongsTo
    {
        return $this->belongsTo(Courier::class, 'courier_id','id');
    }

    public function shipping_address(): BelongsTo
    {
        return $this->belongsTo(Shipping::class, 'shipping_address_id','id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'booker_id','id');
    }
}
