<?php

namespace App\Services;

use App\Http\Resources\Dropdowns\BatchNumberResource;
use App\Http\Resources\Dropdowns\CustomerResource;
use App\Http\Resources\Dropdowns\ItemResource;
use App\Http\Resources\Dropdowns\PurchaseOrderResource;
use App\Http\Resources\Dropdowns\SalesOrderResource;
use App\Http\Resources\Dropdowns\VendorResource;
use App\Http\Resources\Dropdowns\WarehouseResource;
use App\Models\Customer;
use App\Models\Item;
use App\Models\PurchaseOrder;
use App\Models\SalesOrder;
use App\Models\Vendor;
use App\Models\Warehouse;
use Illuminate\Support\Facades\DB;

/**
 * This will generate collection design for all specified dropdowns within the application
 * @return `array`
 */

class DropdownService {

    private $search;

    public function __construct($search)
    {
        $this->search = $search;
    }

    public function _parse($array) {
        
        $assoc_array = [];

        foreach ($array as $key => $value) {
            $assoc_array[] = [
                'value' => $key,
                'label' => $value,
                'code' => $value
            ];
        }

        return $assoc_array;

    }

    public function toArray() {
        switch ($this->search) {
            case 'warehouse':
                return WarehouseResource::collection(Warehouse::select('id', 'warehouse_code', 'warehouse_name')->get());
                break;
            case 'vendor':
                return VendorResource::collection(Vendor::select('id', 'vendorcode', 'displayname')->get());
                break;
            case 'item':
                return ItemResource::collection(Item::select('id', 'item_code', 'item_name')->get());
                break;
            case 'customer':
                return CustomerResource::collection(Customer::select('id', 'registered_name', 'customer_code')->get());
                break;
            case 'sales_order':
                return SalesOrderResource::collection(SalesOrder::select('id', 'reference_number')->get());
                break;
            case 'purchase_order':
                return PurchaseOrderResource::collection(PurchaseOrder::select('id', 'reference_number')->get());
                break;
            case 'batch_number':
                return BatchNumberResource::collection(DB::table('item_batches')->select('lot_number')->distinct('lot_number')->get());
                break;
            case 'sop_workflow_status':
                return $this->_parse(config('app.sales_order_workflow_status'));
                break;
                 
            default:
                # code...
                break;
        }
       
    }
}