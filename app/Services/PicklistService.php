<?php

namespace App\Services;

use App\Http\Resources\Dropdowns\BatchNumberResource;
use App\Http\Resources\Dropdowns\CustomerResource;
use App\Http\Resources\Dropdowns\ItemResource;
use App\Http\Resources\Dropdowns\PurchaseOrderResource;
use App\Http\Resources\Dropdowns\SalesOrderResource;
use App\Http\Resources\Dropdowns\VendorResource;
use App\Http\Resources\Dropdowns\WarehouseResource;
use App\Models\Customer;
use App\Models\Item;
use App\Models\PurchaseOrder;
use App\Models\SalesOrder;
use App\Models\Vendor;
use App\Models\Warehouse;
use Illuminate\Support\Facades\DB;

/**
 * This will generate collection design for all specified dropdowns within the application
 * @return `array`
 */

class PicklistService {

    private $order;

    public function __construct($order)
    {
        $this->order = $order;
    }

    public function picklist_item_instruction() {

    }


}
