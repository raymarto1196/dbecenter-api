<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\DepartmentsController;
use App\Http\Controllers\Api\FileUploadController;
use App\Http\Controllers\Api\ForgotPasswordController;
use App\Http\Controllers\Api\ItemsController;
use App\Http\Controllers\Api\ModuleController;
use App\Http\Controllers\Api\ModulePermissionController;
use App\Http\Controllers\Api\PermissionsController;
use App\Http\Controllers\Api\UserDepartmentController;
use App\Http\Controllers\Api\UserRoleController;
use App\Http\Controllers\Api\RefDepartmentController;
use App\Http\Controllers\Api\ResetPasswordController;
use App\Http\Controllers\Api\RolesController;
use App\Http\Controllers\Api\UsersController;
use App\Http\Controllers\Api\VendorsController;
use App\Http\Controllers\Api\InventoryTransferController;
use App\Http\Controllers\Api\InventoryAdjustmentsController;
use App\Http\Controllers\Api\InventoryRevaluationsController;
use App\Http\Controllers\Api\CouriersController;
use App\Http\Controllers\Api\InventoryReceiptsController;
use App\Http\Controllers\Api\LoginController;
use App\Http\Controllers\Api\WarehouseController;
use App\Http\Controllers\Api\CustomersController;
use App\Http\Controllers\Api\CustomerAddressController;
use App\Http\Controllers\Api\ItemTaxOptionsController;
use App\Http\Controllers\Api\ItemUOMController;
use App\Http\Controllers\Api\ActivityLogsController;
use App\Http\Controllers\Api\BanksController;
use App\Http\Controllers\Api\NotificationController;
use App\Http\Controllers\Api\SalesOrdersController;
use App\Http\Controllers\Api\PurchaseOrdersController;
use App\Http\Controllers\Api\PurchaseRequestsController;
use App\Http\Controllers\Api\InventoryReportsController;
use App\Http\Controllers\Api\MaintenanceFieldCustomizationController;
use App\Http\Controllers\Api\MaintenanceCodeSetupController;
use App\Http\Controllers\Api\BankAccountController;
use App\Http\Controllers\Api\CustomerShippingsController;
use App\Http\Controllers\Api\DeliveryReceiptsController;
use App\Http\Controllers\Api\DispatchingParcelController;
use App\Http\Controllers\Api\SalesPersonController;
use App\Http\Controllers\Api\DocumentsController;
use App\Http\Controllers\Api\FulfillmentOrdersController;
use App\Http\Controllers\Api\InvoicesController;
use App\Http\Controllers\Api\PlannersController;
use App\Http\Controllers\Api\ViewUserDataController;
use App\Http\Controllers\Api\PaymentsController;
use App\Http\Controllers\Api\PicklistsController;
use App\Http\Controllers\Api\StaticModuleRoutesController;
use App\Http\Controllers\Api\RolePermissionsController;
use App\Http\Controllers\Api\SalesOrderEvaluationController;
use App\Http\Controllers\Api\SearchDropdownController;
use App\Http\Controllers\Api\TransportShipmentController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::post('v1/test', function (Request $request) {
    return response()->json(config('app.sales_order_statuses') ?? 'NA');
});

Route::get('view_user_data', [ViewUserDataController::class, 'index']);
Route::get('/v1/sample', [ViewUserDataController::class, 'index']);


Route::post('/v1/auth/register', [AuthController::class, 'register']);
Route::post('/v1/auth/login', [AuthController::class, 'login']);
Route::post('/v1/auth/logout', [AuthController::class, 'logout'])->middleware('auth:api');
Route::post('/v1/auth/password/email', [ForgotPasswordController::class, 'sendResetLinkEmail']);
Route::post('/v1/auth/password/reset', [ResetPasswordController::class, 'reset']);

Route::post('/v1/upload', [FileUploadController::class, 'upload']);

Route::group(['prefix' => 'v1', 'as' => 'api.', 'middleware' => ['auth:api']], function () {
    Route::get('dropdowns', [SearchDropdownController::class, 'search']);

    Route::get('inventory-adjustments/get-item-batches', [InventoryAdjustmentsController::class, 'get_item_batches']);
    Route::put('inventory-adjustments/{inventory_adjustment}/change_status', [InventoryAdjustmentsController::class, 'change_status']);
    Route::get('inventory-revaluations/get-item-batches', [InventoryRevaluationsController::class, 'get_item_batches']);
    Route::put('inventory-revaluations/{inventory_revaluation}/change_status', [InventoryRevaluationsController::class, 'change_status']);
    Route::post('users/{user}/change_password', [UsersController::class, 'change_password']);
    Route::post('users/{user}/change_status', [UsersController::class, 'change_status']);
    Route::post('sales-orders/{sales_order}/change_status', [SalesOrdersController::class, 'change_status']);
    Route::get('picklist/{sales_order}', [PicklistsController::class, 'picklist']);

    Route::get('logs', ActivityLogsController::class);

    // Item Management
    Route::get('items/get_status', [ItemsController::class, 'get_status']);
    // Route::post('items/{item}/change_status', [ItemsController::class, 'change_status']);
    Route::put('items/{item}/set_active_inactive', [ItemsController::class, 'set_active_inactive']);

    // Vendor Management
    Route::post('items/{vendor}/change_to_credited', [ItemsController::class, 'change_to_credited']);

    // Inventory Receipt/Receiving
    Route::get('inventory-receipts/get-irr-items', [InventoryReceiptsController::class, 'get_irr_items']);
    Route::put('inventory-receipts/change-status/{inventory_receipt}', [InventoryReceiptsController::class, 'change_status']);
    Route::put('inventory-receipts/inspection-checklists/{inventory_receipt}', [InventoryReceiptsController::class, 'inspection_checklist']);

    // Commented by @Raymart 07242021  Unused Routes, can be removed
    // Route::get('inventory-receipts/assignment', [InventoryReceiptsController::class, 'index2']);
    // Route::post('inventory-receipts/view', [InventoryReceiptsController::class, 'view']);
    // Route::post('inventory-receipts/get_warehouse_items/{warehouse_id}', [InventoryReceiptsController::class, 'get_warehouse_items']);
    // Route::get('inventory-receipts/get_warehouse_pallets', [InventoryReceiptsController::class, 'get_warehouse_pallet_status']);
    // Route::get('inventory-receipts/get_purchase_order/{po_id}', [InventoryReceiptsController::class, 'get_purchase_order']);
    // Route::put('inventory-receipts/get_warehouse_items/{warehouse_id}/item/{item_id}', [InventoryReceiptsController::class, 'get_batch_by_warehouse_items']);

    // Couriers Management
    Route::put('couriers/{courier}/addwaybill', [CouriersController::class, 'create_waybill']);
    Route::post('couriers/{courier}/update_waybill', [CouriersController::class, 'update_waybill']);
    Route::get('couriers/courier_requirments', [CouriersController::class, 'courier_requirments']);
    Route::put('couriers/{courier}/change_to_credited', [CouriersController::class, 'change_to_credited']);

    Route::put('vendors/{vendor}/change_to_credited', [VendorsController::class, 'change_to_credited']);
    Route::put('vendors/{vendor}/change_status', [VendorsController::class, 'change_status']);
    Route::put('vendors/change_statuses', [VendorsController::class, 'change_statuses']);
    Route::post('vendors/upload', [VendorsController::class, 'upload']);

    // Customer Management
    Route::put('customers/{customer}/set_active_inactive', [CustomersController::class, 'set_active_inactive']);
    Route::put('customers/{customer}/fileupload', [CustomersController::class, 'documentation_fileupload']);
    Route::post('customers/create_courier_requirements', [CustomersController::class, 'customer_with_courier_requirements']);
    Route::delete('customers/delete_courier_requirements/{id}', [CustomersController::class, 'remove_customer_courier_requirements']);
    Route::get('customers/get_all_courier_requirements', [CustomersController::class, 'get_all_customer_courier_requirements']);
    Route::get('customers/{id}/get_courier_requirements', [CustomersController::class, 'get_customer_courier_requirements']);
    Route::put('customers/update_courier_requirements/{id}', [CustomersController::class, 'update_customer_courier_requirements']);

    // Purchase Request Approval
    Route::put('purchase-requests/approver_1/{purchase_request}', [PurchaseRequestsController::class, 'approve_po1']);
    Route::put('purchase-requests/approver_2/{purchase_request}', [PurchaseRequestsController::class, 'approve_po2']);
    Route::get('purchase-orders/{purchase_order}/with_same_pr_supplier', [PurchaseOrdersController::class, 'pr_with_same_po_vendor']);

    //Purchase Order Approval
    Route::get('purchase-requests/approved_purchase_request', [PurchaseOrdersController::class, 'approved_purchase_request']);
    Route::get('purchase-orders/approve_purchase_orders', [PurchaseOrdersController::class, 'approved_purchase_orders']);
    Route::put('purchase-orders/approver_1/{purchase_order}', [PurchaseOrdersController::class, 'approve_po1']);
    Route::put('purchase-orders/approver_2/{purchase_order}', [PurchaseOrdersController::class, 'approve_po2']);
    Route::put('purchase-orders/approver_3/{purchase_order}', [PurchaseOrdersController::class, 'approve_po3']);

    // Warehouse Management
    Route::get('pallets', [WarehouseController::class, 'pallets']);
    Route::put('warehouses/{warehouse}/change_status', [WarehouseController::class, 'change_status']);
    Route::put('warehouses/{warehouse}/set_active_inactive', [WarehouseController::class, 'set_active_inactive']);
    Route::get('warehouses/{warehouse_id}/pallets/{warehouse_pallet_id}', [WarehouseController::class, 'get_warehouse_pallets']);


    Route::post('inventory-reports/get_reports', [InventoryReportsController::class, 'get_reports']);
    Route::put('inventory-reports/warehouses_pallet_assignment', [InventoryReportsController::class, 'get_warehouse_pallet_assignment']);
    Route::put('inventory-reports/pallet_status', [InventoryReportsController::class, 'get_pallet_status']);

    // Document management
    // Route::put('documents/move_files', [DocumentationController::class, 'move_files']);
    // Route::post('documents/{document}/add_folder', [DocumentationController::class, 'add_folder']);
    // Route::get('documents/{document}/get_files', [DocumentationController::class, 'file_previews']);
    // Route::put('documents/{document}/update_files', [DocumentationController::class, 'update_files']);
    // Route::post('documents/{document}/create_files', [DocumentationController::class, 'upload_files']);
    // Route::delete('documents/{document}/delete_files', [DocumentationController::class, 'destroy_files']);

    // SalesOrder Endpoints [for Testing Only]
    Route::get('sales-orders/user/get_customers', [SalesOrdersController::class, 'get_customer']);
    Route::post('createStatusForOrder/{sales_order}', [SalesOrdersController::class, 'createStatusForOrder']);
    Route::post('createCommentForOrder/{sales_order}', [SalesOrdersController::class, 'createCommentForOrder']);



    // Fulfillment Orders
    Route::put('fulfillment-orders/{fulfillment_order}/update_status', [FulfillmentOrdersController::class, 'update_status']);
    Route::put('fulfillment-orders/{fulfillment_order}/transaction_works', [FulfillmentOrdersController::class, 'transaction_works']);

    Route::put('inventory-transfers/{inventoryTransfer}/change-status', [InventoryTransferController::class, 'change_status']);
    Route::get('inventory-transfers/get-item-batches', [InventoryTransferController::class, 'get_item_batches']);
    Route::get('inventory-transfers/last-id', [InventoryTransferController::class, 'get_last_inserted_id']);
    Route::get('inventory-transfers/get_items', [InventoryTransferController::class, 'get_items']);

    Route::post('planners', PlannersController::class);
    Route::post('documents/uploadFile', [DocumentsController::class, 'uploadFile']);
    Route::get('documents/{document}/back', [DocumentsController::class, 'navigateBack']);

    // Create Sales Order Picklist
    Route::put('sales-orders/{sales_order}/delivery_receipt/{delivery_receipt_id}', [PicklistsController::class, 'put_delivery_receipt']);
    Route::get('sales-orders/{sales_order}/delivery_receipt/{delivery_receipt_id}', [PicklistsController::class, 'get_delivery_receipt']);
    Route::post('sales-orders/{sales_order}/delivery_receipt', [PicklistsController::class, 'delivery_receipt']);
    Route::post('sales-orders/{sales_order}/picklist', [PicklistsController::class, 'store']);
    Route::put('sales-orders/{sales_order}/picklist', [PicklistsController::class, 'update']);
    Route::get('sales-orders/{sales_order}/picklist', [PicklistsController::class, 'show']);
    Route::get('picklists', [PicklistsController::class, 'index']);

    Route::put('picklists/{picklist}/dispatching-parcels/{dispatching_parcel}/change-status', [TransportShipmentController::class, 'change_status']);
    Route::get('picklists/dispatching-parcels', [TransportShipmentController::class, 'index']);
    // Create Payments for Sales Order
    // Route::post('sales-orders/{sales_order}/payments', PaymentsController::class);
    Route::post('invoices/{invoice}/payments', [PaymentsController::class, 'store']);


    Route::post('invoices/{delivery_receipt}', [InvoicesController::class, 'store']);


    //Roles and Permissions
    Route::get('roles-permissions/{role_id}', [RolePermissionsController::class, 'show']);
    Route::put('roles-permissions/{role_id}', [RolePermissionsController::class, 'update']);

    Route::apiResource('picklists.dispatching-parcels', TransportShipmentController::class);
    Route::apiResource('picklists.delivery-receipts', DeliveryReceiptsController::class);
    Route::apiResource('customers.shippings', CustomerShippingsController::class);
    Route::apiResource('inventory-adjustments', InventoryAdjustmentsController::class);
    Route::apiResource('inventory-revaluations', InventoryRevaluationsController::class);

    Route::apiResource('item-tax-options', ItemTaxOptionsController::class);
    Route::apiResource('item-uoms', ItemUOMController::class);
    Route::apiResource('permissions', PermissionsController::class);
    Route::apiResource('roles', RolesController::class);
    Route::apiResource('banks', BanksController::class);
    Route::apiResource('departments', DepartmentsController::class);
    Route::apiResource('inventory-transfers', InventoryTransferController::class);
    Route::apiResource('invoices', InvoicesController::class);
    Route::apiResource('items', ItemsController::class);
    Route::apiResource('shipments', ItemsController::class);
    Route::apiResource('users', UsersController::class);
    Route::apiResource('sales-order-evaluations', SalesOrderEvaluationController::class);

    Route::apiResource('vendors', VendorsController::class);
    Route::apiResource('couriers', CouriersController::class);
    Route::apiResource('documents', DocumentsController::class);
    Route::apiResource('bank-accounts', BankAccountController::class);

    Route::apiResource('fulfillment-orders', FulfillmentOrdersController::class);
    Route::apiResource('field-customizations', MaintenanceFieldCustomizationController::class);
    Route::apiResource('code-setups', MaintenanceCodeSetupController::class);
    Route::apiResource('inventory-reports', InventoryReportsController::class);
    Route::apiResource('inventory-receipts', InventoryReceiptsController::class);
    Route::apiResource('sales-orders', SalesOrdersController::class);
    Route::apiResource('sales-persons', SalesPersonController::class);
    Route::apiResource('warehouses', WarehouseController::class);
    Route::apiResource('purchase-orders', PurchaseOrdersController::class);
    Route::apiResource('purchase-requests', PurchaseRequestsController::class);
    Route::apiResource('customers', CustomersController::class);
    Route::apiResource('customers', CustomersController::class);
    // Route::apiResource('payments', PaymentsController::class);
    Route::apiResource('static-module-routes', StaticModuleRoutesController::class);
    Route::apiResource('users-role', UserRoleController::class);



    // Route::resource('module', ModuleController::class);
    // Route::resource('module-permission', ModulePermissionController::class);
    // Route::resource('user-department', UserDepartmentController::class);

    // Route::resource('ref-department', RefDepartmentController::class);


    /*
    *   Test notification on pusher
    */
    Route::apiResource('notifications', NotificationController::class)->only(['index', 'store', 'update']);
});
