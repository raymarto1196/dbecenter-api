<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\CartController;
use App\Http\Controllers\Api\OrderController;
use App\Http\Controllers\Api\ForgotPasswordController;
use App\Http\Controllers\Api\ItemRatingController;
use App\Http\Controllers\Api\ItemsController;
use App\Http\Controllers\Api\ProjectBidController;
use App\Http\Controllers\Api\SearchController;
use App\Http\Controllers\Api\ServiceAvailmentController;
use App\Http\Controllers\Api\SicController;
use App\Http\Controllers\Api\StandardIndustryCodeController;
use App\Http\Controllers\Api\UserCartController;
use App\Http\Controllers\Api\UserItemsController;
use App\Http\Controllers\Api\UserProfileController;
use App\Http\Controllers\Api\UserRatingsController;
use App\Http\Controllers\Api\UsersController;
use App\Http\Controllers\Api\CompanyAssessmentController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::post('v1/test', function (Request $request) {
    return response()->json(config('app.sales_order_statuses') ?? 'NA');
});


Route::post('/v1/auth/register', [AuthController::class, 'register']);
Route::post('/v1/auth/login', [AuthController::class, 'login']);
Route::post('/v1/auth/logout', [AuthController::class, 'logout'])->middleware('auth:api');
Route::post('/v1/auth/password/email', [ForgotPasswordController::class, 'sendResetLinkEmail']);
// Route::post('/v1/auth/password/reset', [ResetPasswordController::class, 'reset']);

// Route::post('/v1/upload', [FileUploadController::class, 'upload']);
Route::get('/v1/items/category/{category_type}', [ItemsController::class, 'item_category']);
Route::get('v1/itemList', [ItemsController::class, 'index']);
Route::get('v1/user/companies', [UsersController::class, 'companies']);
Route::get('v1/user/full_details/{user_id}', [UsersController::class, 'user_full_details']);
Route::get('v1/search', [StandardIndustryCodeController::class, 'get_naic_codes']);

// Search suggestions
Route::get('v1/search/{keyword?}', [SearchController::class, 'search']);

Route::group(['prefix' => 'v1', 'as' => 'api.', 'middleware' => ['auth:api']], function () {
    Route::apiResource('items', ItemsController::class);
    Route::apiResource('users.items', UserItemsController::class);
    Route::apiResource('carts', CartController::class);
    Route::apiResource('orders', OrderController::class);
    Route::apiResource('bids', ProjectBidController::class);
    Route::apiResource('ratings', ItemRatingController::class);
    Route::apiResource('availed_services', ServiceAvailmentController::class);

    Route::put('cart/check_out', [CartController::class, 'item_check_out']);
    Route::get('user/cart', [CartController::class, 'get_user_carts']);
    Route::get('user/orders', [OrderController::class, 'get_user_orders']);
    Route::get('user/project_bids', [ProjectBidController::class, 'get_vendor_bids']);
    Route::get('item/bids/{item_id}', [ProjectBidController::class, 'get_item_bids']);
    Route::put('user/order/status/{order_id}', [OrderController::class, 'change_status']);
    Route::get('item/ratings/{item_id}', [ItemRatingController::class, 'get_item_ratings']);
    Route::get('user/contractor_orders', [OrderController::class, 'get_contractor_orders']);
    Route::get('user/items/category/{category_type}', [ItemsController::class, 'user_item_category']);  
    Route::put('user/availed_service/status/{service_id}', [ServiceAvailmentController::class, 'change_status']);
    Route::get('user/vendor/availed_services', [ServiceAvailmentController::class, 'get_vendor_availed_services']);
    Route::get('user/contractor/availed_services', [ServiceAvailmentController::class, 'get_contractor_availed_services']);

    // User Management
    Route::get('user', [UsersController::class, 'get_user']);
    Route::put('user/profile', [UsersController::class, 'user_profile']);
    Route::put('user/company', [UsersController::class, 'user_company']);
    Route::put('user/company/references', [UsersController::class, 'user_company_references']);
    Route::put('user/company/capabilities', [UsersController::class, 'user_company_capabilities']);
    Route::put('user/company/assessments', [CompanyAssessmentController::class, 'update_assessment']);
    Route::put('user/company/certifications', [UsersController::class, 'user_company_certifications']);
    Route::put('user/company/differentiators', [UsersController::class, 'user_company_differentiators']);
    Route::put('user/company/government-designation', [UsersController::class, 'government_designation']);
    Route::get('user/company/assessments/results', [CompanyAssessmentController::class, 'AssesmentResults']);
    Route::get('user/company/assessments/questionnaires', [CompanyAssessmentController::class, 'AssesmentQuestionnaires']);
  
        
    Route::post('user-ratings', [UserRatingsController::class, 'store']);
    /*
    *   Test notification on pusher
    */
    // Route::apiResource('notifications', NotificationController::class)->only(['index', 'store', 'update']);
});
