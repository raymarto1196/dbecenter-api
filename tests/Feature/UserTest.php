<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        // $this->user = factory(User::class)->make();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    // public function test_example()
    // {
    //     $response = $this->get('/');

    //     $response->assertStatus(200);
    // }

    /**
     * @test
     */
    public function usernameShouldNotBeTooShort()
    {
        $response = $this->post('/api/v1/auth/register', [
            "username" => "tes",
            "email" => "test@mail.com",
            "password" => "Admin123!",
            "password_confirmation" => "Admin123!",
            "name" => "John Doe",
            "role" => "contractor"
        ]);

        $response->assertStatus(422);
        // $response->assertSessionHasErrors([
        //     'username' => 'The username must be at least 4 characters.'
        // ]);
    }
}
