<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthenticationTest extends TestCase
{
    public function testRequiredFieldsForRegistration()
    {
        $this->json('POST', '/api/v1/auth/register', [], ['Accept' => 'application/json'])
        ->assertStatus(422);
        // ->assertJson([
        //     "error" => true,
        //     "message" =>"Validation Error",
        //     "data" => [
        //         "username" => [
        //             "The username field is required."
        //         ],
        //         "email" => [
        //             "The email field is required."
        //         ],
        //         "name" => [
        //             "The name field is required."
        //         ],
        //         "password" => [
        //             "The password field is required."
        //         ]
        //     ]
        // ]);
    }

    public function testDefaultRoleIsContractor()
    {
    }

    public function testLogoutInvalidToken()
    {
        $this->json('POST', 'api/v1/auth/logout', [], [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer test'
        ])
        ->assertStatus(401)
        ->assertJson(
            [
                'error' => true,
                'message' => 'Unauthenticated'
            ]
        );
    }
}
